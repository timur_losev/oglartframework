//
//  TestGLFL.cpp
//  OGLArt
//
//  Created by tlosev on 10/13/14.
//
//

#include "Precompiled.h"
#include "TestGLFL.h"



void sOnGLDebugOutput( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam );

int main()
{
    glfwInit();
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
#endif
    
    GLFWwindow* window = glfwCreateWindow(800, 600, "GLFW Test", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    
    const char * slVer = (const char *) glGetString ( GL_SHADING_LANGUAGE_VERSION );

    gl::init();
    
#ifdef GL_ARB_debug_output
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    glo::glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    glo::glDebugMessageCallbackARB(&sOnGLDebugOutput, NULL);
#endif

    
    return 0;
}

void sOnGLDebugOutput( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam )
{
    char debSource[32], debType[32], debSev[32];
    bool Error(false);
    
    const GLuint texture_usage_warning_flood = 131204;
    
    if (id == texture_usage_warning_flood)
    {
        return;
    }
    
    if (source == GL_DEBUG_SOURCE_API_ARB)
        strcpy(debSource, "OpenGL");
    else if (source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
        strcpy(debSource, "Windows");
    else if (source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
        strcpy(debSource, "Shader Compiler");
    else if (source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
        strcpy(debSource, "Third Party");
    else if (source == GL_DEBUG_SOURCE_APPLICATION_ARB)
        strcpy(debSource, "Application");
    else if (source == GL_DEBUG_SOURCE_OTHER_ARB)
        strcpy(debSource, "Other");
    
    if (type == GL_DEBUG_TYPE_ERROR_ARB)
        strcpy(debType, "error");
    else if (type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
        strcpy(debType, "deprecated behavior");
    else if (type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
        strcpy(debType, "undefined behavior");
    else if (type == GL_DEBUG_TYPE_PORTABILITY_ARB)
        strcpy(debType, "portability");
    else if (type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
        strcpy(debType, "performance");
    else if (type == GL_DEBUG_TYPE_OTHER_ARB)
        strcpy(debType, "message");
    
    if (severity == GL_DEBUG_SEVERITY_HIGH_ARB)
    {
        strcpy(debSev, "high");
        Error = true;
    }
    else if (severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
        strcpy(debSev, "medium");
    else if (severity == GL_DEBUG_SEVERITY_LOW_ARB)
    {
        strcpy(debSev, "low");
    }
    
    char buf[1024];
    sprintf(buf, "%s: %s(%s) %d: %s", debSource, debType, debSev, id, message);

    printf(buf);
}

