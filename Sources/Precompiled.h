/*
 * File:   Precompiled.h
 * Author: void
 *
 * Created on March 23, 2013, 11:26 PM
 */

#ifndef PRECOMPILED_H
#    define	PRECOMPILED_H

//#include <glo/glo.hpp>
// 
// #define AS_HAS_ITERATOR_DEBUGGING _HAS_ITERATOR_DEBUGGING

#ifdef WIN32
#include <Windows.h>
#endif

#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <memory.h>
#include <memory>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <math.h>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <sstream>
#include <stdint.h>

#include <unordered_map>

#include <fstream>

// #undef _HAS_ITERATOR_DEBUGGING
// #define _HAS_ITERATOR_DEBUGGING AS_HAS_ITERATOR_DEBUGGING

#ifndef AS_BDAE

#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include "glo/glo.hpp"

#endif

#define  GLM_SWIZZLE

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform2.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/euler_angles.hpp"
#include "glm/gtc/constants.hpp"
#include "glm/gtx/transform.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/foreach.hpp>

#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/utility/setup/console.hpp>

#include "tl/FVector.h"
#include "tl/FList.h"
#include "tl/FMap.h"
#include "tl/FHashTable.h"
#include "tl/FArray.h"
#include "tl/FString.h"

#ifndef AS_BDAE
#include <OIS.h>
#endif

#include "Platform.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::mat3;
using glm::quat;
using glm::ivec2;

typedef glm::vec4 Color_t;

typedef std::string String_t;
typedef std::ostringstream Ostringstream_t;
namespace FS = boost::filesystem;
typedef FS::path Path_t;
//#define WS(s) L##s
#define WS(s) s

typedef unsigned int Bool_t;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define forward_this(__class__)\
class __class__;\
typedef std::shared_ptr<__class__> __class__##Ptr;\
typedef std::shared_ptr<__class__ const> __class__##ConstPtr

#define forward_this_s(__class__)\
struct __class__;\
typedef std::shared_ptr<__class__> __class__##Ptr;\
typedef std::shared_ptr<__class__ const> __class__##ConstPtr

#define forward_this_w(__class__)\
class __class__;\
typedef std::weak_ptr<__class__> __class__##WeakPtr;\
typedef std::weak_ptr<__class__ const> __class__##ConstWeakPtr

#define forward_this_ws(__class__)\
struct __class__;\
typedef std::weak_ptr<__class__> __class__##WeakPtr;\
typedef std::weak_ptr<__class__ const> __class__##ConstWeakPtr


#if !defined(WIN32) && !defined(APIENTRY)
#define APIENTRY
#endif


template <int v>
struct Int2Type
{
    enum { value = v };
};

#define USE_NORMAL_MAPPING 0

#endif	/* PRECOMPILED_H */

