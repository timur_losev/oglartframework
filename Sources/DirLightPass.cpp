#include "Precompiled.h"
#include "DirLightPass.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"
#include "Vertex.h"

#include "TextureManager.h"
#include "Texture.h"
#include "RenderUtils.h"
#include "Light.h"

DirLightPass::DirLightPass()
{

}

DirLightPass::~DirLightPass()
{

}

void DirLightPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);

    glo::glUniform3f(m_baseLightLoc.Color, 1.0f, 1.0f, 1.0f);
    glo::glUniform1f(m_baseLightLoc.AmbientIntensity, 0.1f);
    glo::glUniform1f(m_baseLightLoc.DiffuseIntensity, 0.1f);

    glo::glUniform3f(m_directionalLightLoc.Direction, 1.0f, 0.f, 0.f);
}

void DirLightPass::commitImpl()
{
    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, position));
    int tcLoc = rut::glVertexAttribPointer("VertexTexcoord0", m_shaderProgram, 2, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, texcoord0));
    int colLoc = rut::glVertexAttribPointer("VertexColor0", m_shaderProgram, 4, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, color));
    int normLoc = rut::glVertexAttribPointer("VertexNormal", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, normal));

    rut::glEnableVertexAttribArray(posLoc);
    rut::glEnableVertexAttribArray(tcLoc);
    rut::glEnableVertexAttribArray(colLoc);
    rut::glEnableVertexAttribArray(normLoc);

    m_baseLightLoc.Color                = m_shaderProgram->get("Color");
    m_baseLightLoc.AmbientIntensity     = m_shaderProgram->get("AmbientIntensity");
    m_baseLightLoc.DiffuseIntensity     = m_shaderProgram->get("DiffuseIntensity");

    m_directionalLightLoc.Direction     = m_shaderProgram->get("Direction");

    glo::glUniform1i(m_shaderProgram->get("Mrt0"), 0);
    glo::glUniform1i(m_shaderProgram->get("Mrt1"), 1);

}
