#pragma once

forward_this(ShaderProgram);

class ShaderProgramManager
{
public:

    ShaderProgramManager();
    ~ShaderProgramManager();

    static ShaderProgramPtr constructGeometryPassDeferredProgram();
    static ShaderProgramPtr constructNullPassDeferredProgram();
    static ShaderProgramPtr constructPointLightPassDeferredProgram();
    static ShaderProgramPtr constructDirectionalPassDeferredProgram();
    static ShaderProgramPtr constructSimpleForwardProgram();
    static ShaderProgramPtr constructObjectOverlayProgram();
    static ShaderProgramPtr constructSpotLightPassDeferredProgram();

    static bool use(ShaderProgramConstPtr);
};