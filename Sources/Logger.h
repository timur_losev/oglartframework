//
//  Logger.h
//  OGLArt
//
//  Created by tlosev on 10/14/14.
//
//

#ifndef __OGLArt__Logger__
#define __OGLArt__Logger__

#include "LazySingleton.h"

namespace BoostLogger = boost::log;
namespace LogSinks = BoostLogger::sinks;
namespace LogSources = BoostLogger::sources;
namespace LogExpressions = BoostLogger::expressions;
namespace LogAttributes = BoostLogger::attributes;
namespace LogKeywords = BoostLogger::keywords;

class Logger : public LazySingleton<Logger>
{
private:
    LogSources::severity_logger< BoostLogger::trivial::severity_level > m_fileLog;
    bool m_fileLoggerActive = false;

public:
    
    Logger();
    ~Logger();

    void setupFileLog(const Path_t& logFilePath);
    void trace(Ostringstream_t& str);
    void log(const std::string& str);
};

#define GetLoggerPtr Logger::GetPtr

#define OGL_TRACE(logger, message) {\
    Ostringstream_t __ss;\
    __ss << message;\
    logger->trace(__ss); }

#define OGL_TRACE_ASSERT(logger, cond, message) if(!(cond)){ OGL_TRACE(logger, "ASSERT: " << message << " " << __FILE__ << ":" << __LINE__); }
#define OGL_TRACE_DEBUG OGL_TRACE
#define OGL_TRACE_ERROR OGL_TRACE
#define OGL_TRACE_WARN OGL_TRACE
#define OGL_TRACE_INFO OGL_TRACE

#define OGL_LOG(x) Logger::GetPtr()->log((boost::format x).str())

#define LOG_TODO(msg)\
OGL_TRACE_WARN(GetLoggerPtr(), boost::str(boost::format("TODO: Block. [%s] Implementation required at %s: %s: %d") % msg % __FILE__ % __FUNCTION__ % __LINE__))


#endif /* defined(__OGLArt__Logger__) */
