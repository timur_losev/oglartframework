#pragma once

forward_this(Texture);

class GeometryBuffer
{
public:
    typedef std::vector<TexturePtr> FBOTextures_t;

    enum
    {
        COLOR_AND_SPECULAR_BUFFER,
        NORMAL_AND_DEPTH_BUFFER,
        
        TOTAL_RENDER_BUFFERS
    };

    enum
    {
        DEPTH_ATTACHMENT = TOTAL_RENDER_BUFFERS,
        FINAL_BUFFER = TOTAL_RENDER_BUFFERS,
        TOTAL_ATTACHMENT_BUFFERS = 1
    };

    enum
    {
        TOTAL_BUFFERS = TOTAL_RENDER_BUFFERS + TOTAL_ATTACHMENT_BUFFERS
    };

private:

    GLuint m_deferredFBO;
    uint32_t m_depthBuf; //deprecated. todo remove

    TexturePtr m_depthTex;
    uint32_t m_finalTex;

    uint32_t m_renderBuffers[TOTAL_RENDER_BUFFERS];

    FBOTextures_t m_fboTextures;
    ivec2         m_dimension;
public:

    GeometryBuffer();
    ~GeometryBuffer(void);

    bool create(const ivec2& dimenstion);

    void startFrame();
    void unbind();

    void bindForGeometryPass();
    void bindForStencilPass();
    void bindForLightPass();
    void bindForFinalPass();

    TexturePtr getDepthTexture() const;

    const FBOTextures_t& getFBOTextures() const;
};

