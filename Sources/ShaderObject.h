#pragma once

#include "ShaderObjectType.h"

class ShaderObject
{
private:
    GLuint handle;

    ShaderObjectType::Enum  objectType;

#ifdef DEBUG_MODE
    std::string             source;
#endif

    Bool_t                  compiled;
public:

    ShaderObject(ShaderObjectType::Enum withType);
    ~ShaderObject();

    GLuint getHandle() const;

    bool compile(const std::string& source);
    bool compile(const char* data, size_t len);
    Bool_t hasCompiled() const;
};