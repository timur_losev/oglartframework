#include "Precompiled.h"
#include "GBufferPass.h"
#include "ShaderProgram.h"

#include "Vertex.h"
#include "Texture.h"

#include "TextureManager.h"

#include "RenderUtils.h"

#include "ShaderProgramManager.h"

GBufferPass::GBufferPass()
{

}

GBufferPass::~GBufferPass()
{

}

void GBufferPass::commitImpl()
{
    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, position));
    int tcLoc = rut::glVertexAttribPointer("VertexTexcoord0", m_shaderProgram, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, texcoord0));
    int colLoc = rut::glVertexAttribPointer("VertexColor0", m_shaderProgram, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, color));
    int normLoc = rut::glVertexAttribPointer("VertexNormal", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, normal));
#if USE_NORMAL_MAPPING
    int tangentLoc = rut::glVertexAttribPointer("VertexTangent", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, tangent));
    int binormLoc = rut::glVertexAttribPointer("VertexBiNormal", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, binormal));

    rut::glEnableVertexAttribArray(tangentLoc);
    rut::glEnableVertexAttribArray(binormLoc);

#endif
    rut::glEnableVertexAttribArray(posLoc);
    rut::glEnableVertexAttribArray(tcLoc);
    rut::glEnableVertexAttribArray(colLoc);
    rut::glEnableVertexAttribArray(normLoc);

    m_addInfoLoc = m_shaderProgram->getUnsafe("Shininess");
}

void GBufferPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);

    if (m_addInfoLoc != INVALID_ID)
    {
        float shininess = getAttribute<float>(MaterialAttributes::Shininess);

        glo::glUniform1f(m_addInfoLoc, shininess);
    }
}

void GBufferPass::useTexture( uint32_t layer, uint32_t index )
{
    if (!m_textures.empty())
    {
        TexturePtr texture = m_textures[layer][index];
        if (TextureManager::use(texture))
        {
            int tex0loc = m_shaderProgram->getUnsafe("Texture0");
            if (tex0loc > -1)
            {
                glo::glActiveTexture(layer);
                glo::glUniform1i(tex0loc, 0);
            }
        }
    }
}