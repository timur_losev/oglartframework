/*
 * File:   Material.h
 * Author: void
 *
 * Created on March 30, 2013, 10:11 PM
 */

#ifndef MATERIAL_H
#    define	MATERIAL_H

forward_this(RenderTechnique);
forward_this(Material);

class Material
{
public:
    typedef std::vector<RenderTechniquePtr> RenderTechniques_t;
    RenderTechniques_t m_renderTechniques;

    enum
    {
        FFP_TECHNIQUE = 0
    };

private:
    std::string                 name; //optional
public:

    Material(const std::string& name);
    ~Material();

    RenderTechniquePtr getTechnique(size_t index) const;
    size_t getTechniquesCount() const;
    void addTechnique(RenderTechniquePtr, int at = -1);

    const std::string& getName() const;
    void setName(const std::string&);

    void commit();
} ;

#endif	/* MATERIAL_H */

