#pragma once

#include "FCrossThreadInvoker.h"

class FResource : public FCrossThreadInvoker
{
public:
    enum Flags
    {
        FlagError,
        FlagStreamable,
    } ;

protected:

    uint32_t m_flags;

public:

    FResource()
    {

    }

    virtual ~FResource()
    {

    }

    void setFlag(uint32_t flag)
    {
        m_flags = flag;
    }

    virtual bool streamChunk()
    {

    }
};