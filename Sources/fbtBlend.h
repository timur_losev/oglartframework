#pragma once

#include "fbt/Blender/Blender.h"
#include "fbt/fbtFile.h"


class fbtBlend : public fbtFile
{
protected:
    virtual int notifyData(void* p, const Chunk& id) override;
    virtual int initializeTables(fbtBinTables* tables) override;
    virtual bool skip(const uint32_t& id) override;
    virtual int writeData(fbtStream* stream) override;

    uint32_t* m_stripList;

    virtual void*   getFBT(void) override;
    virtual FBTsize getFBTlength(void) override;

public:

    fbtBlend();
    virtual ~fbtBlend();

    fbtList m_scene;
    fbtList m_library;
    fbtList m_object;
    fbtList m_mesh;
    fbtList m_curve;
    fbtList m_mball;
    fbtList m_mat;
    fbtList m_tex;
    fbtList m_image;
    fbtList m_latt;
    fbtList m_lamp;
    fbtList m_camera;
    fbtList m_ipo;
    fbtList m_key;
    fbtList m_world;
    fbtList m_screen;
    fbtList m_script;
    fbtList m_vfont;
    fbtList m_text;
    fbtList m_sound;
    fbtList m_group;
    fbtList m_armature;
    fbtList m_action;
    fbtList m_nodetree;
    fbtList m_brush;
    fbtList m_particle;
    fbtList m_wm;
    fbtList m_gpencil;

    Blender::FileGlobal* m_fileGlobal;

    int save(const char* path, const int mode = PM_UNCOMPRESSED);

    void setIgnoreList(uint32_t *stripList) { m_stripList = stripList; }

};