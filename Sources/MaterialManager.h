/*
 * File:   MaterialManager.h
 * Author: void
 *
 * Created on April 21, 2013, 12:45 PM
 */

#ifndef MATERIALMANAGER_H
#    define	MATERIALMANAGER_H


namespace Collada
{
    forward_this(ColladaParser);
} //Collada

forward_this(Material);
forward_this(TextureManager);
forward_this(ShaderProgram);
forward_this(DeferredMaterialManager);

class MaterialManager
{
private:
    typedef std::unordered_map<std::string, MaterialPtr> Materials_t;
    Materials_t materials;

    TextureManagerPtr       m_textureManager;
    DeferredMaterialManagerPtr m_deferredMaterialManager;

private:

public:
    MaterialManager();
    ~MaterialManager();

    void buildMaterials(Collada::ColladaParserPtr);

    TextureManager&             getTextureManager() const;
    DeferredMaterialManager&    getDeferredMaterialManager() const;

    MaterialPtr getMaterial(const std::string& name) const;
};

#endif	/* MATERIALMANAGER_H */

