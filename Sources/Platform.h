#pragma once

#include <limits.h>

#define SYS_PLATFORM_WINDOWS        1
#define SYS_PLATFORM_LINUX          2
#define SYS_PLATFORM_APPLE_IOS      3
#define SYS_PLATFORM_ANDROID        4
#define SYS_PLATFORM_APPLE_MACOS    5

#ifdef WIN32
#define SYS_PLATFORM SYS_PLATFORM_WINDOWS
#elif __linux__
#define SYS_PLATFORM SYS_PLATFORM_LINUX
#elif __APPLE__
#define SYS_PLATFORM SYS_PLATFORM_APPLE_MACOS
#else
static_assert("Platform error", false);
#endif
#define SYS_PLATFORM_DESKTOP 1


#if SYS_PLATFORM == SYS_PLATFORM_WINDOWS
//#    define u64 unsigned __int64
#    define s64 __int64
#    define l64 u64
#    define ASM_GOOD 0
#    ifndef FORCEINLINE
#        define FORCEINLINE __forceinline
#    endif
#    define SYS_FOPEN _wfopen
#elif SYS_PLATFORM == SYS_PLATFORM_APPLE_MACOS
//#    u64 unsigned long long
#    define s64 long
#    define FORCEINLINE inline
#    define ASM_GOOD 0
#    define SYS_FOPEN fopen
#else // for linux
//#    define u64 unsigned long long
#    define l64 long long
#    define s64 long long
#    define ASM_GOOD 0
#    define FORCEINLINE
#    define SYS_FOPEN fopen
#endif

enum
{
    INVALID_ID = UINT_MAX
};
