/*
 * File:   Texture.h
 * Author: void
 *
 * Created on April 15, 2013, 10:27 PM
 */

#ifndef TEXTURE_H
#    define	TEXTURE_H

struct ImageDimentsion
{
    uint32_t width;
    uint32_t height;
    uint32_t depth;
};

typedef ImageDimentsion ImageDimension_t;

forward_this(Texture);

class Texture
{
public:
    GLuint              texture;
    GLenum              target;
    ImageDimension_t    imageDimentsion;
    GLboolean           isMipmapped;
    bool                isCompressed;
    
#ifdef DEBUG_MODE
    boost::filesystem::path name;
#endif

    Texture();
    ~Texture();

    void bind() const;
};

#endif	/* TEXTURE_H */

