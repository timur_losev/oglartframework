#include "Precompiled.h"
#include "Global.h"

namespace Global
{
    glm::mat4* MatrixPool::CurrentProjView = nullptr;
    glm::mat4* MatrixPool::CurrentProjection = nullptr;
    glm::mat4* MatrixPool::CurrentView = nullptr;

#ifdef AS_SHADER_COMPILER
    const String_t FileSystemMappings::DataPath = "data/";
#else
    const Path_t FileSystemMappings::DataPath = "data";
#endif
    const Path_t FileSystemMappings::TexturesPath = DataPath / "textures";

    const String_t FileSystemMappings::TexturesExt = ".dds";

    const String_t FileSystemMappings::ShadersPath = "shaders/";

    glm::ivec2 Viewport::Dimensions;

    float Viewport::FOV = 70.f;
    float Viewport::Aspect()
    {
        if (Dimensions.x == 0 || Dimensions.y == 0)
        {
            return 1;
        }

        return float(Dimensions.x) / float(Dimensions.y);
    }

    float Viewport::Near = 0.1f;
    float Viewport::Far = 2500.f;


    const String_t FileSystemMappings::DeferredShaders::ShadersPath = FileSystemMappings::ShadersPath + "deferred/";

}