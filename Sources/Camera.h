#pragma once

#include "Node.h"

forward_this(Camera);
forward_this(Frustum);

class Camera
{
public:
    enum CameraBehavior
    {
        CAMERA_BEHAVIOR_FIRST_PERSON,
        CAMERA_BEHAVIOR_FLIGHT
    };

private:
    void rotateFlight(float headingDegrees, float pitchDegrees, float rollDegrees);
    void rotateFirstPerson(float headingDegrees, float pitchDegrees);
    void updateVelocity(const vec3 &direction, float elapsedTimeSec);
    void updateViewMatrix();
    void updateViewProjMatrix();
    void extractFrustum();

    static const float DEFAULT_FOVX;
    static const float DEFAULT_ZFAR;
    static const float DEFAULT_ZNEAR;
    static const vec3 WORLD_XAXIS;
    static const vec3 WORLD_YAXIS;
    static const vec3 WORLD_ZAXIS;

    CameraBehavior m_behavior;
    float m_fovx;
    float m_znear;
    float m_zfar;
    float m_aspectRatio;
    float m_accumPitchDegrees;
    vec3  m_eye;
    vec3  m_xAxis;
    vec3  m_yAxis;
    vec3  m_zAxis;
    vec3  m_viewDir;
    vec3  m_acceleration;
    vec3  m_currentVelocity;
    vec3  m_velocity;
    quat  m_orientation;

    mat4  m_viewMatrix;
    mat4  m_projMatrix;
    mat4  m_viewProjMatrix;

    FrustumPtr m_frustum;

public:

    Camera();
    ~Camera();

    const mat4& getViewProjMatrix() const;

    void lookAt(const vec3 &target);
    void lookAt(const vec3 &eye, const vec3 &target, const vec3 &up);
    void move(float dx, float dy, float dz);
    void move(const vec3 &direction, const vec3 &amount);
    void move(const vec3& translateVec);
    void perspective(float fovx, float aspect, float znear, float zfar);
    void rotate(float headingDegrees, float pitchDegrees, float rollDegrees);
    void updatePosition(const vec3 &direction, float elapsedTimeSec);

    // Getter methods.

    const vec3 &getAcceleration() const;
    CameraBehavior getBehavior() const;
    const vec3 &getCurrentVelocity() const;
    const quat &getOrientation() const;
    const vec3 &getPosition() const;
    const mat4 &getProjectionMatrix() const;
    const vec3 &getVelocity() const;
    const vec3 &getViewDirection() const;
    const mat4 &getViewMatrix() const;
    const vec3 &getXAxis() const;
    const vec3 &getYAxis() const;
    const vec3 &getZAxis() const;

    // Setter methods.

    void setAcceleration(float x, float y, float z);
    void setAcceleration(const vec3 &acceleration);
    void setBehavior(CameraBehavior newBehavior);
    void setCurrentVelocity(float x, float y, float z);
    void setCurrentVelocity(const vec3 &currentVelocity);
    void setOrientation(const quat &orientation);
    void setPosition(float x, float y, float z);
    void setPosition(const vec3 &position);
    void setVelocity(float x, float y, float z);
    void setVelocity(const vec3 &velocity);

    void reshape(float aspectRatio);

    FrustumPtr getFrustum() const;
};

//-----------------------------------------------------------------------------

inline const vec3 &Camera::getAcceleration() const
{ return m_acceleration; }

inline Camera::CameraBehavior Camera::getBehavior() const
{ return m_behavior; }

inline const vec3 &Camera::getCurrentVelocity() const
{ return m_currentVelocity; }

inline const quat &Camera::getOrientation() const
{ return m_orientation; }

inline const vec3 &Camera::getPosition() const
{ return m_eye; }

inline const mat4 &Camera::getProjectionMatrix() const
{ return m_projMatrix; }

inline const vec3 &Camera::getVelocity() const
{ return m_velocity; }

inline const vec3 &Camera::getViewDirection() const
{ return m_viewDir; }

inline const mat4 &Camera::getViewMatrix() const
{ return m_viewMatrix; }

inline const vec3 &Camera::getXAxis() const
{ return m_xAxis; }

inline const vec3 &Camera::getYAxis() const
{ return m_yAxis; }

inline const vec3 &Camera::getZAxis() const
{ return m_zAxis; }