#include "Precompiled.h"
#include "ShaderProgram.h"
#include "ShaderObject.h"
#include "RenderUtils.h"
#include "Logger.h"

#include <limits>

ShaderProgram::ShaderProgram():
m_handle(-1), m_linked(FALSE)
{
}

ShaderProgram::~ShaderProgram()
{
    if (m_linked)
    {
        glo::glDeleteProgram(m_handle);
    }
}

bool ShaderProgram::compile( const char* data, size_t size, ShaderObjectType::Enum shaderType)
{
    ShaderObjectPtr inuse;

    if (shaderType == ShaderObjectType::VERTEX)
    {
        m_vertexShader.reset(new ShaderObject(shaderType));
        inuse = m_vertexShader;
    }
    else if (shaderType == ShaderObjectType::FRAGMENT)
    {
        m_fragmentShader.reset(new ShaderObject(shaderType));
        inuse = m_fragmentShader;
    }

    assert("Empty shader file provided." && size > 0);

    return inuse->compile(data, size);
}

bool ShaderProgram::compile( const std::string& data,ShaderObjectType::Enum e)
{
    return compile(data.c_str(), data.length(), e);
}

bool ShaderProgram::compile( const Path_t& file, ShaderObjectType::Enum shaderType )
{
    assert(boost::filesystem::exists(file));

    FILE* fileHandle = nullptr;

#ifdef WIN32
    fileHandle = _wfopen(file.c_str(), L"rt");
#else
    fileHandle = fopen(file.c_str(), "rt");
#endif

    assert(fileHandle);

    fseek(fileHandle, 0, SEEK_END);
    long size = ftell(fileHandle);
    fseek(fileHandle, 0, SEEK_SET);

    char* buf = new char[size];

    fread(buf, 1, size, fileHandle);

    fclose(fileHandle);

    bool compiled = compile(buf, size, shaderType);

    delete[] buf;

    return compiled;
}

void ShaderProgram::use() const
{
    assert("Using of not linked shader program." && m_linked);

    glo::glUseProgram(m_handle);
}

Bool_t ShaderProgram::link()
{
    Bool_t integrityFailed = FALSE;
    integrityFailed |= m_vertexShader && !m_vertexShader->hasCompiled();
    integrityFailed |= m_fragmentShader && !m_fragmentShader->hasCompiled();

    m_linked = false;

    if (!integrityFailed)
    {
        m_handle = glo::glCreateProgram();

        if (m_vertexShader)
        {
            glo::glAttachShader(m_handle, m_vertexShader->getHandle());
            //m_vertexShader.reset();
        }

        if (m_fragmentShader)
        {
            glo::glAttachShader(m_handle, m_fragmentShader->getHandle());
            //m_fragmentShader.reset();
        }

        glo::glLinkProgram(m_handle);

        m_linked = rut::shaderProgramStatus(m_handle, GL_LINK_STATUS) == GL_TRUE;

        if (m_linked)
        {
            printActiveAttributes();
            printActiveUniforms();
        }
    }

    return m_linked;
}

Bool_t ShaderProgram::hasLinked()
{
    return m_linked;
}

void ShaderProgram::printActiveAttributes()
{
    assert(m_linked);

    GLint maxLen, nAttr;
    glo::glGetProgramiv(m_handle, GL_ACTIVE_ATTRIBUTES, &nAttr);
    glo::glGetProgramiv(m_handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLen);

    GLchar* name = new GLchar[maxLen];

    GLint written, size, location;
    GLenum type;

    for(int i = 0; i < nAttr; ++i)
    {
        glo::glGetActiveAttrib(m_handle, i, maxLen, &written, &size, &type, name);
        location = glo::glGetAttribLocation(m_handle, name);

        m_elements[name] = location;

        OGL_TRACE_DEBUG(GetLoggerPtr(), boost::str(boost::format("Attribute [%s] has index [%d] and type [%d]") % name % location % type) );
    }

    delete[] name;
}

void ShaderProgram::printActiveUniforms()
{
    assert(m_linked);

    GLint maxLen, nUniforms;
    glo::glGetProgramiv(m_handle, GL_ACTIVE_UNIFORMS, &nUniforms);
    glo::glGetProgramiv(m_handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLen);

    GLchar* name = new GLchar[maxLen];

    GLint written, size, location;
    GLenum type;

    for(int i = 0; i < nUniforms; ++i)
    {
        glo::glGetActiveUniform(m_handle, i, maxLen, &written, &size, &type, name);
        location = glo::glGetUniformLocation(m_handle, name);

        m_elements[name] = location;

        OGL_TRACE_DEBUG(GetLoggerPtr(), boost::str(boost::format("Uniform [%s] has index [%d] and type [%d]") % name % location % type) );
    }

    delete[] name;
}

uint32_t ShaderProgram::get( const std::string& elemName ) const
{
    Elements_t::const_iterator it = m_elements.find(elemName);

    if (it == m_elements.end())
    {
        assert("Accessed element not found" && false);
        OGL_TRACE_ERROR(GetLoggerPtr(), "Accessed element not found:" << elemName);
        return std::numeric_limits<uint32_t>::max();
    }

    return it->second;
}

bool ShaderProgram::has( const std::string& elemName ) const
{
    Elements_t::const_iterator it = m_elements.find(elemName);

    if (it == m_elements.end())
    {
        return false;
    }

    return true;
}

int ShaderProgram::getUnsafe( const std::string& elemName ) const
{
    Elements_t::const_iterator it = m_elements.find(elemName);

    if (it == m_elements.end())
    {
        return -1;
    }

    return it->second;
}

bool ShaderProgram::operator==( unsigned int h) const
{
    return m_handle == h;
}

bool ShaderProgram::operator!=( unsigned int h) const
{
    return m_handle != h;
}