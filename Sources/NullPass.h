#pragma once

#include "RenderPass.h"

class NullPass : public RenderPass
{
protected:
    virtual void commitImpl();
public:

    NullPass();
    ~NullPass();

    virtual void use();
};