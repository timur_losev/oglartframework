#include "Precompiled.h"
#include "ScreenQuadRenderPass.h"
#include "ShaderProgram.h"
#include "Vertex.h"
#include "TextureManager.h"
#include "Texture.h"
#include "RenderUtils.h"
#include "ShaderProgramManager.h"


ScreenQuadRenderPass::ScreenQuadRenderPass()
{

}

ScreenQuadRenderPass::~ScreenQuadRenderPass()
{

}

void ScreenQuadRenderPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);

    if (!m_textures.empty())
    {
        TexturePtr positionTex = m_textures[GL_TEXTURE0][0];
        TexturePtr normalTex   = m_textures[GL_TEXTURE1][0];
        TexturePtr colorTex    = m_textures[GL_TEXTURE2][0];
        TexturePtr depthTex    = m_textures[GL_TEXTURE3][0];
        TexturePtr finalTex    = m_textures[GL_TEXTURE4][0];

        if (TextureManager::use(depthTex))
        {
            int depthTexLoc = m_shaderProgram->getUnsafe("DepthTex");
            if (depthTexLoc != -1)
            {
                glo::glUniform1i(depthTexLoc, 0);
            }
        }

        if (TextureManager::use(positionTex))
        {
            int posTexLoc = m_shaderProgram->getUnsafe("PositionTex");
            if (posTexLoc != -1)
            {
                glo::glUniform1i(posTexLoc, 0);
            }
        }

        if (TextureManager::use(normalTex))
        {
            int normalTexLoc = m_shaderProgram->getUnsafe("NormalTex");
            if (normalTexLoc != -1)
            {
                glo::glUniform1i(normalTexLoc, 0);
            }
        }

        if (TextureManager::use(colorTex))
        {
            int colorTexLoc = m_shaderProgram->getUnsafe("ColorTex");
            if (colorTexLoc != -1)
            {
                glo::glUniform1i(colorTexLoc, 0);
            }
        }

        if (TextureManager::use(finalTex))
        {
            uint32_t finalTex = m_shaderProgram->getUnsafe("FinalTex");
            if (finalTex != INVALID_ID)
            {
                glo::glUniform1i(finalTex, 0);
            }
        }
    }
}

void ScreenQuadRenderPass::setTestLightPos( const vec4& p )
{
    m_testLightPos = p;
}

void ScreenQuadRenderPass::commitImpl()
{
    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, position));
    rut::glEnableVertexAttribArray(posLoc);
}
