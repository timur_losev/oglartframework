#pragma once

#include "LazySingleton.h"

forward_this(Scene);
forward_this(Renderer);
forward_this(InputListener);
class Logger;

class Application: public LazySingleton<Application>
{
private:

    RendererPtr        renderer;
    InputListenerPtr   inputListener;

    ScenePtr           scene;
    Logger*            m_logger;

    unsigned long      hwnd;
    
    GLFWwindow*        m_window = nullptr;

private:
    void setupLogger();
    void setupFrontEnd(int argc, char* argv[]);
    void setupSystemStuff();

    void onDisplay();
    void onIdle();
    void onReshape(GLFWwindow*, int x, int y);
    void onKey(GLFWwindow* window, int key, int scancode, int action, int mods);

    void printExtensions();

    static void sOnDisplay();
    static void sOnIdle();
    static void sOnReshape(GLFWwindow* , int x, int y);
    static void sOnKey(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void WINAPI sOnGLDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);

public:
    Application();
    ~Application();

    void setup(int argc, char* argv[]);
    void run();

    //cast to whatever you want
    unsigned long getWindowHandle() const;
};