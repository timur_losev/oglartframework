#include "Precompiled.h"
#include "GeometryBuffer.h"
#include "Texture.h"

GeometryBuffer::GeometryBuffer(void)
{
    m_deferredFBO = 0;
    m_depthBuf = 0;
}

GeometryBuffer::~GeometryBuffer(void)
{
}

#define FINAL_COLOR_ATTACHMENT GL_COLOR_ATTACHMENT4
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

bool GeometryBuffer::create(const ivec2& dimenstion)
{
    m_dimension = dimenstion;
    // Create and bind FBO
    glo::glGenFramebuffers(1, &m_deferredFBO);
    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_deferredFBO);

    glGenTextures(ARRAY_SIZE_IN_ELEMENTS(m_renderBuffers), m_renderBuffers);

    m_depthTex.reset(new Texture());
    m_depthTex->target = GL_TEXTURE_2D;
    m_depthTex->imageDimentsion.width = m_dimension.x;
    m_depthTex->imageDimentsion.height = m_dimension.y;
    m_depthTex->imageDimentsion.depth = 24;

    glGenTextures(1, &m_depthTex->texture);
    glGenTextures(1, &m_finalTex);

    for(uint32_t i = 0; i < ARRAY_SIZE_IN_ELEMENTS(m_renderBuffers); ++i)
    {
        glBindTexture(GL_TEXTURE_2D, m_renderBuffers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, dimenstion.x, dimenstion.y, 0, GL_RGBA, GL_FLOAT, nullptr);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glo::glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, m_renderBuffers[i], 0);
    }

    //! Depth
    glBindTexture(GL_TEXTURE_2D, m_depthTex->texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH32F_STENCIL8, dimenstion.x, dimenstion.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

    glo::glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_depthTex->texture, 0);

    //! Final
    glBindTexture(GL_TEXTURE_2D, m_finalTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, dimenstion.x, dimenstion.y, 0, GL_RGBA, GL_FLOAT, nullptr);
    glo::glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, FINAL_COLOR_ATTACHMENT, GL_TEXTURE_2D, m_finalTex, 0);

    m_fboTextures.resize(TOTAL_RENDER_BUFFERS);

    for (size_t i = 0; i < ARRAY_SIZE_IN_ELEMENTS(m_renderBuffers); ++i)
    {
        TexturePtr texture(new Texture());
        texture->target = GL_TEXTURE_2D;
        texture->texture = m_renderBuffers[i];
        m_fboTextures[i] = texture;
    }

    uint32_t status = glo::glCheckFramebufferStatus(GL_FRAMEBUFFER);

    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    return status == GL_FRAMEBUFFER_COMPLETE;
}

void GeometryBuffer::startFrame()
{
    if (m_deferredFBO)
    {
        glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_deferredFBO);
        //glDrawBuffer(FINAL_COLOR_ATTACHMENT);
        glClear(GL_COLOR_BUFFER_BIT);
    }
}

void GeometryBuffer::unbind()
{
    glo::glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

const GeometryBuffer::FBOTextures_t& GeometryBuffer::getFBOTextures() const
{
    return m_fboTextures;
}

void GeometryBuffer::bindForGeometryPass()
{
    if (m_deferredFBO)
    {
        glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_deferredFBO);

        GLenum DrawBuffers[] =
        {   GL_COLOR_ATTACHMENT0, //! Mrt0 : color
            GL_COLOR_ATTACHMENT1, //! Mrt1 : normal
            GL_COLOR_ATTACHMENT2  //! Depth-Stencil
        };

        glo::glDrawBuffers(ARRAY_SIZE_IN_ELEMENTS(DrawBuffers), DrawBuffers);
    }
}

void GeometryBuffer::bindForStencilPass()
{
    glDrawBuffer(GL_NONE);
}

void GeometryBuffer::bindForLightPass()
{
    glDrawBuffer(FINAL_COLOR_ATTACHMENT);

    for (uint32_t i = 0; i < ARRAY_SIZE_IN_ELEMENTS(m_renderBuffers); ++i)
    {
        glo::glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, m_renderBuffers[i]);
    }
}

void GeometryBuffer::bindForFinalPass()
{
    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glo::glBindFramebuffer(GL_READ_FRAMEBUFFER, m_deferredFBO);
    glReadBuffer(FINAL_COLOR_ATTACHMENT);
}

TexturePtr GeometryBuffer::getDepthTexture() const
{
    return m_depthTex;
}
