/*
 * File:   Node.h
 * Author: void
 *
 * Created on April 6, 2013, 6:31 PM
 */

#ifndef NODE_H
#    define	NODE_H

forward_this_w(Node);
forward_this(Node);
forward_this(GraphicMesh);
forward_this(Light);

class Node: public std::enable_shared_from_this<Node>
{
    friend class Scene;
    friend class MeshManager;
public:
    typedef std::list<NodePtr> Children_t;
    typedef std::vector<GraphicMeshPtr> Meshes_t;

    enum ETransformSpace
    {
        /// Transform is relative to the local space
        ETS_LOCAL,
        /// Transform is relative to the space of the parent node
        ETS_PARENT,
        /// Transform is relative to world space
        ETS_WORLD
    };

    enum ENodeType
    {
        ENT_NONE,
        ENT_LIGHT
    };

protected:
    std::string m_name;

    Children_t  children;
    Meshes_t    meshes;

    NodeWeakPtr parent;

    glm::mat4   m_transformation;
    vec3        m_position;
    ENodeType   m_nodeType;

protected:

public:

    Node(const std::string& name);
    
    ~Node();

    void                        setName(const std::string& n) { m_name = n; }
    const std::string&          getName() const { return m_name; }

    bool                        hasChild(NodePtr) const;
    bool                        hasChild(const std::string& name) const;
    void                        addChild(NodePtr);
    NodeConstPtr                getChild(const std::string& name) const;
    NodePtr                     getChild(const std::string& name);
    NodeConstWeakPtr            getParent() const { return parent; }

    const Children_t&           getChildren() const { return children; }

    void                        addMesh(GraphicMeshPtr);
    GraphicMeshConstPtr         getMesh(std::string name) const;
    const Meshes_t&             getMeshes() const { return meshes; }
    Meshes_t&                   getMeshes() { return meshes; }

    void                        updateTransform(const glm::mat4& vp);

    const glm::mat4&            getTransform() const { return m_transformation; }
    //glm::mat4&                  getTransform() { return m_transformation; }
    void                        setTransform(const glm::mat4& t);


    const vec3&                 getPosition() const;
    void                        setPosition(const vec3& pos);
    void                        translate(const vec3& pos /*TODO: relation*/);


    ENodeType                   getNodeType() const;

    //! Interpretation
    LightPtr                    asLightNode();
    LightConstPtr               asLightNode() const;
};

#endif	/* NODE_H */

