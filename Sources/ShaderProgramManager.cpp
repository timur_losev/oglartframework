#include "Precompiled.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"

#include "Global.h"

ShaderProgramManager::ShaderProgramManager()
{

}

ShaderProgramManager::~ShaderProgramManager()
{

}

bool ShaderProgramManager::use( ShaderProgramConstPtr program)
{
    GLint id;
    glGetIntegerv(GL_CURRENT_PROGRAM, &id);

    if (*program != id)
    {
        program->use();
    }
    return true;
}

ShaderProgramPtr ShaderProgramManager::constructGeometryPassDeferredProgram()
{
    ShaderProgramPtr geometryPass(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    boost::filesystem::path shaderPath = shaderFolder / "geometry_passVs.h";

    assert(geometryPass->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "geometry_passPs.h";

    assert(geometryPass->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(geometryPass->link());

    return geometryPass;
}

ShaderProgramPtr ShaderProgramManager::constructNullPassDeferredProgram()
{
    ShaderProgramPtr nullPassProgram(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    Path_t shaderPath = shaderFolder / "null_passVs.h";

    assert(nullPassProgram->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "null_passPs.h";

    assert(nullPassProgram->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(nullPassProgram->link());

    return nullPassProgram;
}

ShaderProgramPtr ShaderProgramManager::constructPointLightPassDeferredProgram()
{
    ShaderProgramPtr lightPassProgram(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    Path_t  shaderPath = shaderFolder / "point_light_passVs.h";

    assert(lightPassProgram->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "point_light_passPs.h";

    assert(lightPassProgram->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(lightPassProgram->link());

    return lightPassProgram;
}

ShaderProgramPtr ShaderProgramManager::constructDirectionalPassDeferredProgram()
{
    ShaderProgramPtr lightPassProgram(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    Path_t shaderPath = shaderFolder / "directional_light_passVs.h";

    assert(lightPassProgram->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "directional_light_passPs.h";

    assert(lightPassProgram->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(lightPassProgram->link());

    return lightPassProgram;
}

ShaderProgramPtr ShaderProgramManager::constructSimpleForwardProgram()
{
    ShaderProgramPtr program(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::ShadersPath;

    boost::filesystem::path shaderPath = shaderFolder / "ForwardVs.h";

    assert(program->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "ForwardPs.h";

    assert(program->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(program->link());

    return program;
}

ShaderProgramPtr ShaderProgramManager::constructObjectOverlayProgram()
{
    ShaderProgramPtr program(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    Path_t shaderPath = shaderFolder / "overlay_pass_Vs.h";

    assert(program->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "overlay_pass_Ps.h";

    assert(program->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(program->link());

    return program;
}

ShaderProgramPtr ShaderProgramManager::constructSpotLightPassDeferredProgram()
{
    ShaderProgramPtr program(new ShaderProgram());

    Path_t shaderFolder = Global::FileSystemMappings::DataPath /
        Global::FileSystemMappings::DeferredShaders::ShadersPath;

    Path_t shaderPath = shaderFolder / "spot_light_passVs.h";

    assert(program->compile(shaderPath, ShaderObjectType::VERTEX));

    shaderPath = shaderFolder / "spot_light_passPs.h";

    assert(program->compile(shaderPath, ShaderObjectType::FRAGMENT));

    assert(program->link());

    return program;
}
