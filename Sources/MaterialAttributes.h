/*
 * File:   MaterialAttributes.h
 * Author: void
 *
 * Created on April 20, 2013, 11:36 PM
 */

#ifndef MATERIALATTRIBUTES_H
#define MATERIALATTRIBUTES_H

class MaterialAttributes
{
public:

    enum Enum
    {
        DiffuseColor,
        SpecularColor,
        AmbientColor,
        EmissiveColor,
        NameString,
        Shininess,
        ShadeType
    } ;

    static std::string toString(Enum val)
    {
        switch (val)
        {
        case DiffuseColor: return "DiffuseColor";
        case SpecularColor: return "SpecularColor";
        case AmbientColor: return "AmbientColor";
        case EmissiveColor: return "EmissiveColor";
        case NameString: return "NameString";
        case Shininess: return "Shininess";
        case ShadeType: return "ShadeType";
        default: assert(false);
            return "";
        }
    }
} ;

#endif	/* MATERIALATTRIBUTES_H */

