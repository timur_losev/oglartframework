#pragma once

#include <boost/variant.hpp>
#include "Adaptation.h"
#include "MaterialAttributes.h"

#include "UniformAccessors.h"

#include "Logger.h"

forward_this(ShaderProgram);
forward_this(Texture);
forward_this(RenderPass);
class Logger;

class RenderPass
{
public:
    typedef std::vector<TexturePtr> Textures_t;
    typedef std::map<uint32_t, Textures_t> LayredTextures_t;

    typedef std::map<MaterialAttributes::Enum,
        boost::variant<
        Color_t,
        float,
        String_t,
        Bool_t,
        uint32_t
        >
    > AttributesMap_t;

private:

protected:
    ShaderProgramPtr            m_shaderProgram;

    LayredTextures_t            m_textures;
    mutable AttributesMap_t     m_attributesMap;

    uint32_t                    m_id;

    uint32_t                    m_uniformAccessors[UniformAccessors::TOTAL];
    
    Logger*                     m_logger;

protected:

    virtual void  commitImpl() = 0;

public:

    RenderPass();
    virtual ~RenderPass();

    template<class T>
    const T& getAttribute(MaterialAttributes::Enum id) const
    {
        assert(m_attributesMap.find(id) != m_attributesMap.end());

        return boost::get<T>(m_attributesMap[id]);
    }

    template<class T>
    void setAttribute(MaterialAttributes::Enum id, const T& value)
    {
        // stream operator must be defined anyway
        OGL_TRACE_DEBUG(m_logger, "Set attribute with id " << MaterialAttributes::toString(id) /*<< " and value " << value*/);

        m_attributesMap[id] = value;
    }

    void setTexture(uint32_t layer, TexturePtr texture);

    const LayredTextures_t& getTextures() const;

    void                dumpAttributes();

    void                setShaderProgram(ShaderProgramPtr program);
    ShaderProgramPtr    getShaderProgram() const;

    void                setId(uint32_t id);
    uint32_t            getId() const;


    virtual void        use() {}
    virtual void        switchToIt();

    virtual void        commit();

    void                setUniform(const mat4& v, UniformAccessors::Enum);
    void                setUniform(uint32_t v, UniformAccessors::Enum);
    void                setUniform(float v, UniformAccessors::Enum);
    void                setUniform(const vec3& v, UniformAccessors::Enum);
    void                setUniform(const vec4& v, UniformAccessors::Enum);
    void                setUniform(const vec2& v, UniformAccessors::Enum);
};
