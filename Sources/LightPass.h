#pragma once

#include "RenderPass.h"

forward_this(Light);
forward_this(LightPass);

class LightPass : public RenderPass
{
protected:
    struct BaseLight
    {
        uint32_t Color;
        uint32_t AmbientIntensity;
        uint32_t DiffuseIntensity;

        BaseLight():
            Color(INVALID_ID),
            AmbientIntensity(INVALID_ID),
            DiffuseIntensity(INVALID_ID)
        {}
    } m_baseLightLoc;

protected:
    virtual void commitImpl();

    float m_ambientIntensity;
    float m_diffuseIntensity;
public:

    LightPass();
    virtual ~LightPass();

    virtual void use(LightConstPtr light);

    virtual void use();
};