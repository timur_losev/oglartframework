#include "Precompiled.h"

#include "ScreenQuad.h"
#include "GraphicMesh.h"
#include "Material.h"

#include "DeferredMaterialManager.h"

#include "Global.h"

ScreenQuad::ScreenQuad(DeferredMaterialManager& dmMgr)
{
    static const vec3 g_quad_vertex_buffer_data[] = {
        vec3 (-1.0f, -1.0f, 0.0f),
        vec3 (1.0f, -1.0f, 0.0f),
        vec3 (-1.0f,  1.0f, 0.0f),
        vec3 (-1.0f,  1.0f, 0.0f),
        vec3 (1.0f, -1.0f, 0.0f),
        vec3 (1.0f,  1.0f, 0.0f)
    };

    m_quadMesh.reset(new GraphicMesh());

    for(uint32_t i = 0; i < 6; ++i)
    {
        Vertex v;
        v.position = g_quad_vertex_buffer_data[i];
        m_quadMesh->vertices.push_back(v);
    }

    m_quadMesh->setMaterial(dmMgr.createScreenQuadMaterial("QuadMat"));

    assert(m_quadMesh->commit());
}

ScreenQuad::~ScreenQuad()
{

}

void ScreenQuad::render()
{
    //GraphicMesh::unbindAll();
    //m_quadMesh->bindVAO();
    //m_quadMesh->bindVBO();

    //m_renderPass->use();

    glDrawArrays(GL_TRIANGLES, 0, 6);
}
