#pragma once

#include "HardwareElement.h"
#include "Mesh.h"

forward_this(Material);

class GraphicMesh:
    public HardwareElement, public Mesh
{
protected:
    MaterialPtr m_material; //Should it be a weak?
public:

    GraphicMesh();
    virtual ~GraphicMesh();

    std::string name;

    virtual bool            commit(GLuint usage = GL_STATIC_DRAW);

    void                    setMaterial(MaterialPtr mat);
    MaterialPtr             getMaterial();
    MaterialConstPtr        getMaterial() const;
};
