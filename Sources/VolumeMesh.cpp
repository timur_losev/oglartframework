#include "Precompiled.h"
#include "VolumeMesh.h"
#include "RenderUtils.h"
#include "Material.h"

VolumeMesh::VolumeMesh()
{

}

VolumeMesh::~VolumeMesh()
{

}

bool VolumeMesh::commit( GLuint usage /*= GL_STATIC_DRAW*/ )
{
    assert(checkVirginity());

    if (!vertices.empty())
    {
        glo::glGenVertexArrays(1, &m_vboHandles[VAO]);
        glo::glGenBuffers(1, &m_vboHandles[VBO]);

        bindVAO();
        bindVBO();

        rut::glBufferData(GL_ARRAY_BUFFER, vertices, usage);

        if (!indices.empty())
        {
            glo::glGenBuffers(1, &m_vboHandles[IBO]);
            bindIBO();

            rut::glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, usage);

        }

        if (m_material)
        {
            m_material->commit();
        }

        unbindAll();

        return true;
    }

    return false;
}
