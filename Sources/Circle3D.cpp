#include "Precompiled.h"
#include "Circle3D.h"
#include "Material.h"

#include "RenderUtils.h"

Circle3D::Circle3D( const vec3& origin, float radius, size_t segm, const Color_t& col )
{
    assert(segm > 2);

    segments.resize(segm);

    for(size_t i = 0; i < segm; ++i)
    {
        float angle = 2.f * glm::pi<float>() * float(i) / float(segm);

        float x = radius * glm::cos(angle);
        float z = radius * glm::sin(angle);

        segments[i].position = vec3(x, 0.f, z) + origin;
        segments[i].color = col;
    }
}

bool Circle3D::commit(GLenum usage)
{
    assert(checkVirginity());

    glo::glGenVertexArrays(1, &m_vboHandles[VAO]);
    glo::glBindVertexArray(m_vboHandles[VAO]);

    glo::glGenBuffers(1, &m_vboHandles[VBO]);
    glo::glBindBuffer(GL_ARRAY_BUFFER, m_vboHandles[VBO]);

    rut::glBufferData(GL_ARRAY_BUFFER, segments, usage);

    assert(m_material);

    m_material->commit();

    unbindAll();

    return true;
}

void Circle3D::setMaterial( MaterialPtr mat )
{
    m_material = mat;
}

MaterialPtr Circle3D::getMaterial()
{
    return m_material;
}

MaterialConstPtr Circle3D::getMaterial() const
{
    return m_material;
}
