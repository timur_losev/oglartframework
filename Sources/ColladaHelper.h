/*
 * File:   ColladaHelper.h
 * Author: void
 *
 * Created on March 30, 2013, 7:52 PM
 */

#ifndef COLLADAHELPER_H
#    define	COLLADAHELPER_H

#include "Adaptation.h"
#include "Material.h"

namespace Collada
{

    forward_this_s(Controller);
    forward_this_s(Data);
    forward_this_s(Camera);
    forward_this_s(Light);
    forward_this_ws(Node);
    forward_this_s(Node);
    forward_this_s(Accessor);
    forward_this_s(Material);
    forward_this_s(Effect);
    forward_this_s(Image);
    forward_this_s(AnimationChannel);
    forward_this_s(Animation);

    /** Collada file versions which evolved during the years ... */
    enum EFormatVersion
    {
        FV_1_5_n,
        FV_1_4_n,
        FV_1_3_n
    } ;

    /** Transformation types that can be applied to a node */
    enum ETransformType
    {
        TF_LOOKAT,
        TF_ROTATE,
        TF_TRANSLATE,
        TF_SCALE,
        TF_SKEW,
        TF_MATRIX
    } ;

    /** Different types of input data to a vertex or face */
    enum EInputType
    {
        IT_Invalid,
        IT_Vertex,  // special type for per-index data referring to the <vertices> element carrying the per-vertex data.
        IT_Position,
        IT_Normal,
        IT_Texcoord,
        IT_Color,
        IT_Tangent,
        IT_Bitangent
    } ;

    struct IT_ToString
    {
        std::string operator()(EInputType e) const
        {
            switch(e)
            {
            case IT_Invalid: return "IT_Invalid";
            case IT_Vertex: return "IT_Vertex";
            case IT_Position: return "IT_Position";
            case IT_Normal: return "IT_Normal";
            case IT_Texcoord: return "IT_Texcoord";
            case IT_Color: return "IT_Color";
            case IT_Tangent: return "IT_Tangent";
            case IT_Bitangent: return "IT_Bitangent";
            };

            return "";
        }
    };

    /** Contains all data for one of the different transformation types */
    struct Transform
    {
        std::string ID;  ///< SID of the transform step, by which anim channels address their target node
        ETransformType type;
        float f[16]; ///< Interpretation of data depends on the type of the transformation
    } ;


    /** A collada camera. */
    struct Camera
    {
        // Name of camera
        std::string name;

        // True if it is an orthografic camera
        bool ortho;

        //! Horizontal field of view in degrees
        float horFov;

        //! Vertical field of view in degrees
        float verFov;

        //! Screen aspect
        float aspect;

        //! Near& far z
        float ZNear, ZFar;


        Camera()
        :	ortho  (false)
        ,	horFov (10e10f)
        ,	verFov (10e10f)
        ,	aspect (10e10f)
        ,	ZNear  (0.1f)
        ,	ZFar   (1000.f)
        {
        }
    } ;

static const uint32_t aiLightSource_AMBIENT = 0xdeaddead;
static const float _COLLADA_LIGHT_ANGLE_NOT_SET = 1e9f; //huge value

    /** A collada light source. */
    struct Light
    {
        //! Type of the light source aiLightSourceType + ambient
        unsigned int type;

        //! Color of the light
        Color_t color;

        //! Light attenuation
        float attConstant, attLinear, attQuadratic;

        //! Spot light falloff
        float falloffAngle;
        float falloffExponent;

        // -----------------------------------------------------
        // FCOLLADA extension from here

        //! ... related stuff from maja and max extensions
        float penumbraAngle;
        float outerAngle;

        //! Common light intensity
        float intensity;

        // Blender profile
        float dist;

        unsigned int falloffType;

        Light()
        :   attConstant     (1.f)
        ,   attLinear       (0.f)
        ,   attQuadratic    (0.f)
        ,   falloffAngle    (180.f)
        ,   falloffExponent (0.f)
        ,   penumbraAngle   (_COLLADA_LIGHT_ANGLE_NOT_SET)
        ,   outerAngle      (_COLLADA_LIGHT_ANGLE_NOT_SET)
        ,   intensity       (1.f)
        ,   dist(1.f)
        ,   falloffType     (0)
        {
        }

    } ;

    /** Short vertex index description */
    struct InputSemanticMapEntry
    {
        //! Index of set, optional
        unsigned int set;

        //! Name of referenced vertex input
        EInputType type;


        InputSemanticMapEntry()
        :	set	(0)
        {
        }
    } ;

    /** Table to map from effect to vertex input semantics */
    struct SemanticMappingTable
    {
        //! Name of material
        std::string matName;

        //! List of semantic map commands, grouped by effect semantic name
        std::unordered_map<std::string, InputSemanticMapEntry> table;

        //! For std::find

        bool operator == (const std::string& s) const
        {
            return s == matName;
        }
    } ;

    /** A reference to a mesh inside a node, including materials assigned to the various subgroups.
     * The ID refers to either a mesh or a controller which specifies the mesh
     */
    struct MeshInstance
    {
        ///< ID of the mesh or controller to be instanced
        std::string meshOrController;

        ///< Map of materials by the subgroup ID they're applied to
        std::unordered_map<std::string, SemanticMappingTable> materials;
    } ;

    /** A reference to a camera inside a node*/
    struct CameraInstance
    {
        ///< ID of the camera
        std::string camera;
    } ;

    /** A reference to a light inside a node*/
    struct LightInstance
    {
        ///< ID of the camera
        std::string light;
    } ;

    /** A reference to a node inside a node*/
    struct NodeInstance
    {
        ///< ID of the node
        std::string node;
    } ;

    /** A node in a scene hierarchy */
    struct Node
    {
        std::string name;
        std::string ID;
        std::string SID;
        NodeWeakPtr parent;

        typedef std::list<NodePtr> Children_t;
        Children_t children;

        /** Operations in order to calculate the resulting transformation to parent. */
        std::vector<Transform> transforms;

        /** Meshes at this node */
        std::vector<MeshInstance> meshes;

        /** Lights at this node */
        std::vector<LightInstance> lights;

        /** Cameras at this node */
        std::vector<CameraInstance> cameras;

        /** Node instances at this node */
        std::vector<NodeInstance> nodeInstances;

        /** Rootnodes: Name of primary camera, if any */
        std::string primaryCamera;

        //! Constructor. Begin with a zero parent

        Node()
        {
        }

        //! Destructor: delete all children subsequently
        ~Node()
        {
            //Warning: Children deletion is undefined
        }
    } ;

    /** Data source array: either floats or strings */
    struct Data
    {
        bool isStringArray;
        float* values;
        std::vector<std::string> strings;

        Data() : values(nullptr) {}

        ~Data()
        {
            delete[] values;
        }
    } ;

    /** Accessor to a data array */
    struct Accessor
    {
        size_t count;   // in number of objects
        size_t size;    // size of an object, in elements (floats or strings, mostly 1)
        size_t offset;  // in number of values
        size_t stride;  // Stride in number of values
        std::vector<std::string> params; // names of the data streams in the accessors. Empty string tells to ignore.
        size_t subOffset[4]; // Suboffset inside the object for the common 4 elements. For a vector, thats XYZ, for a color RGBA and so on.
        // For example, SubOffset[0] denotes which of the values inside the object is the vector X component.
        std::string source;   // URL of the source array
        mutable DataConstPtr data; // Pointer to the source array, if resolved. NULL else

        Accessor()
        {
            count = 0;
            size = 0;
            offset = 0;
            stride = 0;
            data = nullptr;
            subOffset[0] = subOffset[1] = subOffset[2] = subOffset[3] = 0;
        }
    } ;

    /** A single face in a mesh */
    struct Face
    {
        std::vector<size_t> indices;
    } ;

    /** An input channel for mesh data, referring to a single accessor */
    struct InputChannel
    {
        EInputType type;      // Type of the data
        size_t index;		  // Optional index, if multiple sets of the same data type are given
        size_t offset;       // Index offset in the indices array of per-face indices. Don't ask, can't explain that any better.
        std::string accessor; // ID of the accessor where to read the actual values from.
        mutable AccessorConstPtr resolved; // Pointer to the accessor, if resolved. NULL else

        InputChannel()
        {
            type = IT_Invalid;
            index = 0;
            offset = 0;
        }
    } ;

    /** Subset of a mesh with a certain material */
    struct SubMesh
    {
        std::string material; ///< subgroup identifier
        size_t numFaces; ///< number of faces in this submesh
    } ;

    /** Contains data for a single mesh */
    struct Mesh
    {
        // just to check if there's some sophisticated addressing involved...
        // which we don't support, and therefore should warn about.
        std::string vertexID;

        // Vertex data addressed by vertex indices
        std::vector<InputChannel> perVertexData;

        // actual mesh data, assembled on encounter of a <p> element. Verbose format, not indexed
        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec3> tangents;
        std::vector<glm::vec3> bitangents;
        std::vector<glm::vec3> texCoords[_MAX_NUMBER_OF_TEXTURECOORDS];
        std::vector<glm::vec4>  colors[_MAX_NUMBER_OF_COLOR_SETS];

        unsigned int numUVComponents[_MAX_NUMBER_OF_TEXTURECOORDS];

        // Faces. Stored are only the number of vertices for each face.
        // 1 == point, 2 == line, 3 == triangle, 4+ == poly
        std::vector<size_t> faceSize;

        // Position indices for all faces in the sequence given in mFaceSize -
        // necessary for bone weight assignment
        std::vector<size_t> facePosIndices;

        // Submeshes in this mesh, each with a given material
        std::vector<SubMesh> subMeshes;


        Mesh()
        {
            for (unsigned int i = 0; i < _MAX_NUMBER_OF_TEXTURECOORDS; ++i)
                numUVComponents[i] = 2;
        }

    } ;

    /** Which type of primitives the ReadPrimitives() function is going to read */
    enum PrimitiveType
    {
        Prim_Invalid,
        Prim_Lines,
        Prim_LineStrip,
        Prim_Triangles,
        Prim_TriStrips,
        Prim_TriFans,
        Prim_Polylist,
        Prim_Polygon
    } ;

    /** A skeleton controller to deform a mesh with the use of joints */
    struct Controller
    {
        // the URL of the mesh deformed by the controller.
        std::string meshId;

        // accessor URL of the joint names
        std::string jointNameSource;

        ///< The bind shape matrix, as array of floats. I'm not sure what this matrix actually describes, but it can't be ignored in all cases
        float bindShapeMatrix[16];

        // accessor URL of the joint inverse bind matrices
        std::string jointOffsetMatrixSource;

        // input channel: joint names.
        InputChannel weightInputJoints;
        // input channel: joint weights
        InputChannel weightInputWeights;

        // Number of weights per vertex.
        std::vector<size_t> weightCounts;

        // JointIndex-WeightIndex pairs for all vertices
        std::vector< std::pair<size_t, size_t> > weights;
    } ;

    /** A collada material. Pretty much the only member is a reference to an effect. */
    struct Material
    {
        std::string effect;
    };

    /** Type of the effect param */
    enum ParamType
    {
        Param_Sampler,
        Param_Surface
    } ;

    /** A param for an effect. Might be of several types, but they all just refer to each other, so I summarize them */
    struct EffectParam
    {
        ParamType type;
        std::string reference; // to which other thing the param is referring to.
    } ;

    /** Shading type supported by the standard effect spec of Collada */
    enum ShadeType
    {
        Shade_Invalid,
        Shade_Constant,
        Shade_Lambert,
        Shade_Phong,
        Shade_Blinn
    } ;

    /** Represents a texture sampler in collada */
    struct Sampler
    {
        /** Name of image reference
         */
        std::string name;

        /** Wrap U?
         */
        bool wrapU;

        /** Wrap V?
         */
        bool wrapV;

        /** Mirror U?
         */
        bool mirrorU;

        /** Mirror V?
         */
        bool mirrorV;

        /** Blend mode
         */
        ETextureOp op;

        /** UV transformation
         */
        UVTransform transform;

        /** Name of source UV channel
         */
        std::string UVChannel;

        /** Resolved UV channel index or UINT_MAX if not known
         */
        unsigned int UVId;

        // OKINO/MAX3D extensions from here
        // -------------------------------------------------------

        /** Weighting factor
         */
        float weighting;

        /** Mixing factor from OKINO
         */
        float mixWithPrevious;


        Sampler()
        :	wrapU		(true)
        ,	wrapV		(true)
        ,	mirrorU	()
        ,	mirrorV	()
        ,	op			(TextureOp_Multiply)
        ,	UVId		(UINT_MAX)
        ,	weighting  (1.f)
        ,	mixWithPrevious (1.f)
        {
        }
    } ;

    /** A collada effect. Can contain about anything according to the Collada spec,
        but we limit our version to a reasonable subset. */
    struct Effect
    {
        // Shading mode
        ShadeType shadeType;

        // Colors
        Color_t emissive, ambient, diffuse, specular,
                transparent, reflective;

        // Textures
        Sampler texEmissive, texAmbient, texDiffuse, texSpecular,
                texTransparent, texBump, texReflective;

        // Scalar factory
        float shininess, refractIndex, reflectivity;
        float transparency;

        // local params referring to each other by their SID
        typedef std::map<std::string, Collada::EffectParam> ParamLibrary_t;
        ParamLibrary_t params;

        // MAX3D extensions
        // ---------------------------------------------------------
        // Double-sided?
        bool doubleSided, wireframe, faceted;

        Effect()
        : shadeType    (Shade_Phong)
        , emissive		( 0, 0, 0, 1)
        , ambient		( 0.1f, 0.1f, 0.1f, 1)
        , diffuse		( 0.6f, 0.6f, 0.6f, 1)
        , specular		( 0.4f, 0.4f, 0.4f, 1)
        , transparent	( 0, 0, 0, 1)
        , shininess    (10.0f)
        , refractIndex (1.f)
        , reflectivity (1.f)
        , transparency (0.f)
        , doubleSided	(false)
        , wireframe    (false)
        , faceted      (false)
        {
        }
    } ;

    /** An image, meaning texture */
    struct Image
    {
        Path_t fileName;

        /** If image file name is zero, embedded image data
         */
        std::vector<uint8_t> imageData;

        /** If image file name is zero, file format of
         *  embedded image data.
         */
        std::string embeddedFormat;

    } ;

    /** An animation channel. */
    struct AnimationChannel
    {
        /** URL of the data to animate. Could be about anything, but we support only the
         * "NodeID/TransformID.SubElement" notation
         */
        std::string target;

        /** Source URL of the time values. Collada calls them "input". Meh. */
        std::string sourceTimes;
        /** Source URL of the value values. Collada calls them "output". */
        std::string sourceValues;
    } ;

    /** An animation. Container for 0-x animation channels or 0-x animations */
    struct Animation
    {
        /** Anim name */
        std::string name;

        /** the animation channels, if any */
        std::vector<AnimationChannel> channels;

        /** the sub-animations, if any */
        std::vector<AnimationPtr> subAnims;

        /** Destructor */
        ~Animation()
        {
//            for ( std::vector<Animation*>::iterator it = mSubAnims.begin(); it != mSubAnims.end(); ++it)
//                delete *it;
        }
    } ;

    /** Description of a collada animation channel which has been determined to affect the current node */
    struct ChannelEntry
    {
        AnimationChannelConstPtr        channel; ///> the source channel
        std::string                     transformId;   // the ID of the transformation step of the node which is influenced
        size_t                          transformIndex; // Index into the node's transform chain to apply the channel to
        size_t                          subElement; // starting index inside the transform data

        // resolved data references
        AccessorConstPtr                timeAccessor; ///> Collada accessor to the time values
        DataConstPtr                    timeData; ///> Source data array for the time values
        AccessorConstPtr                valueAccessor; ///> Collada accessor to the key value values
        DataConstPtr                    valueData; ///> Source datat array for the key value values

        ChannelEntry()
        {
            subElement = 0;
        }
    } ;


} //Collada;

#endif	/* COLLADAHELPER_H */

