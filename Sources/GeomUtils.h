#pragma once

forward_this(Mesh);
forward_this(VolumeMesh);

namespace GeomUtils
{
    void CreateLightCone(VolumeMeshPtr mesh, float radius, float height, int nVerticesInBase);
    void CreateLightSphere(VolumeMeshPtr mesh, float radius, uint32_t nSegments, uint32_t nRings);
};