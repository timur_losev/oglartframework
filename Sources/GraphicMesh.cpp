#include "Precompiled.h"
#include "GraphicMesh.h"
#include "RenderUtils.h"
#include "Material.h"

bool GraphicMesh::commit(GLenum usage)
{
    assert(checkVirginity());

    if (!vertices.empty())
    {
        glo::glGenVertexArrays(1, &m_vboHandles[VAO]);

        bindVAO();

        glo::glGenBuffers(1, &m_vboHandles[VBO]);

        bindVBO();

        rut::glBufferData(GL_ARRAY_BUFFER, vertices, usage);

        if (!faces.empty())
        {
            glo::glGenBuffers(1, &m_vboHandles[IBO]);
            bindIBO();

            std::vector<uint32_t> indices;
            indices.resize(faces.size() * faces[0].indices.size());

            uint32_t current = 0;

            for(uint32_t f = 0, fe = faces.size(); f < fe; ++f)
            {
                Face& face = faces[f];

                for(uint32_t i = 0, ie = face.indices.size(); i < ie; ++i)
                {
                    indices[current++] = face.indices[i];
                }
            }

            rut::glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, usage);

        }

        if (m_material)
        {
            m_material->commit();
        }

        unbindAll();
        buildBounds();

        return true;
    }

    return false;
}

GraphicMesh::GraphicMesh()
{

}

GraphicMesh::~GraphicMesh()
{

}

void GraphicMesh::setMaterial( MaterialPtr mat )
{
    m_material = mat;
}

MaterialPtr GraphicMesh::getMaterial()
{
    return m_material;
}

MaterialConstPtr GraphicMesh::getMaterial() const
{
    return m_material;
}
