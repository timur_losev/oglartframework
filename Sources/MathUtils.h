#pragma once

namespace glm
{
    float magnitudeSq(const vec3& v);
    bool closeEnough(float f1, float f2);

    float squaredDistance(const vec3& a, const vec3& b);

    template<typename T>
    T sqr(const T& a)
    {
        return a * a;
    }

    vec4 transformVector4(const vec4& v, const mat4& mat);
    vec3 transformVector3(const vec3& v, const mat4& mat);
    vec3 transformNormal(const vec3& v, const mat4& mat);
}