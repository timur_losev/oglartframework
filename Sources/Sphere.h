#pragma once

class Vertex;

class Sphere
{
public:
    vec3 center;
    float radius;

    Sphere(const vec3& cen, float rad): center(cen), radius(rad) {}
    Sphere(const Vertex* points, size_t count);
};