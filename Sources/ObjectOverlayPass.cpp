#include "Precompiled.h"
#include "ObjectOverlayPass.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"
#include "Vertex.h"
#include "RenderUtils.h"

ObjectOverlayPass::ObjectOverlayPass()
{

}

ObjectOverlayPass::~ObjectOverlayPass()
{

}

void ObjectOverlayPass::commitImpl()
{
    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, position));
    int colLoc = rut::glVertexAttribPointer("VertexColor0", m_shaderProgram, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, color));

    rut::glEnableVertexAttribArray(posLoc);
    rut::glEnableVertexAttribArray(colLoc);
}

void ObjectOverlayPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);
}