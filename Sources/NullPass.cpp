#include "Precompiled.h"
#include "NullPass.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"
#include "Vertex.h"

#include "RenderUtils.h"

NullPass::NullPass()
{

}

NullPass::~NullPass()
{

}

void NullPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);
}

void NullPass::commitImpl()
{
    OGL_TRACE_DEBUG(m_logger, "NullPass commit");

    ShaderProgramManager::use(m_shaderProgram);

    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, position));
    int tcLoc = rut::glVertexAttribPointer("VertexTexcoord0", m_shaderProgram, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, texcoord0));
    int colLoc = rut::glVertexAttribPointer("VertexColor0", m_shaderProgram, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, color));
    int normLoc = rut::glVertexAttribPointer("VertexNormal", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, normal));

    rut::glEnableVertexAttribArray(posLoc);
    rut::glEnableVertexAttribArray(tcLoc);
    rut::glEnableVertexAttribArray(colLoc);
    rut::glEnableVertexAttribArray(normLoc);
}
