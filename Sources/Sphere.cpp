#include "Precompiled.h"
#include "Sphere.h"
#include "Box.h"
#include "MathUtils.h"

#include "Vertex.h"

Sphere::Sphere( const Vertex* points, size_t count):
radius(0.f)
{
    assert(points && count);

    if (count)
    {
        Box box(points, count);

        *this = Sphere( (box.min + box.max) * 0.5f, 0);

        for(size_t i = 0; i < count; ++i)
        {
            float dist = glm::squaredDistance(points[i].position, center);

            if (dist > radius)
            {
                radius = dist;
            }

            radius = glm::sqrt(radius) * 1.001f;
        }
    }
}
