#include "Precompiled.h"
#include "LightManager.h"
#include "Light.h"
#include "Node.h"
#include "GraphicMesh.h"
#include "VolumeMesh.h"

#include "ColladaParser.h"
#include "ColladaHelper.h"
#include "GeomUtils.h"

#include "Logger.h"

namespace Collada
{
    forward_this_s(Light);
}

LightManager::LightManager()
{
    m_logger = GetLoggerPtr();
}

LightManager::~LightManager()
{

}

void LightManager::correctBounds( LightPtr light )
{
    if (light->type == LightTypes::SPOT)
    {
        const float h = light->distance;
        const float coneRadiusAngleRadian = light->angleOuterCone;
        const float rad = glm::tan(coneRadiusAngleRadian) * h;
        light->lightGeometry.reset(new VolumeMesh());
        GeomUtils::CreateLightCone(light->lightGeometry, rad, h, 36);
        light->lightGeometry->buildBounds();
    }
    else if (light->type == LightTypes::POINT)
    {
        float c = light->attenuationConstant;
        float l = light->attenuationLinear;
        float q = light->attenuationQuadratic;

        float outerRadius = light->distance;

        if (c != 1.0f || l != 0.0f || q != 0.0f)
        {
            // Calculate radius from Attenuation
            const float threshold_level = 10.f;// difference of 10-15 levels deemed unnoticeable
            const float threshold = 1.0f / (threshold_level / 256.0f); 

            c = c - threshold;

            // Use quadratic formula to determine outer radius
            const float d = glm::sqrt(l * l - 4 * q * c); //b^2 - 4ac

            outerRadius = (-2 * c) / (l + d);
            outerRadius *= 1.2f;
        }
        else
        {
            OGL_TRACE_ERROR(m_logger, "Impossible to calculate outer radius for light " << light->getName());
        }

        light->lightGeometry.reset(new VolumeMesh());
        GeomUtils::CreateLightSphere(light->lightGeometry, outerRadius, 10, 10);
        light->lightGeometry->buildBounds();
    }
}

void LightManager::buildLightsForNode( Collada::ColladaParserPtr parser, Collada::NodeConstPtr source, LightPtr target )
{
    assert(parser);

    //! WARN: Only one light here
    BOOST_FOREACH(const Collada::LightInstance& lid, source->lights)
    {
        Collada::ColladaParser::LightLibrary_t::const_iterator srcLightIt = parser->lightLibrary.find(lid.light);

        if (srcLightIt == parser->lightLibrary.end())
        {
            OGL_TRACE_ERROR(m_logger, "Collada: Unable to find light for ID \"" + lid.light + "\". Skipping.");
            continue;
        }

        Collada::LightConstPtr srcLight = srcLightIt->second;

        if (srcLight->type == LightTypes::AMBIENT)
        {
            OGL_TRACE_ERROR(m_logger, "Collada: Skipping ambient light for the moment.");
            continue;
        }

        /*LightPtr newLight(new Light(target->getName()));

        target->addChild(newLight);*/

        LightPtr newLight = target;

        newLight->type = (LightTypes::Enum)srcLight->type;

        newLight->direction = vec3(0.f, 0.f, -1.f);

        newLight->attenuationConstant =     srcLight->attConstant;
        newLight->attenuationLinear =       srcLight->attLinear;
        newLight->attenuationQuadratic =    srcLight->attQuadratic;
        newLight->distance             =    srcLight->dist;
        newLight->falloffType          =    (LightFalloffTypes::Enum)srcLight->falloffType;

        // collada doesn't differentiate between these color types
        newLight->colorDiffuse = newLight->colorSpecular = newLight->colorAmbient = srcLight->color * srcLight->intensity;

        // convert falloff angle and falloff exponent in our representation, if given

        if (newLight->type == LightTypes::SPOT)
        {
            newLight->angleInnerCone = glm::radians(srcLight->falloffAngle) / 2.f;
            // ... some extension magic. FUCKING COLLADA. 
            if (srcLight->outerAngle == Collada::_COLLADA_LIGHT_ANGLE_NOT_SET)
            {
                // ... some deprecation magic. FUCKING FCOLLADA.
                if (srcLight->penumbraAngle == Collada::_COLLADA_LIGHT_ANGLE_NOT_SET)
                {
                    // Need to rely on falloff_exponent. I don't know how to interpret it, so I need to guess ....
                    // epsilon chosen to be 0.1
                    // 
                    newLight->angleOuterCone = glm::radians(glm::acos(glm::pow(0.1f, 1.f / srcLight->falloffExponent)) + srcLight->falloffAngle);
                }
                else
                {
                    newLight->angleOuterCone = newLight->angleInnerCone + glm::radians(srcLight->penumbraAngle);

                    if (newLight->angleOuterCone < newLight->angleInnerCone)
                    {
                        std::swap(newLight->angleInnerCone, newLight->angleOuterCone);
                    }
                }
            }
            else
            {
                newLight->angleOuterCone = glm::radians(srcLight->outerAngle);
            }

            newLight->angleOuterCone /= 2.f;
        }

        OGL_TRACE_DEBUG(m_logger, "Created light " << newLight->getName());

        correctBounds(newLight);

        lights.push_back(newLight);
    }
}
