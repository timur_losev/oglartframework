/*
 * File:   TextureManager.h
 * Author: void
 *
 * Created on April 15, 2013, 10:33 PM
 */

#ifndef TEXTUREMANAGER_H
#    define	TEXTUREMANAGER_H

forward_this(Texture);

class TextureManager
{
private:

    typedef FHashTable<Path_t::string_type, TexturePtr> Textures_t; //name to data
    Textures_t m_textures;

    static TextureConstPtr textureInUse;

public:
    TextureManager();
    ~TextureManager();

    TexturePtr getTexture(const Path_t& filename);

    static bool use(TextureConstPtr texture);
} ;

#endif	/* TEXTUREMANAGER_H */

