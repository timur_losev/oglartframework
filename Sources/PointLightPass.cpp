#include "Precompiled.h"
#include "PointLightPass.h"

#include "ShaderProgramManager.h"
#include "ShaderProgram.h"
#include "Light.h"

PointLightPass::PointLightPass()
{
    m_ambientIntensity = 0.1f;
    m_diffuseIntensity = 1.f;
}

PointLightPass::~PointLightPass()
{

}

void PointLightPass::commitImpl()
{
    LightPass::commitImpl();

    m_pointLightLoc.Constant            = m_shaderProgram->getUnsafe("Constant");
    m_pointLightLoc.Linear              = m_shaderProgram->getUnsafe("Linear");
    m_pointLightLoc.Exp                 = m_shaderProgram->getUnsafe("Exp");
}

void PointLightPass::use( LightConstPtr light )
{
    LightPass::use(light);

    if (m_pointLightLoc.Constant != INVALID_ID)
    {
        glo::glUniform1f(m_pointLightLoc.Constant, light->attenuationConstant);

        glo::glUniform1f(m_pointLightLoc.Linear, light->attenuationLinear);

        glo::glUniform1f(m_pointLightLoc.Exp, light->attenuationQuadratic);
    }
}
