/*
 * File:   Renderer.h
 * Author: void
 *
 * Created on March 23, 2013, 11:25 PM
 */

#ifndef RENDERER_H
#    define	RENDERER_H

#include "Adaptation.h"

forward_this(Scene);
forward_this_w(MeshManager);
forward_this(Line3D);
forward_this(Circle3D);
forward_this(Compositor);
forward_this(GraphicMesh);
forward_this(Light);
forward_this(ScreenQuad);

class Renderer
{
private:
    MeshManagerWeakPtr m_meshManagerRef;
    ScenePtr           m_target;

    CompositorPtr      m_compositor;
    ScreenQuadPtr      m_screenQuad;

    enum
    {
        GEOMETRY_PASS_ID = 0,
        //QUAD_PASS_ID,
        NULL_PASS_ID,
        LIGHT_PASS_ID,
        DIR_LIGHT_PASS_ID
    } m_renderState;
private:

    void setUpRenderTechnique();

    void deferredGeometryPass();

    void deferredStencilPass(uint32_t i);
    void deferredLightPass(uint32_t i);

    void deferredDirectionalLightPass();
    void deferredFinalPass();

public:
    Renderer();
    ~Renderer();

    void init();
    void setTarget(ScenePtr);
    void render();
    void update(uint32_t dt);
    void onReshape();

public:

    //Render accessors

    void drawMesh(GraphicMeshConstPtr mesh, const mat4& transform, const mat4& model) const;
    void drawLine3D(Line3DConstPtr line, const mat4& transform) const;
    void drawCircle3D(Circle3DConstPtr circle, const mat4& transform) const;

    void drawTestLight(LightPtr light, const mat4& modelView, const mat4& transform) const;
} ;

#endif	/* RENDERER_H */

