#pragma once

// Base of any VRAM holder
class HardwareElement
{
protected:
    enum
    {
        VBO,
        IBO,
        VAO,
        TOTAL
    };

    GLuint m_vboHandles[TOTAL];

public:

    HardwareElement();
    virtual ~HardwareElement();

    virtual bool        commit(GLenum usage = GL_STATIC_DRAW) = 0;
    virtual bool        commited() const;

    virtual void        bindVAO() const;
    virtual void        bindVBO() const;
    virtual void        bindIBO() const;

    static void         unbindAll();

    //! Check for none buffer binded
    static bool         checkVirginity();
};