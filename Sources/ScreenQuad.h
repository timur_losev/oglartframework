#pragma once

forward_this(GraphicMesh);

class DeferredMaterialManager;

class ScreenQuad
{
private:
    GraphicMeshPtr m_quadMesh;
public:

    ScreenQuad(DeferredMaterialManager&);
    ~ScreenQuad();

    GraphicMeshPtr          getQuadMesh() const { return m_quadMesh; }

    void render();
};