#pragma once

#include "IInputListener.h"

forward_this_w(IInputListener);
forward_this(IInputListener);

class InputListener:
    public OIS::KeyListener,
    public OIS::MouseListener
{
public:
    typedef std::vector<IInputListenerWeakPtr> Sublisteners_t;

private:
    OIS::InputManager*      inputManager;
    OIS::Keyboard*          keyboard;       // keyboard device
    OIS::Mouse*             mouse;          // mouse 

    Sublisteners_t          sublisteners;

public:

    InputListener();
    virtual ~InputListener();

    OIS::Keyboard* getKeyboard() const;
    OIS::Mouse* getMouse() const;

    void performCapture(uint32_t dt);

    virtual bool keyPressed(const OIS::KeyEvent& evt);
    virtual bool keyReleased(const OIS::KeyEvent& evt);

    virtual bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
    virtual bool mouseMoved(const OIS::MouseEvent& evt);

    void         addSublistener(IInputListenerPtr);
    void         removeSublistener(IInputListenerPtr);
};