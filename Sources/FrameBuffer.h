#pragma once

class FrameBuffer
{
public:

    enum EFlags
    {
        none = 0,
        depth16 = 1,
        depth24 = 2,
        depth32 = 4,

        stencil1 = 16,
        stencil4 = 32,
        stencil8 = 64,
        stencil16 = 128
    };

    enum EFilterModes
    {
        EFM_NEAREST = 0,
        EFM_LINEAR,
        EFM_MIPMAP
    };

private:

    int                     m_flags;
    ivec2                   m_dimenstion;
    uint32_t                m_frameBuffer;              //GL framebuffer object
    uint32_t                m_colorBuffer[8];           //texture id or buffer id
    uint32_t                m_depthBuffer;
    uint32_t                m_stencilBuffer;
    int                     m_saveViewport [4];         // saved viewport setting during bind

public:
    FrameBuffer(const ivec2& dimension, EFlags flags);
    ~FrameBuffer();


    const ivec2&            getDimension() const;
    const int               getWidth() const;
    const int               getHeight() const;

    bool                    hasStencil() const;
    bool                    hasDepth() const;

    uint32_t                getColorBuffer(uint32_t atIndex = 0) const;
    uint32_t                getDepthBuffer() const;

    bool                    validate() const;
    bool                    create();
    bool                    bind();
    bool                    unbind();

    bool                    attachColorTexture(GLenum target, uint32_t texImage, int atIndex = 0);
    bool                    attachDepthTexture(GLenum target, uint32_t texImage);

    bool                    detachColorTexture(GLenum target);
    bool                    detachDepthTexture(GLenum target);

    uint32_t                createColorTexture(GLenum format =          GL_RGBA,
                                               GLenum inernalFormat =   GL_RGBA8,
                                               GLenum clamp =           GL_REPEAT,
                                               GLenum type =            GL_UNSIGNED_BYTE,
                                               EFilterModes filter =    EFM_MIPMAP);

    uint32_t                createColorRectTexture(GLenum format = GL_RGBA, GLenum internalFormat = GL_RGBA8);

    void                    buildMipmaps(GLenum target = GL_TEXTURE_2D) const;


    static bool             checkCapabilities();
    static int              maxColorAttachements();
    static int              maxSize();
};