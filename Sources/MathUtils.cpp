#include "Precompiled.h"
#include "MathUtils.h"

float glm::magnitudeSq( const vec3& v )
{
    return v.x * v.x + v.y * v.y + v.z * v.z;
}

bool glm::closeEnough( float f1, float f2 )
{
    // Determines whether the two floating-point values f1 and f2 are
    // close enough together that they can be considered equal.

    return fabsf((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < glm::epsilon<float>();
}

float glm::squaredDistance( const vec3& a, const vec3& b )
{
    return glm::sqr(b.x - a.x) + glm::sqr(b.y - a.y) - glm::sqr(b.z - a.z);
}

vec4 glm::transformVector4( const vec4& v, const mat4& mat )
{
    vec4 res;

#if 0 //ASM_GOOD
#else
    res = mat * v;
#endif

    return res;
}

vec3 glm::transformVector3( const vec3& v, const mat4& mat )
{
    vec3 res = transformVector4(vec4(v.xyz, 1.f), mat).xyz;

    return res;
}

vec3 glm::transformNormal( const vec3& v, const mat4& mat )
{
    return transformVector4(vec4(v.xyz, 0.f), mat).xyz;
}
