/*
 * File:   Adaptation.h
 * Author: void
 *
 * Created on March 30, 2013, 7:56 PM
 */

#ifndef ADAPTATION_H
#    define	ADAPTATION_H

const uint32_t _MAX_NUMBER_OF_TEXTURECOORDS = 8;
const uint32_t _MAX_NUMBER_OF_COLOR_SETS = 8;



enum ETextureOp
{
    /** T = T1 * T2 */
    TextureOp_Multiply = 0x0,

    /** T = T1 + T2 */
    TextureOp_Add = 0x1,

    /** T = T1 - T2 */
    TextureOp_Subtract = 0x2,

    /** T = T1 / T2 */
    TextureOp_Divide = 0x3,

    /** T = (T1 + T2) - (T1 * T2) */
    TextureOp_SmoothAdd = 0x4,

    /** T = T1 + (T2-0.5) */
    TextureOp_SignedAdd = 0x5,


    /** @cond never
     *  This value is not used. It forces the compiler to use at least
     *  32 Bit integers to represent this enum.
     */
#    ifndef SWIG
    _TextureOp_Force32Bit = 0x9fffffff
#    endif
    //! @endcond
} ;

struct UVTransform
{
    /** Translation on the u and v axes.
     *
     *  The default value is (0|0).
     */
    glm::vec2 translation;

    /** Scaling on the u and v axes.
     *
     *  The default value is (1|1).
     */
    glm::vec2 scaling;

    /** Rotation - in counter-clockwise direction.
     *
     *  The rotation angle is specified in radians. The
     *  rotation center is 0.5f|0.5f. The default value
     *  0.f.
     */
    float rotation;

    UVTransform()
    :	scaling	(1.f, 1.f)
    {
        // nothing to be done here ...
    }
} ;

#endif	/* ADAPTATION_H */

