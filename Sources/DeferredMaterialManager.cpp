#include "Precompiled.h"
#include "DeferredMaterialManager.h"

#include "ShaderProgramManager.h"

#include "Material.h"
#include "RenderPass.h"

#include "GBufferPass.h"
#include "LightPass.h"
#include "NullPass.h"
#include "DirLightPass.h"
#include "ObjectOverlayPass.h"
#include "SpotLightPass.h"
#include "PointLightPass.h"
#include "Light.h"

#include "VolumeMesh.h"

#include "RenderTechnique.h"

RenderPassPtr DeferredMaterialManager::createGeometryPass() const
{
    GBufferPassPtr pass(new GBufferPass());
    pass->setAttribute(MaterialAttributes::NameString, "GBuffer");
    pass->setShaderProgram(m_gbufferProgram);

    return pass;
}

RenderPassPtr DeferredMaterialManager::createGeometryForwardPass() const
{
    GBufferPassPtr pass(new GBufferPass());
    pass->setAttribute(MaterialAttributes::NameString, "GBufferFW");
    pass->setShaderProgram(m_geomProgramFw);

    return pass;
}

RenderPassPtr DeferredMaterialManager::createPointLightPass() const
{
    RenderPassPtr pass(new PointLightPass());
    pass->setAttribute(MaterialAttributes::NameString, "PointLight");
    pass->setShaderProgram(m_pointLightProgram);

    return pass;
}

RenderPassPtr DeferredMaterialManager::createDirectionalLightPass() const
{
    DirLightPassPtr pass(new DirLightPass());
    pass->setAttribute(MaterialAttributes::NameString, "DirectionalLight");
    pass->setShaderProgram(ShaderProgramManager::constructDirectionalPassDeferredProgram());

    return pass;
}

RenderPassPtr DeferredMaterialManager::createNullPass() const
{
    RenderPassPtr pass(new NullPass());
    pass->setAttribute(MaterialAttributes::NameString, "NullPass");
    pass->setShaderProgram(m_nullPassProgram);

    return pass;
}

RenderPassPtr DeferredMaterialManager::createObjectOverlayPass() const
{
    RenderPassPtr pass(new ObjectOverlayPass());
    pass->setAttribute(MaterialAttributes::NameString, "ObjectOverlay");
    pass->setShaderProgram(m_objectOverlayProgram);

    return pass;
}

RenderPassPtr DeferredMaterialManager::createSpotLightPass() const
{
    RenderPassPtr pass(new SpotLightPass());
    pass->setAttribute(MaterialAttributes::NameString, "SpotLight");
    pass->setShaderProgram(m_spotLightProgram);

    return pass;
}

MaterialPtr DeferredMaterialManager::createPointLightVolumeMaterial(const std::string& name) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    tech->setName("LightVolume");
    tech->addPass(createNullPass());
    tech->addPass(createPointLightPass());

    material->addTechnique(tech);

    return material;
}

MaterialPtr DeferredMaterialManager::createSpotLightVolumeMaterial( const std::string& name ) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    tech->setName("LightVolume");
    tech->addPass(createNullPass());
    tech->addPass(createSpotLightPass());

    material->addTechnique(tech);

    return material;
}

MaterialPtr DeferredMaterialManager::createGBufferMaterial(const std::string& name) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    material->addTechnique(tech);
    tech->addPass(createGeometryPass());

    return material;
}

MaterialPtr DeferredMaterialManager::createForwardMaterial( const std::string& name ) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    material->addTechnique(tech);
    tech->addPass(createGeometryForwardPass());

    return material;
}

MaterialPtr DeferredMaterialManager::createScreenQuadMaterial( const std::string& name ) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    material->addTechnique(tech);
    tech->addPass(createDirectionalLightPass());

    return material;
}

MaterialPtr DeferredMaterialManager::createObjectOverlayMaterial( const std::string& name ) const
{
    MaterialPtr material(new Material(name));
    RenderTechniquePtr tech(new RenderTechnique());
    material->addTechnique(tech);
    tech->addPass(createObjectOverlayPass());

    return material;
}

void DeferredMaterialManager::create()
{
    m_gbufferProgram = ShaderProgramManager::constructGeometryPassDeferredProgram();
    m_nullPassProgram = ShaderProgramManager::constructNullPassDeferredProgram();
    m_pointLightProgram = ShaderProgramManager::constructPointLightPassDeferredProgram();
    m_geomProgramFw = ShaderProgramManager::constructSimpleForwardProgram();
    m_objectOverlayProgram = ShaderProgramManager::constructObjectOverlayProgram();
    m_spotLightProgram = ShaderProgramManager::constructSpotLightPassDeferredProgram();
}

DeferredMaterialManager::DeferredMaterialManager()
{
    create();
}

DeferredMaterialManager::~DeferredMaterialManager()
{

}

void DeferredMaterialManager::applyLightMaterial( LightPtr lightNode ) const
{
    if (lightNode->type == LightTypes::POINT)
    {
        lightNode->lightGeometry->setMaterial(createPointLightVolumeMaterial(lightNode->getName()));
    }
    else if (lightNode->type == LightTypes::SPOT)
    {
        lightNode->lightGeometry->setMaterial(createSpotLightVolumeMaterial(lightNode->getName()));
    }
}
