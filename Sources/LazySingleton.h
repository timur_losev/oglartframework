#pragma once

template<class T>
class LazySingleton
{
protected:
    static T*   m_Instance;
public:

    static T& GetRef()
    {
        assert(m_Instance);

        return *m_Instance;
    }

    static T* GetPtr()
    {
        return m_Instance;
    }
};

#    define SINGLETON_SETUP(_class_) template<> _class_* LazySingleton<_class_>::m_Instance = 0
#    define SINGLETON_ENABLE_THIS m_Instance = this