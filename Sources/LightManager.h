#pragma once

namespace Collada
{
    forward_this(ColladaParser);
    forward_this_s(Node);
}

forward_this(Light);
forward_this(Node);
class Logger;

class LightManager
{
public:
    typedef std::list<LightPtr> Lights_t;
private:
    Lights_t lights;

    Logger* m_logger;
public:

    LightManager();
    ~LightManager();

    void buildLightsForNode(Collada::ColladaParserPtr, Collada::NodeConstPtr source, LightPtr target);

    Lights_t& getLights() { return lights; }
    const Lights_t& getLights() const { return lights; }

    void correctBounds(LightPtr light);
};