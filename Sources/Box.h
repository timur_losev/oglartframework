#pragma once

class Vertex;

class Box
{
public:
    vec3 min;
    vec3 max;
    Bool_t isValid;

    Box():isValid(FALSE) {}
    Box(const vec3& amin, const vec3& amax): min(amin), max(amax), isValid(TRUE){}
    Box(const Vertex* points, size_t count);

    Box operator + (const Box& oth) const;
    Box& operator += (const Box& oth);

    vec3 getExtent() const;
    vec3 getCenter() const;

    FORCEINLINE Box& operator += (const vec3& oth)
    {
        if (isValid)
        {
#if ASM_GOOD
            __asm
            {
                mov     eax,[oth]
                mov     ecx,[this]

                movss   xmm3,[eax]vec3.x
                movss   xmm4,[eax]vec3.y
                movss   xmm5,[eax]vec3.z

                movss   xmm0,[ecx]Box.min.x
                movss   xmm1,[ecx]Box.min.y
                movss   xmm2,[ecx]Box.min.z
                minss   xmm0,xmm3
                minss   xmm1,xmm4
                minss   xmm2,xmm5
                movss   [ecx]Box.min.x,xmm0
                movss   [ecx]Box.min.y,xmm1
                movss   [ecx]Box.min.z,xmm2

                movss   xmm0,[ecx]Box.max.x
                movss   xmm1,[ecx]Box.max.y
                movss   xmm2,[ecx]Box.max.z
                maxss   xmm0,xmm3
                maxss   xmm1,xmm4
                maxss   xmm2,xmm5
                movss   [ecx]Box.max.x,xmm0
                movss   [ecx]Box.max.y,xmm1
                movss   [ecx]Box.max.z,xmm2
            }
#else
            min.x = glm::min( min.x, oth.x );
            min.y = glm::min( min.y, oth.y );
            min.z = glm::min( min.z, oth.z );

            max.x = glm::max( max.x, oth.x );
            max.y = glm::max( max.y, oth.y );
            max.z = glm::max( max.z, oth.z );
#endif


            return *this;
        }
        else
        {
            min = max = oth;
            isValid = TRUE;


            return *this;
        }
    }
};