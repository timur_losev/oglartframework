#pragma once

forward_this(ScreenQuad);
forward_this(FrameBuffer);
forward_this(GeometryBuffer);
forward_this(RenderPass);

class Compositor
{
private:
    FrameBufferPtr      m_frameBuffer;
    GeometryBufferPtr   m_geometryBuffer;

public:

    Compositor(const ivec2& dimenstion);
    ~Compositor();

    GeometryBufferPtr       getGeometryBuffer() const;
};