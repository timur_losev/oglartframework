#include "Precompiled.h"
#include "InputListener.h"

#include "Application.h"
#include "Logger.h"

InputListener::InputListener():
    inputManager(nullptr),
    keyboard(nullptr),
    mouse(nullptr)
{
#if SYS_PLATFORM_DESKTOP == 1
    OIS::ParamList pl;
    std::ostringstream winHandleStr;

    unsigned long hwnd = (unsigned long)Application::GetRef().getWindowHandle();

    winHandleStr << hwnd;
    pl.insert(std::make_pair("WINDOW", winHandleStr.str()));

    if (/*nograb*/1)
    {
        pl.insert(std::make_pair("x11_keyboard_grab", "false"));
        pl.insert(std::make_pair("x11_mouse_grab", "false"));
        pl.insert(std::make_pair("w32_mouse", "DISCL_FOREGROUND"));
        pl.insert(std::make_pair("w32_mouse", "DISCL_NONEXCLUSIVE"));
        pl.insert(std::make_pair("w32_keyboard", "DISCL_FOREGROUND"));
        pl.insert(std::make_pair("w32_keyboard", "DISCL_NONEXCLUSIVE"));
    }

    try
    {
        inputManager = OIS::InputManager::createInputSystem(pl);
    }
    catch(OIS::Exception& e)
    {
        OGL_TRACE_ERROR(GetLoggerPtr(), "The input system was not properly created: " << e.what());
        assert(false);
    }

    if (inputManager)
    {
        OIS::Object* obj = inputManager->createInputObject(OIS::OISKeyboard, false);
        keyboard = static_cast<OIS::Keyboard*>(obj);
        mouse = static_cast<OIS::Mouse*>(inputManager->createInputObject(OIS::OISMouse, false));

        keyboard->setEventCallback(this);
        mouse->setEventCallback(this);
    }
    else
    {
        OGL_TRACE_ERROR(GetLoggerPtr(), "The input system was not properly created");
        assert(false);
    }
#endif
}

InputListener::~InputListener()
{
    if (inputManager)
    {
        inputManager->destroyInputObject(keyboard);
        inputManager->destroyInputObject(mouse);
        OIS::InputManager::destroyInputSystem(inputManager);
        inputManager = nullptr;
    }
}

bool InputListener::keyPressed( const OIS::KeyEvent& evt )
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->keyPressed(evt);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }

    return true;
}

bool InputListener::keyReleased( const OIS::KeyEvent& evt )
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->keyReleased(evt);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }

    return true;
}

bool InputListener::mousePressed( const OIS::MouseEvent& evt, OIS::MouseButtonID id )
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->mousePressed(evt, id);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }

    return true;
}

bool InputListener::mouseReleased( const OIS::MouseEvent& evt, OIS::MouseButtonID id )
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->mouseReleased(evt, id);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }

    return false;
}

bool InputListener::mouseMoved( const OIS::MouseEvent& evt )
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->mouseMoved(evt);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }

    return true;
}

void InputListener::performCapture(uint32_t dt)
{
    keyboard->capture();
    mouse->capture();

    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; /*nothing here*/)
    {
        IInputListenerWeakPtr sl = *it;

        if (auto ptr = sl.lock())
        {
            ptr->nonBufferedUpdateInput(keyboard, mouse, dt);
            ++it;
        }
        else
        {
            it = sublisteners.erase(it);
        }
    }
}

void InputListener::addSublistener( IInputListenerPtr s)
{
    sublisteners.push_back(s);
}

void InputListener::removeSublistener( IInputListenerPtr s)
{
    for(Sublisteners_t::iterator it = sublisteners.begin(), ie = sublisteners.end(); it != ie; ++it)
    {
        IInputListenerWeakPtr ptr = *it;
        if(!ptr.expired() && ptr.lock() == s)
        {
            sublisteners.erase(it);
        }
    }
}

OIS::Keyboard* InputListener::getKeyboard() const
{
    return keyboard;
}

OIS::Mouse* InputListener::getMouse() const
{
    return mouse;
}
