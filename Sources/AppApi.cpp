#include "Precompiled.h"
#include "AppApi.h"

#if SYS_PLATFORM == SYS_PLATFORM_LINUX
#include <sys/time.h>
#endif

uint64_t AppApiGetTime()
{
#if SYS_PLATFORM == SYS_PLATFORM_WINDOWS
    __int64 timeval;
    _time64(&timeval);

    return timeval * 1000;
#elif SYS_PLATFORM == SYS_PLATFORM_APPLE_MACOS
    return 0;//GetMSTime();
#else // linux
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts); //get the current time
    return ts.tv_sec * 1000 + ts.tv_nsec / (1000 * 1000);
#endif
}

uint64_t AppApiGetTickCount()
{
#if SYS_PLATFORM == SYS_PLATFORM_WINDOWS
    return GetTickCount();
#else
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (tv.tv_sec*1000+tv.tv_usec/1000);
#endif
} 

uint32_t AppApiGetTimeMS()
{
    return static_cast<uint32_t>(clock() / CLOCKS_PER_SEC);
}
