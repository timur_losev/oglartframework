#include "Precompiled.h"
#include "FrameBuffer.h"

FrameBuffer::FrameBuffer( const ivec2& dimension, EFlags flags ):
    m_dimenstion(dimension),
    m_flags(flags),
    m_frameBuffer(0),
    m_depthBuffer(0),
    m_stencilBuffer(0)
{
    memset(m_colorBuffer, 0, sizeof(uint32_t) * 8);
}

FrameBuffer::~FrameBuffer()
{
    if (m_depthBuffer)
    {
        glo::glDeleteRenderbuffers(1, &m_depthBuffer);
    }

    if (m_stencilBuffer != 0)
    {
        glo::glDeleteRenderbuffers(1, &m_stencilBuffer);
    }

    if (m_frameBuffer)
    {
        glo::glDeleteFramebuffers(1, &m_frameBuffer);
    }
}

const ivec2& FrameBuffer::getDimension() const
{
    return m_dimenstion;
}

const int FrameBuffer::getWidth() const
{
    return m_dimenstion.x;
}

const int FrameBuffer::getHeight() const
{
    return m_dimenstion.y;
}

bool FrameBuffer::hasStencil() const
{
    return m_stencilBuffer != 0;
}

bool FrameBuffer::hasDepth() const
{
    return m_depthBuffer != 0;
}

uint32_t FrameBuffer::getColorBuffer( uint32_t atIndex /*= 0*/ ) const
{
    assert(atIndex < 8);

    return m_colorBuffer[atIndex];
}

uint32_t FrameBuffer::getDepthBuffer() const
{
    return m_depthBuffer;
}

bool FrameBuffer::validate() const
{
    GLint currentfb;

    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &currentfb);

    if (currentfb != m_frameBuffer)
    {
        glo::glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
        glReadBuffer(GL_COLOR_ATTACHMENT0);
    }

    int status = glo::glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (currentfb != m_frameBuffer)
    {
        glo::glBindFramebuffer(GL_FRAMEBUFFER, currentfb);
    }

    return status == GL_FRAMEBUFFER_COMPLETE;
}

bool FrameBuffer::create()
{
    if (getWidth() <= 0 || getHeight() <= 0) return false;

    glo::glGenFramebuffers(1, &m_frameBuffer);
    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);

    int depthFormat = 0, stencilFormat = 0;

    if (m_flags & depth16)
    {
        depthFormat = GL_DEPTH_COMPONENT16;
    }
    else if (m_flags & depth24)
    {
        depthFormat = GL_DEPTH_COMPONENT24;
    }
    else if (m_flags & depth32)
    {
        depthFormat = GL_DEPTH_COMPONENT32;
    }

    if (m_flags & stencil1)
    {
        stencilFormat = GL_STENCIL_INDEX1;
    }
    else if (m_flags & stencil4)
    {
        stencilFormat = GL_STENCIL_INDEX4;
    }
    else if (m_flags & stencil8)
    {
        stencilFormat = GL_STENCIL_INDEX8;
    }
    else if (m_flags & stencil16)
    {
        stencilFormat = GL_STENCIL_INDEX16;
    }

   if (depthFormat != 0)
    {
        glo::glGenRenderbuffers(1, &m_depthBuffer);
        glo::glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
        glo::glRenderbufferStorage(GL_RENDERBUFFER, depthFormat, m_dimenstion.x, m_dimenstion.y);
        glo::glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer);
    }

    if (stencilFormat != 0)
    {
        glo::glGenRenderbuffers(1, &m_stencilBuffer);
        glo::glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
        glo::glRenderbufferStorage(GL_RENDERBUFFER, stencilFormat, m_dimenstion.x, m_dimenstion.y);
        glo::glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_stencilBuffer);
    }

    GLenum status = glo::glCheckFramebufferStatus(GL_FRAMEBUFFER);
    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    return status == GL_FRAMEBUFFER_COMPLETE;
}

bool FrameBuffer::bind()
{
    if (m_frameBuffer == 0)
        return false;

    //glFlush(); // ???

    //glGetIntegerv(GL_VIEWPORT, m_saveViewport);
    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);
    /*glReadBuffer(GL_COLOR_ATTACHMENT0);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glViewport(0, 0, getWidth(), getHeight());*/

    return true;
}

bool FrameBuffer::unbind()
{
    if (m_frameBuffer == 0) return false;

    //glFlush();

    glo::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    //glViewport(m_saveViewport[0], m_saveViewport[1], m_saveViewport[1], m_saveViewport[2]);

    return true;
}

bool FrameBuffer::attachColorTexture( GLenum target, uint32_t texImage, int atIndex /*= 0*/ )
{
    assert(atIndex < 8);

    if (m_frameBuffer == 0)
    {
        return false;
    }

    if (target != GL_TEXTURE_2D && target != GL_TEXTURE_RECTANGLE && 
    (target < GL_TEXTURE_CUBE_MAP_POSITIVE_X || target > GL_TEXTURE_CUBE_MAP_NEGATIVE_Z))
    {
        return false;
    }

    glBindTexture(target, m_colorBuffer[atIndex] = texImage);
    glo::glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + atIndex, target, texImage, 0);
    //glo::glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + atIndex, texImage, 0);

    return true;
}

bool FrameBuffer::attachDepthTexture( GLenum target, uint32_t texImage )
{
    if (m_frameBuffer == 0)
        return false;

    glo::glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target, m_depthBuffer = texImage, 0);

    return true;
}

bool FrameBuffer::detachColorTexture( GLenum target )
{
    return attachColorTexture(target, 0);
}

bool FrameBuffer::detachDepthTexture( GLenum target )
{
    return attachDepthTexture(target, 0);
}

uint32_t FrameBuffer::createColorTexture( GLenum format /*= GL_RGBA*/, GLenum inernalFormat /*= GL_RGBA8*/, GLenum clamp /*= GL_REPEAT*/, GLenum type /*= GL_UNSIGNED_BYTE*/, EFilterModes filter /*= EFM_MIPMAP*/ )
{
    GLuint tex;

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //one byte

    glTexImage2D(GL_TEXTURE_2D, 0, inernalFormat, getWidth(), getHeight(), 0, format, type, nullptr);

    if (filter == EFM_NEAREST)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }
    else if (filter == EFM_LINEAR)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else if (filter == EFM_MIPMAP)
    {
        //TODO:
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_MIPMAP);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_MIPMAP);
        glo::glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST); 
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);

    return tex;
}

uint32_t FrameBuffer::createColorRectTexture( GLenum format /*= GL_RGBA*/, GLenum internalFormat /*= GL_RGBA8*/ )
{
    return 0;
}

void FrameBuffer::buildMipmaps( GLenum target /*= GL_TEXTURE_2D*/ ) const
{
    glo::glGenerateMipmap(target);
}

bool FrameBuffer::checkCapabilities()
{
    return true; //TODO:
}

int FrameBuffer::maxColorAttachements()
{
    GLint n;

    glGetIntegerv ( GL_MAX_COLOR_ATTACHMENTS, &n );

    return n;
}

int FrameBuffer::maxSize()
{
    GLint sz;

    glGetIntegerv ( GL_MAX_RENDERBUFFER_SIZE, &sz );

    return sz;
}
