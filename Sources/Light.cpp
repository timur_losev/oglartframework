#include "Precompiled.h"
#include "Light.h"

Light::Light(const std::string& name):
    Node(name),
    type(LightTypes::UNDEFINED),
    attenuationConstant(0.f),
    attenuationLinear(1.f),
    attenuationQuadratic(0.f),
    angleInnerCone(glm::pi<float>()*2.f),
    angleOuterCone(glm::pi<float>()*2.f),
    distance(1.f),
    falloffType(LightFalloffTypes::CONSTANT)
{
    m_nodeType = Node::ENT_LIGHT;
}

Light::~Light()
{

}
