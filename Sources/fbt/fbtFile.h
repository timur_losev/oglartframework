#ifndef _fbtFile_h_
#define _fbtFile_h_

#include "fbtTypes.h"

/** \addtogroup FBT
*  @{
*/

class fbtStream;
class fbtBinTables;


class fbtFile
{
public:

    enum FileMagic
    {
        FM_BIG_ENDIAN = 'V',
        FM_LITTLE_ENDIAN = 'v',
        FM_32_BIT = '_',
        FM_64_BIT = '-',

        // place custom rules here, note that the
        // full header string must be <= 12 char's
    };


    enum FileStatus
    {
        FS_LINK_FAILED = -7,
        FS_INV_INSERT,
        FS_BAD_ALLOC,
        FS_INV_READ,
        FS_INV_LENGTH,
        FS_INV_HEADER_STR,
        FS_FAILED,
        FS_OK,
    };


    enum ParseMode
    {
        PM_UNCOMPRESSED,
        PM_COMPRESSED,
        PM_READTOMEMORY,

    };

    enum FileHeader
    {
        FH_ENDIAN_SWAP = (1 << 0),
        FH_CHUNK_64 = (1 << 1),
        FH_VAR_BITS = (1 << 2),
    };


    struct Chunk32
    {
        uint32_t       m_code;
        uint32_t       m_len;
        uint32_t       m_old;
        uint32_t       m_typeid;
        uint32_t       m_nr;
    };

    struct Chunk64
    {
        uint32_t       m_code;
        uint32_t       m_len;
        uint64_t       m_old;
        uint32_t       m_typeid;
        uint32_t       m_nr;
    };


    struct Chunk
    {
        uint32_t       m_code;
        uint32_t       m_len;
        FBTsize         m_old;
        uint32_t       m_typeid;
        uint32_t       m_nr;
    };


    struct MemoryChunk
    {
        enum Flag
        {
            BLK_MODIFIED = (1 << 0),
        };

        MemoryChunk* m_next, *m_prev;
        Chunk        m_chunk;
        void*        m_block;
        void*        m_newBlock;


        uint8_t     m_flag;
        FBTtype      m_newTypeId;
    };


protected:

    int parseMagic(const char* cp);

    void writeStruct(fbtStream* stream, FBTtype index, uint32_t code, FBTsize len, void* writeData);
    void writeBuffer(fbtStream* stream, FBTsize len, void* writeData);


    virtual int initializeTables(fbtBinTables* tables) = 0;
    virtual int notifyData(void* p, const Chunk& id) { return FS_OK; }
    virtual int writeData(fbtStream* stream) { return FS_OK; }

    virtual void*   getFBT() = 0;
    virtual FBTsize getFBTlength() = 0;


    // lookup name first 7 of 12
    const char* m_uhid;
    const char* m_aluhid; //alternative header string
    fbtFixedString<12> m_header;


    int m_version, m_fileVersion, m_fileHeader;

    typedef fbtHashTable<fbtSizeHashKey, MemoryChunk*> ChunkMap;
    fbtList     m_chunks;
    ChunkMap    m_map;
    fbtBinTables* m_memory, *m_file;

    Path_t      m_path;

    virtual bool skip(const uint32_t& id) { return false; }
    void* findPtr(const FBTsize& iptr);
    MemoryChunk* findBlock(const FBTsize& iptr);

private:

    int parseHeader(fbtStream* stream, bool suppressHeaderWarning = false);
    int parseStreamImpl(fbtStream* stream, bool suppressHeaderWarning = false);

    int compileOffsets();
    int link();

public:

    fbtFile(const char* uid);
    virtual ~fbtFile();

    int parse(const Path_t& path, int mode = PM_UNCOMPRESSED);
    int parse(const void* memory, FBTsize sizeInBytes, int mode = PM_UNCOMPRESSED, bool suppressHeaderWarning = false);

    /// Saving in non native endianness is not implemented yet.
    int reflect(const Path_t& path, const int mode = PM_UNCOMPRESSED, const fbtEndian& endian = FBT_ENDIAN_NATIVE);


    const fbtFixedString<12>&   getHeader()     const { return m_header; }
    const int&                  getVersion()    const { return m_fileVersion; }
    const Path_t&               getPath()       const { return m_path; }


    fbtBinTables* getMemoryTable(void)  { return m_memory; }
    fbtBinTables* getFileTable(void)    { return m_file; }


    fbtList& getChunks(void) { return m_chunks; }

    virtual void setIgnoreList(uint32_t *stripList) {}

    bool _setuid(const char* uid);
};

/** @}*/
#endif//_fbtFile_h_
