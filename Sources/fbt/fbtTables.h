#ifndef _fbtTables_h_
#define _fbtTables_h_

#include "fbtTypes.h"

/** \addtogroup FBT
*  @{
*/

static const uint32_t k_SignatureLen = 4;

namespace fbtIdNames
{
    const char FBT_SDNA[k_SignatureLen] = { 'S', 'D', 'N', 'A' };
    const char FBT_NAME[k_SignatureLen] = { 'N', 'A', 'M', 'E' }; // Name array
    const char FBT_TYPE[k_SignatureLen] = { 'T', 'Y', 'P', 'E' }; // Type Array
    const char FBT_TLEN[k_SignatureLen] = { 'T', 'L', 'E', 'N' }; // Type length array
    const char FBT_STRC[k_SignatureLen] = { 'S', 'T', 'R', 'C' }; // Struct/Class Array
    const char FBT_OFFS[k_SignatureLen] = { 'O', 'F', 'F', 'S' }; // Offset map (Optional & TODO)
}

FBT_INLINE fbtFixedString<4> fbtByteToString(uint32_t i)
{
    union
    {
        char        ids[4];
        uint32_t   idi;
    } IDU;
    IDU.idi = i;
    fbtFixedString<4> cp;
    cp.push_back(IDU.ids[0]);
    cp.push_back(IDU.ids[1]);
    cp.push_back(IDU.ids[2]);
    cp.push_back(IDU.ids[3]);
    return cp;
}

struct fbtName
{
    char*           m_name;     // note: memory is in the raw table.
    int             m_loc;
    uint32_t        m_nameId;
    int             m_ptrCount;
    int             m_numSlots, m_isFptr;
    int             m_arraySize;
    int             m_slots[FBT_ARRAY_SLOTS];
};

struct fbtType
{
    char*          m_name;     // note: memory is in the raw table.
    uint32_t       m_typeId;	// fbtCharHashKey(typeName)
    uint32_t       m_strcId;
};

union fbtKey32
{
    int16_t k16[2];
    int32_t k32;
};

union fbtKey64
{
    uint32_t k32[2];
    uint64_t k64;

};

class fbtStruct
{
public:
    typedef FVector<fbtStruct> Members_t;
    typedef FVector<fbtKey64>  Keys_t;

    enum Flag
    {
        CAN_LINK = 0,
        MISSING = (1 << 0),
        MISALIGNED = (1 << 1),
        SKIP = (1 << 2),
        NEED_CAST = (1 << 3)
    };

    fbtStruct()
        : m_key(),
        m_val(),
        m_off(0),
        m_len(0),
        m_nr(0),
        m_dp(0),
        m_strcId(0),
        m_flag(0),
        m_link(0)
    {
    }
    ~fbtStruct()    {}


    fbtKey32        m_key;      //k[0]: type, k[1]: name
    fbtKey64        m_val;      //key hash value, k[0]: type hash id, k[1]: member(field) base name hash id or 0(struct)
    int32_t         m_off;       //offset
    int32_t         m_len;
    int32_t         m_nr, m_dp; //nr: array index, dp: embeded depth
    int32_t         m_strcId;
    int32_t         m_flag;
    Members_t       m_members;
    fbtStruct*      m_link;     //file/memory table struct link
    Keys_t          m_keyChain; //parent key hash chain(0: type hash, 1: name hash), size() == m_dp

    FBTsizeType     getUnlinkedMemberCount();
};

class fbtBinTables
{
public:
    typedef fbtName*                Names_t;  // < fbtMaxTable
    typedef fbtType*                Types_t;  // < fbtMaxTable
    typedef FBTtype*                TypeL_t;  // < fbtMaxTable
    typedef FBTtype**               Strcs_t;  // < fbtMaxTable * fbtMaxMember;


    // Base name trim (*[0-9]) for partial type, name matching
    // Example: M(char m_var[32]) F(char m_var[24])
    //
    //          result = M(char m_var[24] = F(char m_var[24]) then (M(char m_var[24->32]) = 0)
    //
    // (Note: bParse will skip m_var all together because of 'strcmp(Mtype, Ftype) && strcmp(Mname, Fname)')
    //
    typedef FVector<FBTsize>       NameB_t;
    typedef FVector<fbtStruct*>    OffsM_t;

    typedef fbtHashTable<fbtCharHashKey, fbtType> TypeFinder;

public:

    fbtBinTables();
    fbtBinTables(void* ptr, const FBTsize& len);
    ~fbtBinTables();

    bool read(bool swap);
    bool read(const void* ptr, const FBTsize& len, bool swap);

    FBTtype findTypeId(const fbtCharHashKey &cp);

    const char* getStructType(const fbtStruct* strc);
    const char* getStructName(const fbtStruct* strc);
    const char* getOwnerStructName(const fbtStruct* strc);


    Names_t   m_name;
    Types_t   m_type;
    TypeL_t   m_tlen;
    Strcs_t   m_strc;
    OffsM_t   m_offs;
    NameB_t   m_base;

    uint32_t m_nameNr;
    uint32_t m_typeNr;
    uint32_t m_strcNr;

    // It's safe to assume that memory len is FBT_VOID and file len is FH_CHUNK_64 ? 8 : 4
    // Otherwise this library will not even compile (no more need for 'sizeof(ListBase) / 2')
    uint8_t     m_ptr;
    void*       m_otherBlock;
    FBTsize     m_otherLen;

private:

    TypeFinder m_typeFinder;

    void putMember(FBTtype* cp, fbtStruct* off, FBTtype nr, uint32_t& cof, uint32_t depth, fbtStruct::Keys_t& keys);
    void compile(FBTtype i, FBTtype nr, fbtStruct* off, uint32_t& cof, uint32_t depth, fbtStruct::Keys_t& keys);
    void compile();
    bool sikp(const uint32_t& type);
};

/** @}*/
#endif//_fbtTables_h_
