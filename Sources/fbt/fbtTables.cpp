#include "Precompiled.h"
#include "Logger.h"

#define FBT_IN_SOURCE
#include "fbtTables.h"
#include "fbtPlatformHeaders.h"


FBTsizeType fbtStruct::getUnlinkedMemberCount()
{
    FBTsizeType count = 0;
    for (FBTsizeType i = 0; i < m_members.size(); i++)
        if (!m_members[i].m_link) count++;

    return count;
}


fbtBinTables::fbtBinTables()
    : m_name(0),
    m_type(0),
    m_tlen(0),
    m_strc(0),
    m_nameNr(0),
    m_typeNr(0),
    m_strcNr(0),
    m_ptr(FBT_VOID),
    m_otherBlock(0),
    m_otherLen(0)
{
}

fbtBinTables::fbtBinTables(void* ptr, const FBTsize& len)
    : m_name(0),
    m_type(0),
    m_tlen(0),
    m_strc(0),
    m_nameNr(0),
    m_typeNr(0),
    m_strcNr(0),
    m_ptr(FBT_VOID),
    m_otherBlock(ptr),
    m_otherLen(len)
{
}

fbtBinTables::~fbtBinTables()
{
    fbtFree(m_name);
    fbtFree(m_type);
    fbtFree(m_tlen);
    fbtFree(m_strc);
    if (m_otherBlock)
        fbtFree(m_otherBlock);

    for (auto val : m_offs)
    {
        delete val;
    }

    OffsM_t::iterator it = m_offs.begin();
}


bool fbtBinTables::read(bool swap)
{
    if (m_otherBlock && m_otherLen != 0)
        return read(m_otherBlock, m_otherLen, swap);
    return false;
}


bool fbtBinTables::read(const void* ptr, const FBTsize& len, bool swap)
{
    uint32_t* intPtr = nullptr;
    uint32_t i = 0, j = 0;
    uint32_t k = 0; 
    uint32_t numberOfElementsInTable = 0;

    FBTtype* typePtr = nullptr;

    char* charPtr = (char*)ptr;

    //! Expecting SDNA
    if (!fbtCharNEq(charPtr, fbtIdNames::FBT_SDNA, k_SignatureLen))
    {
        OGL_LOG(("Bin table is missing the start id!"));
        return false;
    }

    charPtr += k_SignatureLen;

    //! Expecting NAME
    if (!fbtCharNEq(charPtr, fbtIdNames::FBT_NAME, k_SignatureLen))
    {
        OGL_LOG(("Bin table is missing the name id!"));
        return false;
    }

    charPtr += k_SignatureLen;

    int64_t opad;

    intPtr = (uint32_t*)charPtr;
    numberOfElementsInTable = *intPtr++;
    charPtr = (char*)intPtr;

    if (swap) numberOfElementsInTable = fbtSwap32(numberOfElementsInTable);

    if (numberOfElementsInTable > fbtMaxTable)
    {
        OGL_LOG(("Max name table size exceeded!"));
        return false;
    }
    else
    {
        m_name = (Names_t)fbtMalloc((numberOfElementsInTable * sizeof(fbtName)) + 1);
    }

    //! Read names
    i = 0;
    while (i < numberOfElementsInTable && i < fbtMaxTable)
    {
        fbtName name = { charPtr, i, fbtCharHashKey(charPtr).hash(), 0, 0, 0, 1 };

        fbtFixedString<64> bn;

        // re-lex
        while (*charPtr)
        {
            switch (*charPtr)
            {
            default:
            {
                bn.push_back(*charPtr);
                ++charPtr; break;
            }
            case ')':
            case ']':
                ++charPtr;
                break;
            case '(':   {++charPtr; name.m_isFptr = 1; break;    }
            case '*':   {++charPtr; name.m_ptrCount++; break;   }
            case '[':
            {
                while ((*++charPtr) != ']')
                    name.m_slots[name.m_numSlots] = (name.m_slots[name.m_numSlots] * 10) + ((*charPtr) - '0');
                name.m_arraySize *= name.m_slots[name.m_numSlots++];
            }
                break;
            }
        }
        ++charPtr;

        //OGL_LOG(("%d %d: %s %s %u %u") % m_nameNr % m_base.size() % name.m_name % bn.c_str() % name.m_nameId % bn.hash());

        m_name[m_nameNr++] = name;
        m_base.push_back(bn.hash());
        ++i;
    }

    // read alignment
    opad = (int64_t)charPtr;
    opad = ((opad + 3) & ~3) - opad;
    while (opad--) charPtr++;

    //! Expecting type names
    if (!fbtCharNEq(charPtr, fbtIdNames::FBT_TYPE, k_SignatureLen))
    {
        OGL_LOG(("Bin table is missing the type id!"));
        return false;
    }

    charPtr += k_SignatureLen;

    intPtr = (uint32_t*)charPtr;
    numberOfElementsInTable = *intPtr++;
    charPtr = (char*)intPtr;

    if (swap) numberOfElementsInTable = fbtSwap32(numberOfElementsInTable);

    if (numberOfElementsInTable > fbtMaxTable)
    {
        OGL_LOG(("Max name table size exceeded!"));
        return false;
    }
    else
    {
        m_type = (Types_t)fbtMalloc((numberOfElementsInTable * sizeof(fbtType) + 1));
        m_tlen = (TypeL_t)fbtMalloc((numberOfElementsInTable * sizeof(FBTtype) + 1));
    }

    i = 0;
    while (i < numberOfElementsInTable)
    {
        fbtType typeData = { charPtr, fbtCharHashKey(charPtr).hash(), -1 };
        m_type[m_typeNr++] = typeData;
        while (*charPtr) ++charPtr;
        ++charPtr;
        ++i;
    }

    // read alignment
    opad = (int64_t)charPtr;
    opad = ((opad + 3) & ~3) - opad;
    while (opad--) charPtr++;

    //! Expecting type lengths
    if (!fbtCharNEq(charPtr, fbtIdNames::FBT_TLEN, k_SignatureLen))
    {
        OGL_LOG(("Bin table is missing the tlen id!"));
        return false;
    }

    charPtr += k_SignatureLen;

    typePtr = (FBTtype*)charPtr;

    if (swap)
    {
        i = 0;
        while (i < m_typeNr)
        {
            m_tlen[i] = *typePtr++;
            m_tlen[i] = fbtSwap16(m_tlen[i]);
            ++i;
        }
    }
    else
    {
        sysMemcpy(m_tlen, typePtr, m_typeNr * sizeof(FBTtype));
        typePtr += m_typeNr;
    }

    // read alignment
    if (m_typeNr & 1) ++typePtr;

    charPtr = (char*)typePtr;

    if (!fbtCharNEq(charPtr, fbtIdNames::FBT_STRC, k_SignatureLen))
    {
        OGL_LOG(("Bin table is missing the tlen id!"));
        return false;
    }

    charPtr += k_SignatureLen;

    intPtr = (uint32_t*)charPtr;
    numberOfElementsInTable = *intPtr++;
    typePtr = (FBTtype*)intPtr;

    if (swap) numberOfElementsInTable = fbtSwap32(numberOfElementsInTable);

    if (numberOfElementsInTable > fbtMaxTable)
    {
        OGL_LOG(("Max name table size exceeded!"));
        return false;
    }
    else
    {
        m_strc = (Strcs_t)fbtMalloc(numberOfElementsInTable * fbtMaxMember * sizeof(FBTtype) + 1);
    }

    m_typeFinder.reserve(m_typeNr);

    i = 0;
    while (i < numberOfElementsInTable)
    {
        m_strc[m_strcNr++] = typePtr;

        if (swap)
        {
            typePtr[0] = fbtSwap16(typePtr[0]);
            typePtr[1] = fbtSwap16(typePtr[1]);

            m_type[typePtr[0]].m_strcId = m_strcNr - 1;

            m_typeFinder.insert(m_type[typePtr[0]].m_name, m_type[typePtr[0]]);

            k = typePtr[1];
            FBT_ASSERT(k < fbtMaxMember);

            j = 0;
            typePtr += 2;

            while (j < k)
            {
                typePtr[0] = fbtSwap16(typePtr[0]);
                typePtr[1] = fbtSwap16(typePtr[1]);

                ++j;
                typePtr += 2;
            }
        }
        else
        {
            FBT_ASSERT(typePtr[1] < fbtMaxMember);
            m_type[typePtr[0]].m_strcId = m_strcNr - 1;
            fbtType& cur = m_type[typePtr[0]];
            m_typeFinder.insert(cur.m_name, cur);

            typePtr += (2 * typePtr[1]) + 2;
        }

        ++i;
    }

    if (m_strcNr == 0)
    {
        fbtFree(m_name);
        fbtFree(m_type);
        fbtFree(m_tlen);
        fbtFree(m_strc);

        m_name = nullptr;
        m_type = nullptr;
        m_tlen = nullptr;
        m_strc = nullptr;

        return false;
    }

    compile();
    return true;
}

void fbtBinTables::compile(FBTtype i, FBTtype nr, fbtStruct* off, uint32_t& cof, uint32_t depth, fbtStruct::Keys_t& keys)
{
    uint32_t e, l, a, oof, ol;
    uint16_t f = m_strc[0][0];

    if (i > m_strcNr)
    {
        OGL_LOG(("Missing recursive type"));
        return;
    }


    for (a = 0; a < nr; ++a)
    {
        // Only calculate offsets on recursive structs
        // This saves undeded buffers
        FBTtype* strc = m_strc[i];

        oof = cof;
        ol = m_tlen[strc[0]];

        l = strc[1];
        strc += 2;

        for (e = 0; e < l; e++, strc += 2)
        {
            if (strc[0] >= f && m_name[strc[1]].m_ptrCount == 0)
            {
                fbtKey64 k = { m_type[strc[0]].m_typeId, m_name[strc[1]].m_nameId };
                keys.push_back(k);

                compile(m_type[strc[0]].m_strcId, m_name[strc[1]].m_arraySize, off, cof, depth + 1, keys);

                keys.pop_back();
            }
            else
                putMember(strc, off, a, cof, depth, keys);
        }

        if ((cof - oof) != ol)
            OGL_LOG(("Build ==> invalid offset (%i)(%i:%i)") % a % (cof - oof) % ol);

    }
}

void fbtBinTables::compile()
{
    m_offs.reserve(fbtMaxTable);

    if (!m_strc || m_strcNr <= 0)
    {
        OGL_LOG(("Build ==> No structurs."));
        return;
    }

    uint32_t i, cof = 0, depth;
    uint16_t f = m_strc[0][0], e, memberCount;

    fbtStruct::Keys_t emptyKeys;
    for (i = 0; i < m_strcNr; i++)
    {
        FBTtype* strc = m_strc[i];

        FBTtype strcType = strc[0];

        depth = 0;
        cof = 0;
        fbtStruct* off = new fbtStruct;
        off->m_key.k16[0] = strcType;
        off->m_key.k16[1] = 0;
        off->m_val.k32[0] = m_type[strcType].m_typeId;
        off->m_val.k32[1] = 0; // no name
        off->m_nr = 0;
        off->m_dp = 0;
        off->m_off = cof;
        off->m_len = m_tlen[strcType];
        off->m_strcId = i;
        off->m_link = 0;
        off->m_flag = fbtStruct::CAN_LINK;

        m_offs.push_back(off);

        memberCount = strc[1];

        strc += 2;
        off->m_members.reserve(fbtMaxMember);

        for (e = 0; e < memberCount; ++e, strc += 2)
        {
            if (strc[0] >= f && m_name[strc[1]].m_ptrCount == 0) //strc[0]:member_type, strc[1]:member_name
            {
                fbtStruct::Keys_t keys;
                fbtKey64 k = { m_type[strc[0]].m_typeId, m_name[strc[1]].m_nameId };
                keys.push_back(k);
                compile(m_type[strc[0]].m_strcId, m_name[strc[1]].m_arraySize, off, cof, depth + 1, keys);
            }
            else
                putMember(strc, off, 0, cof, 0, emptyKeys);
        }

        if (cof != off->m_len)
        {
            off->m_flag |= fbtStruct::MISALIGNED;
            OGL_LOG(("Build ==> invalid offset %s:%i:%i:%i\n") % m_type[off->m_key.k16[0]].m_name % i % cof % off->m_len);
        }
    }
}

void fbtBinTables::putMember(FBTtype* cp, fbtStruct* off, FBTtype nr, uint32_t& cof, uint32_t depth, fbtStruct::Keys_t& keys)
{
    fbtStruct nof;
    nof.m_key.k16[0] = cp[0];
    nof.m_key.k16[1] = cp[1];
    nof.m_val.k32[0] = m_type[cp[0]].m_typeId;
    nof.m_val.k32[1] = m_base[cp[1]];
    nof.m_off = cof;
    nof.m_strcId = off->m_strcId;
    nof.m_nr = nr;
    nof.m_dp = depth;
    nof.m_link = 0;
    nof.m_flag = fbtStruct::CAN_LINK;
    nof.m_len = (m_name[cp[1]].m_ptrCount ? m_ptr : m_tlen[cp[0]]) * m_name[cp[1]].m_arraySize;
    nof.m_keyChain = keys;
    off->m_members.push_back(nof);
    cof += nof.m_len;

#ifdef _DEBUG
    //OGL_LOG(("%s %s\n") % getStructType(off) % getStructName(off));
    //OGL_LOG(("\t%s %s nr:%d cof:%d depth:%d") % getStructType(&nof) % getStructName(&nof) % nr % cof % depth);
#endif
}

FBTtype fbtBinTables::findTypeId(const fbtCharHashKey &cp)
{
    FBTsizeType pos = m_typeFinder.find(cp);
    if (pos != FBT_NPOS)
        return m_typeFinder.at(pos).m_strcId;
    return -1;
}

const char* fbtBinTables::getStructType(const fbtStruct* strc)
{

    //return strc ? m_type[strc->m_key.k16[0]].m_name : "";

    uint32_t k = strc ? strc->m_key.k16[0] : (uint32_t)-1;
    return  (k >= m_typeNr) ? "" : m_type[k].m_name;
}

const char* fbtBinTables::getStructName(const fbtStruct* strc)
{
    uint32_t k = strc ? strc->m_key.k16[1] : (uint32_t)-1;
    return  (k >= m_nameNr) ? "" : m_name[k].m_name;
}

const char* fbtBinTables::getOwnerStructName(const fbtStruct* strc)
{
    //cp0 = mp->m_type[mp->m_strc[c->m_strcId][0]].m_name;
    uint32_t k = strc ? strc->m_strcId : (uint32_t)-1;
    return (k >= m_strcNr || *m_strc[k] >= m_typeNr) ? "" : m_type[*m_strc[k]].m_name;
}