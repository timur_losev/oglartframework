#ifndef _fbtStreams_h_
#define _fbtStreams_h_


#include "fbtTypes.h"

/** \addtogroup FBT
*  @{
*/

class fbtStream
{
public:
    enum StreamMode
    {
        SM_READ = 1,
        SM_WRITE = 2,
    };

public:
    fbtStream() {}
    virtual ~fbtStream() {}

    virtual void open(const Path_t& path, fbtStream::StreamMode mode) {}
    virtual void clear(void) {};


    virtual bool isOpen(void) const = 0;
    virtual bool eof(void) const = 0;

    virtual FBTsize  read(void* dest, FBTsize nr) const = 0;
    virtual FBTsize  write(const void* src, FBTsize nr) = 0;
    virtual FBTsize  writef(const char* fmt, ...) { return 0; };

    virtual FBTsize  position(void) const = 0;
    virtual FBTsize  size(void) const = 0;

    virtual FBTsize seek(int32_t off, int32_t way) { return 0; }

protected:
    virtual void reserve(FBTsize nr) {}
};



class           fbtMemoryStream;
typedef void*   fbtFileHandle;


class fbtFileStream : public fbtStream
{
public:
    fbtFileStream();
    ~fbtFileStream();

    void open(const Path_t& path, fbtStream::StreamMode mode);
    void close();

    bool isOpen()   const { return m_handle != 0; }
    bool eof()      const;

    FBTsize  read(void* dest, FBTsize nr) const;
    FBTsize  write(const void* src, FBTsize nr);
    FBTsize  writef(const char* buf, ...);


    FBTsize  position() const;
    FBTsize  size()     const;
    FBTsize seek(int32_t off, int32_t way);

    void write(fbtMemoryStream &ms) const;

protected:

    Path_t              m_file;
    fbtFileHandle       m_handle;
    int                 m_mode;
    int                 m_size;
};




#if FBT_USE_GZ_FILE == 1

class fbtGzStream : public fbtStream
{
public:
    fbtGzStream();
    ~fbtGzStream();

    void open(const char* path, fbtStream::StreamMode mode);
    void close(void);

    bool isOpen(void)   const {return m_handle != 0;}
    bool eof(void)      const;

    FBTsize  read(void* dest, FBTsize nr) const;
    FBTsize  write(const void* src, FBTsize nr);
    FBTsize  writef(const char* buf, ...);


    FBTsize  position(void) const;
    FBTsize size(void) const;

    // watch it no size / seek

protected:


    fbtFixedString<272> m_file;
    fbtFileHandle       m_handle;
    int                 m_mode;
};


#endif


class fbtMemoryStream : public fbtStream
{
public:
    fbtMemoryStream();
    ~fbtMemoryStream();

    void clear(void);

    void open(fbtStream::StreamMode mode);
    void open(const char* path, fbtStream::StreamMode mode);
    void open(const fbtFileStream& fs, fbtStream::StreamMode mode);
    void open(const void* buffer, FBTsize size, fbtStream::StreamMode mode, bool compressed = false);


    bool     isOpen(void)    const   { return m_buffer != 0; }
    bool     eof(void)       const   { return !m_buffer || m_pos >= m_size; }
    FBTsize  position(void)  const   { return m_pos; }
    FBTsize  size(void)      const   { return m_size; }

    FBTsize  read(void* dest, FBTsize nr) const;
    FBTsize  write(const void* src, FBTsize nr);
    FBTsize  writef(const char* buf, ...);
#if FBT_USE_GZ_FILE == 1
    bool gzipInflate( char* inBuf, int inSize);
#endif
    void*            ptr(void)          { return m_buffer; }
    const void*      ptr(void) const    { return m_buffer; }

    FBTsize seek(int32_t off, int32_t way);


    void reserve(FBTsize nr);
protected:
    friend class fbtFileStream;

    char*            m_buffer;
    mutable FBTsize  m_pos;
    FBTsize          m_size, m_capacity;
    int              m_mode;
};

/** @}*/
#endif//_fbtStreams_h_
