#ifndef _fbtConfig_h_
#define _fbtConfig_h_


/** \addtogroup FBT
*  @{
*/

// global config settings

#define fbtDEBUG        1           // Traceback detail
#define fbtMaxTable     5000        // Maximum number of elements in a table
#define fbtMaxID        64          // Maximum character array length
#define fbtMaxMember    256         // Maximum number of members in a struct or class.
#define fbtDefaultAlloc 2048        // Table default allocation size
#define FBT_TYPE_LEN_VALIDATE   0   // Write a validation file (use MakeFBT.cmake->ADD_FBT_VALIDATOR to add a self validating build)
#define FBT_ARRAY_SLOTS         2   // Maximum dimensional array, eg: (int m_member[..][..] -> [FBT_ARRAY_SLOTS])

/** @}*/

#endif//_fbtConfig_h_
