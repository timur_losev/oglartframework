#include "Precompiled.h"

#include "fbtTypes.h"


#define FBT_IN_SOURCE
#include "fbtPlatformHeaders.h"


// ----------------------------------------------------------------------------
// Debug Utilities


#ifdef FBT_DEBUG
bool fbtDebugger::isDebugger(void)
{
#if FBT_COMPILER == FBT_COMPILER_MSVC
    return IsDebuggerPresent() != 0;
#else
    return false;
#endif
}

void fbtDebugger::breakProcess(void)
{
#if FBT_COMPILER == FBT_COMPILER_MSVC
    _asm int 3;
#else
    asm("int $3");
#endif
}

#else//FBT_DEBUG


bool fbtDebugger::isDebugger(void)
{
    return false;
}

void fbtDebugger::breakProcess(void)
{
}

#endif//FBT_DEBUG


#define FBT_DEBUG_BUF_SIZE (1024)
fbtDebugger::Reporter fbtDebugger::m_report = { 0, 0 };


void fbtDebugger::setReportHook(Reporter& hook)
{
    m_report.m_client = hook.m_client;
    m_report.m_hook = hook.m_hook;
}


void fbtDebugger::report(const char* fmt, ...)
{
    char ReportBuf[FBT_DEBUG_BUF_SIZE + 1];

    va_list lst;
    va_start(lst, fmt);
    int size = fbtp_printf(ReportBuf, FBT_DEBUG_BUF_SIZE, fmt, lst);
    va_end(lst);

    if (size < 0)
    {
        ReportBuf[FBT_DEBUG_BUF_SIZE] = 0;
        size = FBT_DEBUG_BUF_SIZE;
    }

    if (size > 0)
    {
        ReportBuf[size] = 0;

        if (m_report.m_hook)
        {
#if FBT_COMPILER == FBT_COMPILER_MSVC
            if (IsDebuggerPresent())
                OutputDebugStringA(ReportBuf);

#endif
            m_report.m_hook(m_report.m_client, ReportBuf);
        }
        else
        {

#if FBT_COMPILER == FBT_COMPILER_MSVC
            if (IsDebuggerPresent())
                OutputDebugStringA(ReportBuf);
            else
#endif
                fprintf(stderr, "%s", ReportBuf);
        }
    }

}


void fbtDebugger::reportIDE(const char* src, long line, const char* fmt, ...)
{
    static char ReportBuf[FBT_DEBUG_BUF_SIZE + 1];

    va_list lst;
    va_start(lst, fmt);


    int size = fbtp_printf(ReportBuf, FBT_DEBUG_BUF_SIZE, fmt, lst);
    va_end(lst);

    if (size < 0)
    {
        ReportBuf[FBT_DEBUG_BUF_SIZE] = 0;
        size = FBT_DEBUG_BUF_SIZE;
    }

    if (size > 0)
    {
        ReportBuf[size] = 0;
#if FBT_COMPILER == FBT_COMPILER_MSVC
        report("%s(%i): warning: %s", src, line, ReportBuf);
#else
        report("%s:%i: warning: %s", src, line, ReportBuf);
#endif
    }
}


void fbtDebugger::errorIDE(const char* src, long line, const char* fmt, ...)
{
    static char ReportBuf[FBT_DEBUG_BUF_SIZE + 1];

    va_list lst;
    va_start(lst, fmt);


    int size = fbtp_printf(ReportBuf, FBT_DEBUG_BUF_SIZE, fmt, lst);
    va_end(lst);

    if (size < 0)
    {
        ReportBuf[FBT_DEBUG_BUF_SIZE] = 0;
        size = FBT_DEBUG_BUF_SIZE;
    }

    if (size > 0)
    {
        ReportBuf[size] = 0;
#if FBT_COMPILER == FBT_COMPILER_MSVC
        report("%s(%i): error: %s", src, line, ReportBuf);
#else
        report("%s:%i: error: %s", src, line, ReportBuf);
#endif
    }
}

FBT_PRIM_TYPE fbtGetPrimType(uint32_t typeKey)
{
    static uint32_t charT = fbtCharHashKey("char").hash();
    static uint32_t ucharT = fbtCharHashKey("uchar").hash();
    static uint32_t shortT = fbtCharHashKey("short").hash();
    static uint32_t ushortT = fbtCharHashKey("ushort").hash();
    static uint32_t intT = fbtCharHashKey("int").hash();
    static uint32_t longT = fbtCharHashKey("long").hash();
    static uint32_t ulongT = fbtCharHashKey("ulong").hash();
    static uint32_t floatT = fbtCharHashKey("float").hash();
    static uint32_t doubleT = fbtCharHashKey("double").hash();
    static uint32_t voidT = fbtCharHashKey("void").hash();

    if (typeKey == charT)	return FBT_PRIM_CHAR;
    if (typeKey == ucharT)	return FBT_PRIM_UCHAR;
    if (typeKey == shortT)	return FBT_PRIM_SHORT;
    if (typeKey == ushortT)	return FBT_PRIM_USHORT;
    if (typeKey == intT)	return FBT_PRIM_INT;
    if (typeKey == longT)	return FBT_PRIM_LONG;
    if (typeKey == ulongT)	return FBT_PRIM_ULONG;
    if (typeKey == floatT)	return FBT_PRIM_FLOAT;
    if (typeKey == doubleT)	return FBT_PRIM_DOUBLE;
    if (typeKey == voidT)	return FBT_PRIM_VOID;

    return FBT_PRIM_UNKNOWN;
}

fbtList::Iterator::Iterator(List* list):
m_list(list)
{
    m_index = list ? list->first : nullptr;
}

bool fbtList::Iterator::hasMoreElements() const
{
    return !m_list && !m_index;
}

fbtList::Iterator::ListItem* fbtList::Iterator::getNext()
{
    ListItem* item = m_index;
    m_index = m_index->next;

    return item;
}
