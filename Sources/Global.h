/*
* File:   Global.h
* Author: void
*
* Created on March 27, 2013, 12:53 AM
*/

#ifndef GLOBAL_H
#    define	GLOBAL_H

namespace Global
{
    class MatrixPool
    {
    public:
        static glm::mat4* CurrentProjection;
        static glm::mat4* CurrentView;
        static glm::mat4* CurrentProjView;
    };

    class FileSystemMappings
    {
    public:
        static const Path_t   DataPath;
        static const Path_t   TexturesPath;
        static const String_t TexturesExt;
        static const String_t ShadersPath;

        class DeferredShaders
        {
        public:
            static const String_t ShadersPath;
        };
    };

    class Viewport
    {
    public:

        static glm::ivec2 Dimensions;
        static float FOV;
        static float Aspect();
        static float Near;
        static float Far;
    };

}

#endif	/* GLOBAL_H */

