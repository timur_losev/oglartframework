#include "Precompiled.h"
#include "HardwareElement.h"

HardwareElement::HardwareElement()
{
    memset(m_vboHandles, 0, TOTAL * sizeof(GLuint));
}

HardwareElement::~HardwareElement()
{
    // Slow, but safe
    for (uint32_t i = 0; i < TOTAL - 1 /*VAO is the last, don't touch it*/; ++i)
    {
        if (m_vboHandles[i] != 0)
        {
            glo::glDeleteBuffers(1, &m_vboHandles[i]);
        }
    }

    glo::glDeleteVertexArrays(1, &m_vboHandles[VAO]);

    /*if (m_vboHandles[0] != 0)
    {
        glo::glDeleteBuffers(TOTAL, m_vboHandles);
    }*/
}

bool HardwareElement::commited() const
{
    return m_vboHandles[VBO] > 0 || m_vboHandles[IBO] > 0;
}

void HardwareElement::bindIBO() const
{
    glo::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboHandles[IBO]);
}

void HardwareElement::bindVBO() const
{
    glo::glBindBuffer(GL_ARRAY_BUFFER, m_vboHandles[VBO]);
}

void HardwareElement::bindVAO() const
{
    glo::glBindVertexArray(m_vboHandles[VAO]);
}

void HardwareElement::unbindAll()
{
    glo::glBindBuffer(GL_ARRAY_BUFFER, 0);
    glo::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glo::glBindVertexArray(0);
}

bool HardwareElement::checkVirginity()
{
    int val = 0;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &val);
    if (val != 0) return false;

    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &val);
    if (val != 0) return false;

    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &val);
    if (val != 0) return false;

    return true;
}
