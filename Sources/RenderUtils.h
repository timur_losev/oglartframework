/*
 * File:   RenderUtils.h
 * Author: void
 *
 * Created on March 24, 2013, 11:37 PM
 */

#ifndef RENDERUTILS_H
#    define	RENDERUTILS_H

forward_this(ShaderProgram);

namespace rut
{
    template <class T>
    inline void glBufferData(GLenum target, const std::vector<T>& v, GLenum usage)
    {
        glo::glBufferData(target, v.size() * sizeof(T), &v[0], usage);
    }

    GLint shaderStatus(GLuint shader, GLenum param);

    GLint shaderProgramStatus(GLuint program, GLenum param);

    int glVertexAttribPointer(const std::string& attrib, ShaderProgramPtr program, GLint size,
        GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);

    void glEnableVertexAttribArray(int attr);
}

#endif	/* RENDERUTILS_H */

