#include "Precompiled.h"
#include "Application.h"
#include "Renderer.h"
#include "InputListener.h"

#include "Scene.h"

#include "Global.h"

#include "AppApi.h"

#include "Logger.h"
#include "GLFW\glfw3native.h"

SINGLETON_SETUP(Application);

Application::Application():
    hwnd(0)
{
    SINGLETON_ENABLE_THIS;
}

Application::~Application()
{
    delete m_logger;
}

void Application::setup(int argc, char* argv[])
{
    setupLogger();
    setupFrontEnd(argc, argv);
}

void Application::run()
{
    while(!glfwWindowShouldClose(m_window))
    {
        onDisplay();
        
        glfwSwapBuffers(m_window);
        glfwPollEvents();
    }
    
    glfwTerminate();
}

void Application::setupLogger()
{
    m_logger = new Logger();
    
    m_logger->setupFileLog("OglArt.log");
    
    OGL_TRACE(m_logger, "Boot");
}

void Application::setupFrontEnd(int argc, char* argv[])
{
    glfwInit();
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
#endif
    
    m_window = glfwCreateWindow(1024, 768, "GLFW Test", nullptr, nullptr);


    hwnd = (unsigned long)glfwGetWin32Window(m_window);

    glfwSetFramebufferSizeCallback(m_window, &Application::sOnReshape);
    glfwMakeContextCurrent(m_window);
    glfwSwapInterval( 1 );
    
    GLenum texError = glGetError();

    int width = 0, height = 0;
    glfwGetFramebufferSize(m_window, &width, &height);
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.f);

    const char * slVer = (const char *) glGetString ( GL_SHADING_LANGUAGE_VERSION );
    
    OGL_TRACE(m_logger, "Shader lang ver " << slVer);

    gl::init();

    setupSystemStuff();

#ifndef AS_SHADER_COMPILER
    renderer.reset(new Renderer());

    scene.reset(new Scene());
    renderer->init();

    scene->load("test.bdae");
    renderer->setTarget(scene);

    inputListener.reset(new InputListener());
    inputListener->addSublistener(scene);
    
    onReshape(m_window, width, height);
#endif
}

void Application::sOnDisplay()
{
    Application::GetRef().onDisplay();
}

void Application::sOnReshape(GLFWwindow* wnd, int x, int y)
{
    Application::GetRef().onReshape(wnd, x, y);
}

void Application::sOnIdle()
{
    Application::GetRef().onIdle();
}

static uint64_t prevTime;
static uint64_t deltaTime;

void Application::onDisplay()
{
#ifndef AS_SHADER_COMPILER
    onIdle(); //! make it true way: make render work at 60 fps and rest time for idle
    renderer->update(0);
    renderer->render();

    //glutPostRedisplay();
#endif
}

int inputCreationTime = 0;

void Application::onIdle()
{
#ifndef AS_SHADER_COMPILER
    prevTime = AppApiGetTickCount();

    //inputListener->performCapture((uint32_t)deltaTime);

    if (inputListener)
    {
        inputListener->performCapture((uint32_t)deltaTime);
    }
    else
    {
        if (inputCreationTime >= 3000)
        {
            inputListener.reset(new InputListener());
            inputListener->addSublistener(scene);
        }

        inputCreationTime += deltaTime;
    }

    //renderer->update((uint32_t)deltaTime);

    uint64_t nowTime = AppApiGetTickCount();
    deltaTime = nowTime - prevTime;
#endif
}

void Application::onReshape(GLFWwindow*, int x, int y )
{
    glViewport ( 0, 0, (GLsizei) x, (GLsizei) y );

    Global::Viewport::Dimensions = glm::ivec2(x, y);
    
#ifndef AS_SHADER_COMPILER
    if (renderer)
        renderer->onReshape();

#endif
}

void WINAPI Application::sOnGLDebugOutput( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam )
{
    char debSource[32], debType[32], debSev[32];
    bool Error(false);

    const GLuint texture_usage_warning_flood = 131204;

    if (id == texture_usage_warning_flood)
    {
        return;
    }

    if (source == GL_DEBUG_SOURCE_API_ARB)
        strcpy(debSource, "OpenGL");
    else if (source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
        strcpy(debSource, "Windows");
    else if (source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
        strcpy(debSource, "Shader Compiler");
    else if (source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
        strcpy(debSource, "Third Party");
    else if (source == GL_DEBUG_SOURCE_APPLICATION_ARB)
        strcpy(debSource, "Application");
    else if (source == GL_DEBUG_SOURCE_OTHER_ARB)
        strcpy(debSource, "Other");

    if (type == GL_DEBUG_TYPE_ERROR_ARB)
        strcpy(debType, "error");
    else if (type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
        strcpy(debType, "deprecated behavior");
    else if (type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
        strcpy(debType, "undefined behavior");
    else if (type == GL_DEBUG_TYPE_PORTABILITY_ARB)
        strcpy(debType, "portability");
    else if (type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
        strcpy(debType, "performance");
    else if (type == GL_DEBUG_TYPE_OTHER_ARB)
        strcpy(debType, "message");

    if (severity == GL_DEBUG_SEVERITY_HIGH_ARB)
    {
        strcpy(debSev, "high");
        Error = true;
    }
    else if (severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
    {
        strcpy(debSev, "medium");
    }
    else if (severity == GL_DEBUG_SEVERITY_LOW_ARB)
    {
        strcpy(debSev, "low");
    }
    else if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
    {
        strcpy(debSev, "notification");
    }
    

    OGL_LOG(("%s: %s(%s) %d: %s") % debSource % debType % debSev % id % message);
}

void Application::setupSystemStuff()
{
#ifdef GL_ARB_debug_output
    if (glo::glDebugMessageControl)
    {
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        glo::glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
        glo::glDebugMessageCallback(&Application::sOnGLDebugOutput, NULL);
    }
#endif

    printExtensions();
}

void Application::printExtensions()
{
    {
        GLint nExt = 0;
        glGetIntegerv(GL_NUM_EXTENSIONS, &nExt);

        for(int i = 0; i < nExt; ++i)
        {
            OGL_TRACE(m_logger, glo::glGetStringi(GL_EXTENSIONS, i));
        }
    }

    {
        GLint nMaxDrawBuffers = 0;

        glGetIntegerv(GL_MAX_DRAW_BUFFERS, &nMaxDrawBuffers);

        OGL_TRACE(m_logger, "Max draw buffers " << nMaxDrawBuffers);
    }

    {

    }
}

unsigned long Application::getWindowHandle() const
{
    return hwnd;
}

void Application::sOnKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    Application::GetRef().onKey(window, key, scancode, action, mods);
}

void Application::onKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

int main(int argc, char** argv)
{
    GLenum texError = glGetError();

    Application app;
    app.setup(argc, argv);
    
    app.run();
    
    return 0;
}


