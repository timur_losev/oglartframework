/*
 * File:   Mesh.h
 * Author: void
 *
 * Created on April 6, 2013, 4:08 PM
 */

#ifndef MESH_H
#    define	MESH_H

#include "Vertex.h"
#include "Face.h"
#include "BoxSphereBounds.h"

class Mesh
{
public:
    typedef std::vector<Vertex> Vertices_t;
    typedef std::vector<Face>   Faces_t;

    Vertices_t  vertices;
    Faces_t     faces;

    BoxSphereBounds bounds;

    Mesh();
    virtual ~Mesh();

    void        buildBounds();
};

#endif	/* MESH_H */

