#include "Precompiled.h"
#include "RenderTechnique.h"
#include "RenderPass.h"


RenderTechnique::RenderTechnique()
{

}

RenderTechnique::~RenderTechnique()
{

}

RenderPassPtr RenderTechnique::getPass( size_t index ) const
{
    assert(index < m_passes.size());

    return m_passes[index];
}

const std::string& RenderTechnique::getName() const
{
    return m_name;
}

void RenderTechnique::setName( const std::string& n)
{
    m_name = n;
}

void RenderTechnique::addPass(RenderPassPtr p, int at /*= -1*/ )
{
    if (at >= 0 && at < (int)m_passes.size())
    {
        p->setId(at);
        m_passes.insert(m_passes.begin() + at, p);
    }
    else
    {
        p->setId(m_passes.size());
        m_passes.push_back(p);
    }
}

size_t RenderTechnique::getPassesCount() const
{
    return m_passes.size();
}

RenderPassPtr RenderTechnique::switchPass( size_t switchToPassId )
{
    RenderPassPtr ps = getPass(switchToPassId);
    ps->switchToIt();

    m_currentPass = ps;

    return ps;
}

RenderPassPtr RenderTechnique::getCurrentPass() const
{
    return m_currentPass;
}

