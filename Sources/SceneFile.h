#pragma once

#include "fbtBlend.h"
#include "fbt/Blender/Blender.h"

forward_this(Scene);

class SceneFile
{
private:
    fbtBlend* m_file;

    ScenePtr m_requester;
protected:

    float m_fps = 0.f;

protected:


    void _readCurrentSceneInfo(Blender::Scene* sc);
    void _buildAllTextures();

public:

    SceneFile(ScenePtr requester);
    ~SceneFile();

    bool parse(const Path_t& fname);
    bool parse(const void* mem, uint32_t size);

    Blender::FileGlobal*        getFileGlobal() const;
    Blender::Scene*             getFirstScene() const;

    int                         getVersion() const;

    fbtList::Iterator           getScenes() const;
    fbtList::Iterator           getTexts() const;
    
    fbtList::Iterator           getObjects() const;
    fbtList::Iterator           getMeshes() const;
    fbtList::Iterator           getLamps() const;
    fbtList::Iterator           getCameras() const;
    
    fbtList::Iterator           getMaterials() const;
    fbtList::Iterator           getTextures() const;
    fbtList::Iterator           getImages() const;

    fbtList::Iterator           getIpos() const;
    fbtList::Iterator           getKeys() const;
    fbtList::Iterator           getWorlds() const;

    fbtList::Iterator           getScripts() const;
    fbtList::Iterator           getVFonts() const;
    fbtList::Iterator           getSounds() const;
    fbtList::Iterator           getGroups() const;
    fbtList::Iterator           getArmatures() const;
    fbtList::Iterator           getActions() const;
    fbtList::Iterator           getParticles() const;

    fbtList::Iterator           getLatts() const;
    fbtList::Iterator           getCurves() const;
    fbtList::Iterator           getLibrarys() const;
    fbtList::Iterator           getNodeTrees() const;
    fbtList::Iterator           getMBalls() const;



    void                        loadActiveScene();
};