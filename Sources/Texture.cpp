#include "Precompiled.h"
#include "Texture.h"

Texture::Texture():
texture(0), target(GL_TEXTURE_2D), isMipmapped(0)
{
    memset(&imageDimentsion, 0, sizeof(ImageDimension_t));
}

Texture::~Texture()
{
    //LOG_TODO("Implement destructor");
    glDeleteTextures(1, &texture);
}

void Texture::bind() const
{
    glBindTexture(target, texture);
}