#pragma once

#include "RenderPass.h"

forward_this(GBufferPass);

class GBufferPass : public RenderPass
{
private:
    uint32_t m_modelMatLoc;
    uint32_t m_addInfoLoc;
protected:
    virtual void commitImpl();
public:

    GBufferPass();
    ~GBufferPass();

    virtual void use();
    void useTexture(uint32_t layer, uint32_t index);
};