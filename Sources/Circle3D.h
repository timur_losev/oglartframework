#pragma once

#include "Vertex.h"
#include "HardwareElement.h"

forward_this(Material);

// IMPORTANT: Use this class for debug visualize only

class Circle3D: public HardwareElement
{
protected:
    MaterialPtr m_material;
public:
    std::vector<Vertex> segments;
    vec3 origin;
    float radius;
public:

    Circle3D(const vec3& origin, float radius, size_t segments, const Color_t& col);

    virtual bool commit(GLenum usage = GL_STATIC_DRAW);

    void                    setMaterial(MaterialPtr mat);
    MaterialPtr             getMaterial();
    MaterialConstPtr        getMaterial() const;
};