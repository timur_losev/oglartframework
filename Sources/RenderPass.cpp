#include "Precompiled.h"
#include "RenderPass.h"
#include "Texture.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"

#include "Logger.h"

RenderPass::RenderPass():
    m_id(INVALID_ID)
{
    m_logger = GetLoggerPtr();
}

RenderPass::~RenderPass()
{

}

void RenderPass::setTexture(uint32_t layer, TexturePtr texture)
{
    assert(texture);
    if (!texture)
    {
        OGL_TRACE_ERROR(m_logger, "Nullptr texture!");
        return;
    }

    LayredTextures_t::iterator it = m_textures.find(layer);

    if (it == m_textures.end())
    {
        Textures_t newLayer;
        newLayer.push_back(texture);
        m_textures[layer] = newLayer;
    }
    else
    {
        it->second.push_back(texture);
    }
}

void RenderPass::dumpAttributes()
{
    struct Dumper: boost::static_visitor<>
    {
        MaterialAttributes::Enum attrId;
        Logger* m_logger;

        Dumper(MaterialAttributes::Enum id, Logger* l):
        attrId(id), m_logger(l){}

        void operator()(float val) const
        {
            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Attribute %s value REAL = %f") % MaterialAttributes::toString(attrId) % val));
        }

        void operator()(const Color_t& val) const
        {
            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Attribute %s value RGBA = %f%f%f%f") % MaterialAttributes::toString(attrId) % val.r % val.g % val.b % val.a));
        }

        void operator()(const String_t& val) const
        {
            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Attribute %s value STRING = %s") % MaterialAttributes::toString(attrId) % val));
        }

        void operator()(Bool_t val) const
        {
            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Attribute %s value BOOL = %s") % MaterialAttributes::toString(attrId) % val));
        }
    };

    OGL_TRACE_DEBUG(m_logger, "Dump attributes");

    for(auto i = m_attributesMap.begin(), e = m_attributesMap.end(); i != e; ++i)
    {
        Dumper dumper(i->first, m_logger);
        boost::apply_visitor(dumper, i->second);
    }
}

void RenderPass::setShaderProgram( ShaderProgramPtr program )
{
    m_shaderProgram = program;
}

ShaderProgramPtr RenderPass::getShaderProgram() const
{
    return m_shaderProgram;
}

const RenderPass::LayredTextures_t& RenderPass::getTextures() const
{
    return m_textures;
}

void RenderPass::switchToIt()
{
    assert("Operation is not allowed on incomplete pass" && m_shaderProgram);
    assert("This pass does not have an ID" && m_id != INVALID_ID);

    setUniform(m_id, UniformAccessors::RenderState);
}

void RenderPass::setId( uint32_t id )
{
    m_id = id;
}

uint32_t RenderPass::getId() const
{
    return m_id;
}

void RenderPass::commit()
{
    ShaderProgramManager::use(m_shaderProgram);

    m_uniformAccessors[UniformAccessors::MVP] = m_shaderProgram->getUnsafe("MVP");
    m_uniformAccessors[UniformAccessors::ModelViewMatrix] = m_shaderProgram->getUnsafe("ModelView");
    m_uniformAccessors[UniformAccessors::RenderState] = m_shaderProgram->getUnsafe("RenderPass");
    m_uniformAccessors[UniformAccessors::NormalMatrix] = m_shaderProgram->getUnsafe("NormalMatrix");
    m_uniformAccessors[UniformAccessors::ModelMatrix] = m_shaderProgram->getUnsafe("ModelMatrix");

    commitImpl();
}

void RenderPass::setUniform( uint32_t v, UniformAccessors::Enum e )
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniform1ui(loc, v);
    }
}

void RenderPass::setUniform( const mat4& v, UniformAccessors::Enum e)
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniformMatrix4fv(loc, 1, GL_FALSE, &v[0][0]);
    }
}

void RenderPass::setUniform( const vec3& v, UniformAccessors::Enum e)
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniform3fv(loc, 1, &v[0]);
    }
}

void RenderPass::setUniform( const vec4& v, UniformAccessors::Enum e)
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniform4fv(loc, 1, &v[0]);
    }
}

void RenderPass::setUniform( const vec2& v, UniformAccessors::Enum e)
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniform2fv(loc, 1, &v[0]);
    }
}

void RenderPass::setUniform( float v, UniformAccessors::Enum e)
{
    uint32_t loc = m_uniformAccessors[e];

    if (loc != INVALID_ID)
    {
        glo::glUniform1f(loc, v);
    }
}

