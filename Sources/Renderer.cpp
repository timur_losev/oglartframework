/*
* File:   Renderer.cpp
* Author: void
*
* Created on March 23, 2013, 11:25 PM
*/
#include "Precompiled.h"

#include "Renderer.h"
#include "Global.h"
#include "Scene.h"
#include "GraphicMesh.h"
#include "VolumeMesh.h"
#include "Material.h"
#include "Camera.h"
#include "Light.h"

#include "RenderTechnique.h"

#include "RenderPass.h"
#include "GBufferPass.h"
#include "NullPass.h"
#include "ScreenQuadRenderPass.h"
#include "LightPass.h"
#include "DirLightPass.h"

#include "ShaderProgram.h"
#include "ShaderProgramManager.h"
#include "TextureManager.h"
#include "MeshManager.h"
#include "MaterialManager.h"
#include "DeferredMaterialManager.h"

#include "Texture.h"
#include "GeometryBuffer.h"

#include "ScreenQuad.h"

#include "Line3D.h"
#include "Circle3D.h"

#include "Compositor.h"

Renderer::Renderer()
{
}

Renderer::~Renderer()
{

}

void Renderer::init()
{
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
   /* glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);*/
    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
}

void Renderer::setTarget(ScenePtr s)
{
    m_target = s;
    m_meshManagerRef = m_target->getMeshManager();
    setUpRenderTechnique();
}

void Renderer::setUpRenderTechnique()
{
    m_compositor.reset(new Compositor(ivec2(1024, 768)));
    DeferredMaterialManager& dmm = m_meshManagerRef.lock()->getMaterialManager().getDeferredMaterialManager();
    m_screenQuad.reset(new ScreenQuad(dmm));
}

void Renderer::onReshape()
{
    if (m_target && m_target->getCamera())
    {
        m_target->getCamera()->reshape(Global::Viewport::Aspect());
    }
}

void Renderer::render()
{
    if (m_target)
    {
        m_target->lock();

        m_compositor->getGeometryBuffer()->startFrame();
        
        deferredGeometryPass();
    
        // We need stencil to be enabled in the stencil pass to get the stencil buffer
        // updated and we also need it in the light pass because we render the light
        // only if the stencil passes.
        glEnable(GL_STENCIL_TEST);
    
        const uint32_t count = m_target->getLightsInFrustumCount();

        for(uint32_t i = 0; i < count; ++i)
        {
            deferredStencilPass(i);
            deferredLightPass(i);
        }
    
        // is unlimited and the final pass simply copies the texture.
        // The directional light does not need a stencil test because its volume
        glDisable(GL_STENCIL_TEST);

        deferredDirectionalLightPass();
        deferredFinalPass();
        
        m_target->unlock();
    }
}

void Renderer::deferredGeometryPass()
{
    m_compositor->getGeometryBuffer()->bindForGeometryPass();

    // Only the geometry pass updates the depth buffer
    glDepthMask(GL_TRUE);
    glClearColor(0,0,0,0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    //! Might be removed
    //--
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    //--

    m_target->render(*this);
    //m_target->renderLightVolume(*this);

    // When we get here the depth buffer is already populated and the stencil pass
    // depends on it, but it does not write to it.
    glDepthMask(GL_FALSE);
}

void Renderer::deferredStencilPass(uint32_t i)
{
    m_compositor->getGeometryBuffer()->bindForStencilPass();
    m_renderState = NULL_PASS_ID;

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glClear(GL_STENCIL_BUFFER_BIT);

    // We need the stencil test to be enabled but we want it
    // to succeed always. Only the depth test matters.
    glStencilFunc(GL_ALWAYS, 0, 0);

    glo::glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    glo::glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

    m_target->renderLightVolume(*this, i);
}

void Renderer::deferredLightPass(uint32_t i)
{
    m_compositor->getGeometryBuffer()->bindForLightPass();

    m_renderState = LIGHT_PASS_ID;

    glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glo::glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    m_target->renderLightVolume(*this, i);

    glCullFace(GL_BACK);
    glDisable(GL_BLEND);
}

void Renderer::deferredDirectionalLightPass()
{
    m_compositor->getGeometryBuffer()->bindForLightPass();
    m_renderState = DIR_LIGHT_PASS_ID;

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glo::glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    {
        GraphicMeshPtr mesh = m_screenQuad->getQuadMesh();

        mesh->bindVAO();

        DirLightPassPtr pass =  std::static_pointer_cast<DirLightPass>(
            mesh->getMaterial()->getTechnique(0)->getPass(0));
        pass->use();

        //pass->setModelView(mat);

        m_screenQuad->render();
    }

    glDisable(GL_BLEND);
}

void Renderer::deferredFinalPass()
{
    m_compositor->getGeometryBuffer()->bindForFinalPass();
    glo::glBlitFramebuffer(0, 0, 1024, 768,
        0, 0, 1024, 768, GL_COLOR_BUFFER_BIT, GL_LINEAR);
}

void Renderer::update( uint32_t dt )
{
    if (m_target)
    {
        m_target->update(dt);
    }
}

void Renderer::drawMesh( GraphicMeshConstPtr mesh, const mat4& transform, const mat4& model ) const
{
    HardwareElement::unbindAll();

    MaterialConstPtr material = mesh->getMaterial();
    const size_t tcount = material->getTechniquesCount();

    for(size_t ti = 0; ti < tcount; ++ti)
    {
        RenderTechniqueConstPtr tech = material->getTechnique(ti);
        const size_t pcount = tech->getPassesCount();

        for(size_t pi = 0; pi < pcount; ++pi)
        {
            RenderPassPtr pass = tech->getPass(pi);
            pass->use();

            mesh->bindVAO();
            mesh->bindIBO();

            pass->setUniform(transform, UniformAccessors::MVP);

            const mat4 modelView = m_target->getCamera()->getViewMatrix() * model;

            mat4 normalMatrix = glm::transpose(glm::inverse(modelView)); //! PERF_WARN: In case of model view is orthogonal we can avoid inverse-transpose

            pass->setUniform(normalMatrix, UniformAccessors::NormalMatrix);
            pass->setUniform(modelView, UniformAccessors::ModelViewMatrix);

            const RenderPass::LayredTextures_t& textures = pass->getTextures();

            //! TEMP SOLUTION FOR SINGLE TEXTURE
            RenderPass::LayredTextures_t::const_iterator it = textures.find(GL_TEXTURE0);

            assert(it != textures.cend());

            TextureConstPtr texture0 = it->second[0]; //Must be!

            glo::glActiveTexture(GL_TEXTURE0);
            //if (TextureManager::use(texture0))
            texture0->bind();
            {
                int tex0loc = pass->getShaderProgram()->getUnsafe("Texture0");
                if (tex0loc > -1)
                {
                    glo::glUniform1i(tex0loc, 0);
                }
            }
#if USE_NORMAL_MAPPING
            it = textures.find(GL_TEXTURE1);
            assert(it != textures.end());
            TextureConstPtr texture1 = it->second[0]; //Must be!

            glo::glActiveTexture(GL_TEXTURE1);
            texture1->bind();
            {
                int texloc = pass->getShaderProgram()->getUnsafe("Texture1");
                if (texloc > -1)
                {
                    glo::glUniform1i(texloc, 1);
                }
            }
#endif
            glDrawElements(GL_TRIANGLES, mesh->faces.size() * 3, GL_UNSIGNED_INT, nullptr);
        }
    }
}

void Renderer::drawLine3D(Line3DConstPtr line, const mat4& transform) const
{
    line->bindVAO();

    RenderPassPtr pass = line->getMaterial()->getTechnique(0)->getPass(0);

    pass->use();
    pass->setUniform(transform, UniformAccessors::MVP);

    glDrawArrays(GL_LINES, 0, 2);
}

void Renderer::drawCircle3D( Circle3DConstPtr circle, const mat4& transform ) const
{
    circle->bindVAO();

    RenderPassPtr pass = circle->getMaterial()->getTechnique(0)->getPass(0);

    pass->use();
    pass->setUniform(transform, UniformAccessors::MVP);

    glDrawArrays(GL_LINES, 0, circle->segments.size());
}

void Renderer::drawTestLight(LightPtr light, const mat4& modelView, const mat4& transform) const
{
    HardwareElement::unbindAll();

    if (m_renderState == NULL_PASS_ID)
    {
        VolumeMeshPtr mesh =  light->lightGeometry;

        mesh->bindVAO();
        mesh->bindIBO();

        RenderPassPtr renderPass = mesh->getMaterial()->getTechnique(0)->getPass(0);

        renderPass->use();
        renderPass->setUniform(transform, UniformAccessors::MVP);

        glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, nullptr);
    }
    else if (m_renderState == LIGHT_PASS_ID)
    {
        VolumeMeshPtr mesh =  light->lightGeometry;

        mesh->bindVAO();
        mesh->bindIBO();

        LightPassPtr pass =  std::static_pointer_cast<LightPass>(mesh->getMaterial()->getTechnique(0)->getPass(1));

        light->directionViewSpace = mat3(modelView) * light->direction;
        glm::normalize(light->directionViewSpace);
        pass->use(light);
        pass->setUniform(transform, UniformAccessors::MVP);
        pass->setUniform(modelView[3].xyz, UniformAccessors::LightPositionViewSpace);
        pass->setUniform(modelView, UniformAccessors::ModelViewMatrix);

        {
            vec4 v = m_target->getCamera()->getViewMatrix() * vec4(m_target->getCamera()->getPosition(), 1.f);

            pass->setUniform(v.xyz, UniformAccessors::EyePositionViewSpace);
        }

        glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, nullptr);
    }
    else if (m_renderState == DIR_LIGHT_PASS_ID)
    {
    }
}