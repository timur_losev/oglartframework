/*
 * File:   Scene.cpp
 * Author: void
 *
 * Created on April 3, 2013, 11:28 PM
 */

#include "Precompiled.h"
#include "Scene.h"

#include "Node.h"
#include "GraphicMesh.h"
#include "VolumeMesh.h"

#include "MeshManager.h"
#include "MaterialManager.h"
#include "DeferredMaterialManager.h"
#include "LightManager.h"

#include "ColladaParser.h"
#include "Global.h"

#include "Camera.h"
#include "Renderer.h"
#include "Frustum.h"

#include "Line3D.h"
#include "Circle3D.h"
#include "Light.h"

#include "MathUtils.h"

#include "Logger.h"

#include "SceneFile.h"


Scene::Scene():
m_locked(FALSE),
m_renderDebug(FALSE)
{ }

Scene::~Scene() { }

NodePtr Scene::buildHierarchy(Collada::NodeConstPtr n)
{
    std::string name = findNameForNode(n);
    NodePtr node;

    if (!n->lights.empty())
    {
        node.reset(new Light(name));
        LightPtr lightNode = node->asLightNode();
        lightManager->buildLightsForNode(colladaParser, n, lightNode);

        //! WARN: Single light here!!! TODO: Make it true
        //! WARN: Very ugly solution. Rework this asap
        assert(lightNode->lightGeometry);

        const DeferredMaterialManager& defferedMaterialManager =
            meshManager->getMaterialManager().getDeferredMaterialManager();

        defferedMaterialManager.applyLightMaterial(lightNode);

        assert(lightNode->lightGeometry->commit());

        {
            Circle3DPtr cirVis = lightNode->lightGeometry->bounds.getCircleVisualization();

            cirVis->setMaterial(defferedMaterialManager.createObjectOverlayMaterial("CirVisMat"));
            cirVis->commit();
        }

        {
            Line3DPtr radVis = lightNode->lightGeometry->bounds.getRadiusVisualization();
            radVis->setMaterial(defferedMaterialManager.createObjectOverlayMaterial("RadVis"));

            radVis->commit();
        }
    }
    else
    {
        node.reset(new Node(name));
    }

    OGL_TRACE_DEBUG(GetLoggerPtr(), "Created node " << name);

    node->setTransform(colladaParser->CalculateResultTransform(n->transforms));

    std::vector<Collada::NodeConstPtr> instances;
    resolveNodeInstances(n, instances);

    for (auto i = n->children.begin(), e = n->children.end(); i != e; ++i)
    {
        NodePtr childNode = buildHierarchy(*i);
        node->addChild(childNode);
    }

    // resolved instances
    for (auto i = instances.begin(), e = instances.end(); i != e; ++i)
    {
        NodePtr childNode = buildHierarchy(*i);
        node->addChild(childNode);
    }

    meshManager->buildMeshesForNode(colladaParser, n, node);

    return node;
}

void Scene::resolveNodeInstances(Collada::NodeConstPtr n, std::vector<Collada::NodeConstPtr>& resolved)
{
    resolved.reserve(n->nodeInstances.size());

    for (auto i = n->nodeInstances.cbegin(), e = n->nodeInstances.cend(); i != e; ++i)
    {
        const auto it = colladaParser->nodeLibrary.find(i->node);
        Collada::NodeConstPtr node = it == colladaParser->nodeLibrary.end() ? nullptr : it->second;

        if (!node)
        {
            node = findNode(colladaParser->rootNode, i->node);
        }

        if (!node)
        {
            OGL_TRACE_ERROR(GetLoggerPtr(), "Unable to resolve reference to instanced node " + i->node);
        }
        else
        {
            resolved.push_back(node);
        }
    }
}

Collada::NodeConstPtr Scene::findNode(Collada::NodeConstPtr n, const std::string& name) const
{
    if (n->name == name || n->ID == name)
        return n;

    for (auto i = n->children.cbegin(), e = n->children.cend(); i != e; ++i)
    {
        Collada::NodeConstPtr node = findNode(*i, name);

        if (node)
        {
            return node;
        }
    }

    return nullptr;
}

std::string Scene::findNameForNode(Collada::NodeConstPtr n) const
{
    if (!n->name.empty())
    {
        return n->name;
    }
    else if (!n->ID.empty())
    {
        return n->ID;
    }
    else if (!n->SID.empty())
    {
        return n->SID;
    }
    else
    {
        std::ostringstream ss;

        ss << "_auto_name_" << clock();

        return ss.str();
    }
}

bool Scene::commitStaticRecursive(NodePtr node)
{
    OGL_TRACE_ASSERT(GetLoggerPtr(), node, "Where is root node?");

    BOOST_FOREACH(NodePtr& curNode, node->children)
    {
        BOOST_FOREACH(GraphicMeshPtr& mesh, curNode->meshes)
        {
            OGL_TRACE_ASSERT(GetLoggerPtr(), !mesh->commited(), "Double commit of static geometry detected.")

            if (!mesh->commit())
            {
                OGL_TRACE_ASSERT(GetLoggerPtr(), false, "Could not to commit to VRAM");
                return false;
            }
        }

        if (!commitStaticRecursive(curNode))
        {
            return false;
        }
    }

    return true;
}

bool Scene::load(const std::string& filename)
{
    //SceneFile bscene(this->shared_from_this());
    //bscene.parse("c:\\Users\\void\\Documents\\1.blend");

    colladaParser.reset(new Collada::ColladaParser(filename));

    Collada::NodePtr colRoot = colladaParser->getRootNode();

    assert(colRoot);

    meshManager.reset(new MeshManager());
    meshManager->getMaterialManager().buildMaterials(colladaParser);

    lightManager.reset(new LightManager());

    root = buildHierarchy(colRoot);

    //meshManager->dumpMeshes();

    correctAxis();

    if (!commitStaticRecursive(root))
    {
        return false;
    }

    camera.reset(new Camera());

    camera->perspective(
        Global::Viewport::FOV,
        Global::Viewport::Aspect(),
        Global::Viewport::Near,
        Global::Viewport::Far);

    NodePtr tempCameraNode = root->getChild("Camera");

    camera->setBehavior(Camera::CAMERA_BEHAVIOR_FIRST_PERSON);

    vec3 camPos = tempCameraNode ? tempCameraNode->getPosition() : glm::vec3(0.0, -25.0, 0.0);

    camera->lookAt(
        camPos,
        glm::vec3(0.0, 0.0, 0.0),
        glm::vec3(0.0, 0.0, 1.0));
    camera->setVelocity(2.0f, 2.0f, 2.0f);
    camera->setAcceleration(8.0f, 8.0f, 8.0f);

    colladaParser.reset();

    return true;
}

void Scene::correctAxis()
{
    /*if (colladaParser->upDirection == Collada::ColladaParser::UP_Z)
    {
        root->transformation *= glm::mat4(
                1, 0, 0, 0,
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1);
    }*/
}

void Scene::update(uint32_t dt)
{
    if (m_locked == FALSE)
    {
        m_lightVolumes.clear();
        applyFrustumRecursive(root, camera->getFrustum());
    }
}

void Scene::applyFrustum( LightPtr curNode, FrustumPtr frustum)
{
    const glm::mat4& model = curNode->getTransform();
    glm::mat4 mvp = camera->getViewProjMatrix() * model;

    mat4 vm = camera->getViewMatrix() * model;

    BoxSphereBounds b = curNode->lightGeometry->bounds.transform(model);

    if (frustum->inside(b))
    {
        LightTransformPair_t p(std::make_pair(curNode, std::make_tuple(vm, mvp)));
        m_lightVolumes.push_back(p);
    }
    else
    {
        //OGL_TRACE_DEBUG(logger, curNode->getName() + " outside the frustum");
    }

    if (m_renderDebug)
    {
        m_renderables.push_back(std::make_pair(curNode->lightGeometry->bounds.getRadiusVisualization(), mvp));
        m_renderables.push_back(std::make_pair(curNode->lightGeometry->bounds.getCircleVisualization(), mvp));
        m_renderables.push_back(std::make_pair(curNode->lightGeometry->bounds.getCircleVisualization(), glm::rotate(mvp, 90.f, vec3(0.f, 0.f, 1.f))));
    }
}

void Scene::applyFrustum( NodePtr curNode, FrustumPtr frustum)
{
    const glm::mat4& model = curNode->getTransform();
    glm::mat4 mvp = camera->getViewProjMatrix() * model;

    BOOST_FOREACH(GraphicMeshPtr& mesh, curNode->meshes)
    {
        BoxSphereBounds b = mesh->bounds.transform(model);

        if (frustum->inside(b))
        {
            MeshTransformPair_t p(std::make_pair(mesh, std::make_tuple(mvp, model)));
            m_renderables.push_back(p);
        }
        /*else
        {
            OGL_TRACE_DEBUG(logger, mesh->name + " outside the frustum");
        }*/

        if (0 && m_renderDebug) //! Generates an error, debug rendering is not nessesary in that case, so fix this in low prio
        {
            m_renderables.push_back(std::make_pair(mesh->bounds.getRadiusVisualization(), mvp));
            m_renderables.push_back(std::make_pair(mesh->bounds.getCircleVisualization(), mvp));
            m_renderables.push_back(std::make_pair(mesh->bounds.getCircleVisualization(), glm::rotate(mvp, 90.f, vec3(0.f, 0.f, 1.f))));
        }
    }
}

void Scene::applyFrustumRecursive( NodePtr node, FrustumPtr frustum)
{
    BOOST_FOREACH(NodePtr& curNode, node->children)
    {
        if (curNode->getNodeType() == Node::ENT_LIGHT)
        {
            applyFrustum(curNode->asLightNode(), frustum);
        }
        else
        {
            applyFrustum(curNode, frustum);
        }

        applyFrustumRecursive(curNode, frustum);
    }
}

void Scene::render(Renderer& renderer)
{
    struct RenderVisitor: public boost::static_visitor<>
    {
        const Renderer& renderer;

        RenderVisitor(const Renderer& r): renderer(r) {}

        void operator() (const MeshTransformPair_t& p) const
        {
            renderer.drawMesh(p.first, std::get<0>(p.second), std::get<1>(p.second));
        }

        void operator() (const Line3DTransformPair_t& p) const
        {
            renderer.drawLine3D(p.first, p.second);
        }

        void operator() (const Circle3DTransformPair_t& p) const
        {
            renderer.drawCircle3D(p.first, p.second);
        }

        void operator() (const LightTransformPair_t& p) const
        {
            // Stub
        }
    };

    RenderVisitor rv(renderer);

    std::for_each(m_renderables.begin(), m_renderables.end(), boost::apply_visitor(rv));

    m_renderables.clear();
}

void Scene::renderLightVolume( Renderer& renderer, uint32_t i)
{
    Renderables_t::iterator it = m_lightVolumes.begin();
    std::advance(it, i);
    const LightTransformPair_t& p = boost::get<LightTransformPair_t>(*it);
    renderer.drawTestLight(p.first, std::get<0>(p.second), std::get<1>(p.second));
}

void Scene::nonBufferedUpdateInput( OIS::Keyboard* keyboard, OIS::Mouse* mouse, uint32_t dt)
{
    
#if 1
    vec3 translateVec;

    float rotx = 0.f, roty = 0.f;

    float changeVal = 1.f;

    if (keyboard->isKeyDown(OIS::KC_LSHIFT))
    {
        changeVal *= 4;
    }

    if (keyboard->isKeyDown(OIS::KC_W))
    {
        translateVec.z = changeVal;
    }

    if (keyboard->isKeyDown(OIS::KC_S))
    {
        translateVec.z = -changeVal;
    }

    if (keyboard->isKeyDown(OIS::KC_A))
    {
        translateVec.x = -changeVal;
    }

    if (keyboard->isKeyDown(OIS::KC_D))
    {
        translateVec.x = changeVal;
    }

    if (keyboard->isKeyDown(OIS::KC_HOME))
    {
        camera->lookAt(
            glm::vec3(0.0, 25.0, 0.0),
            glm::vec3(0.0, 0.0, 0.0),
            glm::vec3(0.0, 0.0, 1.0));
    }

    if (!mouse->buffered())
    {
        const OIS::MouseState& ms = mouse->getMouseState();

        if (ms.buttonDown(OIS::MB_Left))
        {
            float fdt = (float)dt;

            if (fdt <= 0.f)
            {
                fdt = 0.005f;
            }
            else
            {
                fdt /= 1000.f;
                //OGL_TRACE_DEBUG(logger, "DT :" << fdt);
            }

            //OGL_TRACE_DEBUG(logger, "X " << ms.X.abs << " Y " << ms.Y.abs);

            rotx = glm::degrees(-ms.X.rel * 0.00005f);
            roty = glm::degrees(-ms.Y.rel * 0.00005f);
        }
    }

    if (keyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        exit(0);
    }

    if (translateVec != vec3(0, 0, 0))
    {
        camera->move(translateVec);
    }

    if (rotx != 0 || roty != 0)
    {
        camera->rotate(rotx, roty, 0);
    }
    
#endif
}

void Scene::lock()
{
    m_locked = TRUE;
}

void Scene::unlock()
{
    m_locked = FALSE;
}

size_t Scene::getLightsInFrustumCount() const
{
    return m_lightVolumes.size();
}
