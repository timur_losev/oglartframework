#include "Precompiled.h"
#include "Material.h"

#include "RenderTechnique.h"
#include "RenderPass.h"

Material::Material(const std::string& n): name(n)
{
}

Material::~Material()
{

}

const std::string& Material::getName() const
{
    return name;
}

void Material::setName( const std::string& n)
{
    name = n;
}

RenderTechniquePtr Material::getTechnique(size_t index) const
{
    assert(index < m_renderTechniques.size());

    return m_renderTechniques[index];
}

void Material::addTechnique( RenderTechniquePtr tch, int at /*= -1*/ )
{
    if (at >= 0 && at < (int)m_renderTechniques.size())
    {
        m_renderTechniques.insert(m_renderTechniques.begin() + at, tch);
    }
    else
    {
        m_renderTechniques.push_back(tch);
    }
}

size_t Material::getTechniquesCount() const
{
    return m_renderTechniques.size();
}

void Material::commit()
{
    std::for_each(m_renderTechniques.begin(), m_renderTechniques.end(), [](RenderTechniquePtr tech)
    {
        const size_t count = tech->getPassesCount();
        for(size_t i = 0; i < count; ++i)
        {
            tech->getPass(i)->commit();
        }
    });
}
