#pragma once

struct UniformAccessors
{
    enum Enum
    {
        MVP = 0,
        ModelViewMatrix,
        NormalMatrix,
        ModelMatrix,
        RenderState,
        LightPositionViewSpace,
        EyePositionViewSpace,

        TOTAL
    };
};
