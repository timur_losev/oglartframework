#pragma once

class BoxSphereBounds;

class Frustum
{
private:
    vec4 frustum[6];
public:
    Frustum();

    void extractFrustum(const mat4& viewProjMatrix);

    bool inside(const vec3& point) const;

    bool inside(const BoxSphereBounds& bbs) const;
};