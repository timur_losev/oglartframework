/*
 * File:   Vertex.h
 * Author: void
 *
 * Created on April 6, 2013, 2:57 PM
 */

#ifndef VERTEX_H
#    define	VERTEX_H


class Vertex
{
public:
    
    glm::vec3 position;
    glm::vec2 texcoord0;
    glm::vec3 normal;
    glm::vec4 color;
#if USE_NORMAL_MAPPING
    glm::vec3 tangent;
    glm::vec3 binormal;
#endif
    //char      _temp_padding_force_32[12];
};

#endif	/* VERTEX_H */

