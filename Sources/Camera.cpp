#include "Precompiled.h"
#include "Camera.h"
#include "MathUtils.h"
#include "Frustum.h"

const float Camera::DEFAULT_FOVX = 90.0f;
const float Camera::DEFAULT_ZFAR = 1000.0f;
const float Camera::DEFAULT_ZNEAR = 0.1f;
const vec3 Camera::WORLD_XAXIS(1.0f, 0.0f, 0.0f);
const vec3 Camera::WORLD_YAXIS(0.0f, 1.0f, 0.0f);
const vec3 Camera::WORLD_ZAXIS(0.0f, 0.0f, 1.0f);

Camera::Camera()
{
    m_behavior = CAMERA_BEHAVIOR_FLIGHT;

    m_fovx = DEFAULT_FOVX;
    m_znear = DEFAULT_ZNEAR;
    m_zfar = DEFAULT_ZFAR;
    m_aspectRatio = 0.0f;

    m_accumPitchDegrees = 0.0f;


    m_eye = vec3(0.0f, 0.0f, 0.0f);
    m_xAxis = vec3(1.0f, 0.0f, 0.0f);
    m_yAxis = vec3(0.0f, 1.0f, 0.0f);
    m_zAxis = vec3(0.0f, 0.0f, 1.0f);
    m_viewDir = vec3(0.0f, 0.0f, -1.0f);

    m_acceleration = vec3(0.0f, 0.0f, 0.0f);
    m_currentVelocity = vec3(0.0f, 0.0f, 0.0f);
    m_velocity = vec3(0.0f, 0.0f, 0.0f);

    m_frustum.reset(new Frustum());
}

Camera::~Camera()
{
}

void Camera::lookAt(const vec3 &target)
{
    lookAt(m_eye, target, m_yAxis);
}

void Camera::lookAt(const vec3 &eye, const vec3 &target, const vec3 &up)
{
#if 1

    m_eye = eye;

    m_viewMatrix = glm::lookAt(eye, target, up);

    m_xAxis = vec3(m_viewMatrix[0][0], m_viewMatrix[1][0], m_viewMatrix[2][0]);
    m_yAxis = vec3(m_viewMatrix[0][1], m_viewMatrix[1][1], m_viewMatrix[2][1]);
    m_zAxis = vec3(m_viewMatrix[0][2], m_viewMatrix[1][2], m_viewMatrix[2][2]);

    m_viewDir = -m_zAxis;

    // Extract the pitch angle from the view matrix.
    m_accumPitchDegrees = glm::degrees(asinf(m_viewMatrix[1][2]));

    m_orientation = glm::quat_cast(m_viewMatrix);

    updateViewProjMatrix();

    extractFrustum();
#else

    m_eye = eye;
    m_zAxis = target - eye;
    m_zAxis = glm::normalize(m_zAxis);

    m_viewDir = -m_zAxis;

    vec3 u = glm::normalize(up);

    m_xAxis = glm::cross( u, m_zAxis );
    m_xAxis = glm::normalize(m_xAxis);

    m_yAxis = glm::cross( m_zAxis, m_xAxis );
    m_yAxis = glm::normalize(m_yAxis);

    m_viewMatrix[0][0] = m_xAxis.x;							m_viewMatrix[0][1] = m_yAxis.x;							m_viewMatrix[0][2] = m_zAxis.x;
    m_viewMatrix[1][0] = m_xAxis.y;							m_viewMatrix[1][1] = m_yAxis.y;							m_viewMatrix[1][2] = m_zAxis.y;
    m_viewMatrix[2][0] = m_xAxis.z;							m_viewMatrix[2][1] = m_yAxis.z;							m_viewMatrix[2][2] = m_zAxis.z;
    m_viewMatrix[3][0] = -glm::dot(m_xAxis, eye);		    m_viewMatrix[3][1] = -glm::dot(m_yAxis, eye);		    m_viewMatrix[3][2] = -glm::dot(m_zAxis, eye);

    // Extract the pitch angle from the view matrix.
    m_accumPitchDegrees = glm::degrees(asinf(m_viewMatrix[1][2]));

    m_orientation = glm::quat_cast(m_viewMatrix);
#endif
}

void Camera::updateViewMatrix()
{
    // Reconstruct the view matrix.

    m_viewMatrix = glm::mat4_cast(m_orientation);

    m_xAxis = vec3(m_viewMatrix[0][0], m_viewMatrix[1][0], m_viewMatrix[2][0]);
    m_yAxis = vec3(m_viewMatrix[0][1], m_viewMatrix[1][1], m_viewMatrix[2][1]);
    m_zAxis = vec3(m_viewMatrix[0][2], m_viewMatrix[1][2], m_viewMatrix[2][2]);
    m_viewDir = -m_zAxis;

    m_viewMatrix[3][0] = -glm::dot(m_xAxis, m_eye);
    m_viewMatrix[3][1] = -glm::dot(m_yAxis, m_eye);
    m_viewMatrix[3][2] = -glm::dot(m_zAxis, m_eye);
    m_viewMatrix[3][3] = 1.0f;

    updateViewProjMatrix();

    extractFrustum();
}

void Camera::rotateFirstPerson(float headingDegrees, float pitchDegrees)
{
    // Implements the rotation logic for the first person style and
    // spectator style camera behaviors. Roll is ignored.

   m_accumPitchDegrees += pitchDegrees;

    const float constraint = 360.f;

    if (m_accumPitchDegrees > constraint)
    {
        pitchDegrees = constraint - (m_accumPitchDegrees - pitchDegrees);
        m_accumPitchDegrees = 0;
    }

    if (m_accumPitchDegrees < -constraint)
    {
        pitchDegrees = -constraint - (m_accumPitchDegrees - pitchDegrees);
        m_accumPitchDegrees = 0;
    }

    quat rot;

    if (headingDegrees != 0.0f)
    {
        //glm::angleAxis(headingDegrees, WORLD_YAXIS);
        vec3 new_axis = WORLD_ZAXIS * m_orientation; 

        m_orientation = glm::rotate(m_orientation, headingDegrees, WORLD_ZAXIS); // rot * m_orientation;
    }

    if (pitchDegrees != 0.0f)
    {
        vec3 new_axis = WORLD_XAXIS * m_orientation; 

        //rot = glm::angleAxis(pitchDegrees, WORLD_XAXIS);
        //m_orientation = m_orientation * rot;
        // 
        m_orientation = glm::rotate(m_orientation, pitchDegrees, new_axis);
    }
}

void Camera::move(float dx, float dy, float dz)
{
    // Moves the camera by dx world units to the left or right; dy
    // world units upwards or downwards; and dz world units forwards
    // or backwards.

    vec3 eye = m_eye;
    vec3 forwards;

    if (m_behavior == CAMERA_BEHAVIOR_FIRST_PERSON)
    {
        // Calculate the forwards direction. Can't just use the camera's local
        // z axis as doing so will cause the camera to move more slowly as the
        // camera's view approaches 90 degrees straight up and down.

        forwards = glm::cross(m_yAxis, m_xAxis);
        forwards = glm::normalize(forwards);
    }
    else
    {
        forwards = m_viewDir;
    }

    eye += m_xAxis * dx;
    eye += m_yAxis * dy;
    eye += forwards * dz;

    setPosition(eye);
}

void Camera::move(const vec3 &direction, const vec3 &amount)
{
    // Moves the camera by the specified amount of world units in the specified
    // direction in world space.

    m_eye.x += direction.x * amount.x;
    m_eye.y += direction.y * amount.y;
    m_eye.z += direction.z * amount.z;

    updateViewMatrix();
}

void Camera::move( const vec3& translateVec )
{
    move(translateVec.x, translateVec.y, translateVec.z);
}

void Camera::perspective(float fovx, float aspect, float znear, float zfar)
{

#if 0
    // Construct a projection matrix based on the horizontal field of view
    // 'fovx' rather than the more traditional vertical field of view 'fovy'.

    float e = 1.0f / tanf(glm::radians(fovx) / 2.0f);
    float aspectInv = 1.0f / aspect;
    float fovy = 2.0f * atanf(aspectInv / e);
    float xScale = 1.0f / tanf(0.5f * fovy);
    float yScale = xScale / aspectInv;

    m_projMatrix[0][0] = xScale;
    m_projMatrix[0][1] = 0.0f;
    m_projMatrix[0][2] = 0.0f;
    m_projMatrix[0][3] = 0.0f;

    m_projMatrix[1][0] = 0.0f;
    m_projMatrix[1][1] = yScale;
    m_projMatrix[1][2] = 0.0f;
    m_projMatrix[1][3] = 0.0f;

    m_projMatrix[2][0] = 0.0f;
    m_projMatrix[2][1] = 0.0f;
    m_projMatrix[2][2] = (zfar + znear) / (znear - zfar);
    m_projMatrix[2][3] = -1.0f;

    m_projMatrix[3][0] = 0.0f;
    m_projMatrix[3][1] = 0.0f;
    m_projMatrix[3][2] = (2.0f * zfar * znear) / (znear - zfar);
    m_projMatrix[3][3] = 0.0f;

    m_fovx = fovx;
    m_aspectRatio = aspect;
    m_znear = znear;
    m_zfar = zfar;

#else

    m_projMatrix = glm::perspective(fovx, aspect, znear, zfar);

    m_fovx = fovx;
    m_aspectRatio = aspect;
    m_znear = znear;
    m_zfar = zfar;
#endif
}

void Camera::reshape(float aspectRatio)
{
    m_aspectRatio = aspectRatio;
    m_projMatrix = glm::perspective(m_fovx, aspectRatio, m_znear, m_zfar);

    updateViewProjMatrix();
    extractFrustum();
}

void Camera::rotate(float headingDegrees, float pitchDegrees, float rollDegrees)
{
    // Rotates the camera based on its current behavior.
    // Note that not all behaviors support rolling.

    pitchDegrees = -pitchDegrees;
    headingDegrees = -headingDegrees;
    rollDegrees = -rollDegrees;

    switch (m_behavior)
    {
    case CAMERA_BEHAVIOR_FIRST_PERSON:
        rotateFirstPerson(headingDegrees, pitchDegrees);
        break;

    case CAMERA_BEHAVIOR_FLIGHT:
        rotateFlight(headingDegrees, pitchDegrees, rollDegrees);
        break;
    }

    updateViewMatrix();
}

void Camera::rotateFlight(float headingDegrees, float pitchDegrees, float rollDegrees)
{
    // Implements the rotation logic for the flight style camera behavior.

    mat4 rot = glm::eulerAngleYXZ(headingDegrees, pitchDegrees, rollDegrees);

    m_orientation = m_orientation * glm::quat_cast(rot);
}


void Camera::setAcceleration(float x, float y, float z)
{
    m_acceleration = vec3(x, y, z);
}

void Camera::setAcceleration(const vec3 &acceleration)
{
    m_acceleration = acceleration;
}

void Camera::setBehavior(CameraBehavior newBehavior)
{
    if (m_behavior == CAMERA_BEHAVIOR_FLIGHT && newBehavior == CAMERA_BEHAVIOR_FIRST_PERSON)
    {
        // Moving from flight-simulator mode to first-person.
        // Need to ignore camera roll, but retain existing pitch and heading.

        lookAt(m_eye, m_eye + vec3(-m_zAxis.x, -m_zAxis.y, -m_zAxis.z), WORLD_YAXIS);
    }

    m_behavior = newBehavior;
}

void Camera::setCurrentVelocity(float x, float y, float z)
{
    m_currentVelocity = vec3(x, y, z);
}

void Camera::setCurrentVelocity(const vec3 &currentVelocity)
{
    m_currentVelocity = currentVelocity;
}

void Camera::setOrientation(const quat &orientation)
{
    mat4 m = glm::mat4_cast(orientation);

    m_accumPitchDegrees = glm::radians(asinf(m[1][2]));
    m_orientation = orientation;

    if (m_behavior == CAMERA_BEHAVIOR_FIRST_PERSON)
    {
        lookAt(m_eye, m_eye + m_viewDir, WORLD_YAXIS);
    }

    updateViewMatrix();
}

void Camera::setPosition(float x, float y, float z)
{
    m_eye = vec3(x, y, z);
    updateViewMatrix();
}

void Camera::setPosition(const vec3 &position)
{
    m_eye = position;
    updateViewMatrix();
}

void Camera::setVelocity(float x, float y, float z)
{
    m_velocity = vec3(x, y, z);
}

void Camera::setVelocity(const vec3 &velocity)
{
    m_velocity = velocity;
}

void Camera::updatePosition(const vec3 &direction, float elapsedTimeSec)
{
    // Moves the camera using Newton's second law of motion. Unit mass is
    // assumed here to somewhat simplify the calculations. The direction vector
    // is in the range [-1,1].

    if (glm::magnitudeSq(m_currentVelocity) != 0.0f)
    {
        // Only move the camera if the velocity vector is not of zero length.
        // Doing this guards against the camera slowly creeping around due to
        // floating point rounding errors.

        vec3 displacement = (m_currentVelocity * elapsedTimeSec) +
            (0.5f * m_acceleration * elapsedTimeSec * elapsedTimeSec);

        // Floating point rounding errors will slowly accumulate and cause the
        // camera to move along each axis. To prevent any unintended movement
        // the displacement vector is clamped to zero for each direction that
        // the camera isn't moving in. Note that the updateVelocity() method
        // will slowly decelerate the camera's velocity back to a stationary
        // state when the camera is no longer moving along that direction. To
        // account for this the camera's current velocity is also checked.

        

        if (direction.x == 0.0f && glm::closeEnough(m_currentVelocity.x, 0.0f))
            displacement.x = 0.0f;

        if (direction.y == 0.0f && glm::closeEnough(m_currentVelocity.y, 0.0f))
            displacement.y = 0.0f;

        if (direction.z == 0.0f && glm::closeEnough(m_currentVelocity.z, 0.0f))
            displacement.z = 0.0f;

        move(displacement.x, displacement.y, displacement.z);
    }

    // Continuously update the camera's velocity vector even if the camera
    // hasn't moved during this call. When the camera is no longer being moved
    // the camera is decelerating back to its stationary state.

    updateVelocity(direction, elapsedTimeSec);
}

void Camera::updateVelocity(const vec3 &direction, float elapsedTimeSec)
{
    // Updates the camera's velocity based on the supplied movement direction
    // and the elapsed time (since this method was last called). The movement
    // direction is in the range [-1,1].

    if (direction.x != 0.0f)
    {
        // Camera is moving along the x axis.
        // Linearly accelerate up to the camera's max speed.

        m_currentVelocity.x += direction.x * m_acceleration.x * elapsedTimeSec;

        if (m_currentVelocity.x > m_velocity.x)
        {
            m_currentVelocity.x = m_velocity.x;
        }
        else if (m_currentVelocity.x < -m_velocity.x)
        {
            m_currentVelocity.x = -m_velocity.x;
        }
    }
    else
    {
        // Camera is no longer moving along the x axis.
        // Linearly decelerate back to stationary state.

        if (m_currentVelocity.x > 0.0f)
        {
            if ((m_currentVelocity.x -= m_acceleration.x * elapsedTimeSec) < 0.0f)
            {
                m_currentVelocity.x = 0.0f;
            }
        }
        else
        {
            if ((m_currentVelocity.x += m_acceleration.x * elapsedTimeSec) > 0.0f)
            {
                m_currentVelocity.x = 0.0f;
            }
        }
    }

    if (direction.y != 0.0f)
    {
        // Camera is moving along the y axis.
        // Linearly accelerate up to the camera's max speed.

        m_currentVelocity.y += direction.y * m_acceleration.y * elapsedTimeSec;

        if (m_currentVelocity.y > m_velocity.y)
        {
            m_currentVelocity.y = m_velocity.y;
        }
        else if (m_currentVelocity.y < -m_velocity.y)
        {
            m_currentVelocity.y = -m_velocity.y;
        }
    }
    else
    {
        // Camera is no longer moving along the y axis.
        // Linearly decelerate back to stationary state.

        if (m_currentVelocity.y > 0.0f)
        {
            if ((m_currentVelocity.y -= m_acceleration.y * elapsedTimeSec) < 0.0f)
            {
                m_currentVelocity.y = 0.0f;
            }
        }
        else
        {
            if ((m_currentVelocity.y += m_acceleration.y * elapsedTimeSec) > 0.0f)
            {
                m_currentVelocity.y = 0.0f;
            }
        }
    }

    if (direction.z != 0.0f)
    {
        // Camera is moving along the z axis.
        // Linearly accelerate up to the camera's max speed.

        m_currentVelocity.z += direction.z * m_acceleration.z * elapsedTimeSec;

        if (m_currentVelocity.z > m_velocity.z)
        {
            m_currentVelocity.z = m_velocity.z;
        }
        else if (m_currentVelocity.z < -m_velocity.z)
        {
            m_currentVelocity.z = -m_velocity.z;
        }
    }
    else
    {
        // Camera is no longer moving along the z axis.
        // Linearly decelerate back to stationary state.

        if (m_currentVelocity.z > 0.0f)
        {
            if ((m_currentVelocity.z -= m_acceleration.z * elapsedTimeSec) < 0.0f)
            {
                m_currentVelocity.z = 0.0f;
            }
        }
        else
        {
            if ((m_currentVelocity.z += m_acceleration.z * elapsedTimeSec) > 0.0f)
            {
                m_currentVelocity.z = 0.0f;
            }
        }
    }
}

const mat4& Camera::getViewProjMatrix() const
{
    return m_viewProjMatrix;
}

void Camera::extractFrustum()
{
    m_frustum->extractFrustum(m_viewProjMatrix);
}

void Camera::updateViewProjMatrix()
{
    m_viewProjMatrix = m_projMatrix * m_viewMatrix;
}

FrustumPtr Camera::getFrustum() const
{
    return m_frustum;
}
