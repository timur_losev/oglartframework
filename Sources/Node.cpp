#include "Precompiled.h"
#include "Node.h"
#include "GraphicMesh.h"

#include "Light.h"

Node::Node(const std::string& n):
m_name(n)
{
    m_nodeType = ENT_NONE;
}

Node::~Node()
{}

bool Node::hasChild(NodePtr n) const
{
    for(auto i = children.cbegin(), e = children.cend(); i != e; ++i)
    {
        if (*i == n) return true;
    }
    return false;
}

bool Node::hasChild(const std::string& name) const
{
    return getChild(name) != nullptr;
}

void Node::addChild(NodePtr n)
{
    assert(n);
    assert(!n->m_name.empty());
    assert(!hasChild(n));
    assert(!hasChild(n->m_name));

    n->parent = shared_from_this();
    children.push_back(n);
}

NodeConstPtr Node::getChild(const std::string& name) const
{
    for(auto i = children.cbegin(), e = children.cend(); i != e; ++i)
    {
        if ((*i)->m_name == name) return (*i);
    }

    return nullptr;
}

NodePtr Node::getChild(const std::string& name)
{
    for(auto i = children.cbegin(), e = children.cend(); i != e; ++i)
    {
        if ((*i)->m_name == name) return (*i);
    }

    return nullptr;
}

GraphicMeshConstPtr Node::getMesh(std::string name) const
{
    for(auto i = meshes.cbegin(), e = meshes.cend(); i != e; ++i)
    {
        if ((*i)->name == name) return *i;
    }

    return nullptr;
}

void Node::addMesh(GraphicMeshPtr m)
{
    assert(m);
    assert(!m->name.empty());
    assert(!getMesh(m->name));

    meshes.push_back(m);
}

void Node::updateTransform(const glm::mat4& vp)
{
    m_transformation *= vp;

    for(auto i = children.cbegin(), e = children.cend(); i != e; ++i)
    {
        (*i)->updateTransform(m_transformation);
    }
}

void Node::setTransform( const glm::mat4& t )
{
    m_transformation = t;
    m_position = m_transformation[3].xyz;
}

void Node::setPosition( const vec3& pos )
{
    m_position = pos;
    m_transformation[3] = vec4(pos.xyz, 1.f);
}

const vec3& Node::getPosition() const
{
    return m_position;
}

void Node::translate( const vec3& pos /*TODO: relation*/ )
{
    glm::translate(m_transformation, pos);
    m_position = m_transformation[3].xyz;
}

Node::ENodeType Node::getNodeType() const
{
    return m_nodeType;
}

LightPtr Node::asLightNode()
{
    assert(m_nodeType == ENT_LIGHT);

    return std::static_pointer_cast<Light>(this->shared_from_this());
}

LightConstPtr Node::asLightNode() const
{
    assert(m_nodeType == ENT_LIGHT);

    return std::static_pointer_cast<Light const>(this->shared_from_this());
}
