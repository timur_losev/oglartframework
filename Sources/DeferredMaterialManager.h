#pragma once

forward_this(Material);
forward_this(RenderPass);
forward_this(ShaderProgram);
forward_this(Light);

class DeferredMaterialManager
{
private:
    void          create();
private:

    RenderPassPtr m_geometryPass,
                  m_pointLightPass, 
                  m_directionalLightPass,
                  m_nullPass;

    MaterialPtr   m_lightVolumeMaterial,
                  m_gbufferMaterial;

    ShaderProgramPtr m_gbufferProgram,
                     m_nullPassProgram,
                     m_pointLightProgram,
                     m_geomProgramFw,
                     m_objectOverlayProgram,
                     m_spotLightProgram;
public:
    RenderPassPtr createGeometryPass() const;
    RenderPassPtr createGeometryForwardPass() const;
    RenderPassPtr createPointLightPass() const;
    RenderPassPtr createDirectionalLightPass() const;
    RenderPassPtr createNullPass() const;
    RenderPassPtr createObjectOverlayPass() const;
    RenderPassPtr createSpotLightPass() const;

    MaterialPtr   createPointLightVolumeMaterial(const std::string& name) const;
    MaterialPtr   createSpotLightVolumeMaterial(const std::string& name) const;
    MaterialPtr   createGBufferMaterial(const std::string& name) const;
    MaterialPtr   createForwardMaterial(const std::string& name) const;
    MaterialPtr   createScreenQuadMaterial(const std::string& name) const;
    MaterialPtr   createObjectOverlayMaterial(const std::string& name) const;

public:

    DeferredMaterialManager();
    ~DeferredMaterialManager();

    void         applyLightMaterial(LightPtr light) const;
};