#pragma once

uint64_t AppApiGetTime();
uint64_t AppApiGetTickCount();
uint32_t AppApiGetTimeMS();