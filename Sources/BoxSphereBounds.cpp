#include "Precompiled.h"
#include "BoxSphereBounds.h"

#include "Box.h"
#include "Sphere.h"

#include "Vertex.h"

#include "MathUtils.h"

#include "Line3D.h"
#include "Circle3D.h"

BoxSphereBounds::BoxSphereBounds(): radius(0.f)
{

}

BoxSphereBounds::BoxSphereBounds( const vec3& inOrigin, const vec3& inBoxExtent, float inRadius ):
    origin(inOrigin),
    boxExtent(inBoxExtent),
    radius(inRadius)
{

}

BoxSphereBounds::BoxSphereBounds( const Box& inBox, const Sphere& inSphere )
{
    origin = inBox.getCenter();
    boxExtent = inBox.getExtent();

    radius = glm::length(boxExtent);
}

BoxSphereBounds::BoxSphereBounds( const Vertex* points, size_t count)
{
    buildFromPoints(points, count);
}

void BoxSphereBounds::buildFromPoints( const Vertex* points, size_t count)
{
    Box boundingBox;

    for (size_t i = 0; i < count; ++i)
    {
        boundingBox += points[i].position;
    }

    origin = boundingBox.getCenter();
    boxExtent = boundingBox.getExtent();

    radius = 0.f;
    // Using the center of the bounding box as the origin of the sphere, find the radius of the bounding sphere.

    for (size_t i = 0; i < count; ++i)
    {
        radius = glm::max(radius, glm::length((points[i].position - origin)));
    }

    m_radiusVisualization.reset(new Line3D(origin, vec3(radius, origin.y, origin.z), Color_t(1, 0, 0, 1)));
    m_circleVisualization.reset(new Circle3D(origin, radius, 100, Color_t(1, 1, 1, 1)));
}

BoxSphereBounds BoxSphereBounds::transform( const mat4& m )
{
    BoxSphereBounds ret;
    //if (m_prev != m)
    {
        m_prev = m;

        ret.origin = glm::transformVector3(origin, m);

        float signs[2] = { -1.f, 1.f };

        for (uint32_t x = 0; x < 2; ++x)
        {
            for(uint32_t y = 0; y < 2; ++y)
            {
                for(uint32_t z = 0; z < 2; ++z)
                {
                    vec3 corner = glm::transformNormal(
                        vec3(signs[x] * boxExtent.x,
                             signs[y] * boxExtent.y,
                             signs[z] * boxExtent.z), m);

                    ret.boxExtent.x = glm::max(corner.x, boxExtent.x);
                    ret.boxExtent.y = glm::max(corner.y, boxExtent.y);
                    ret.boxExtent.z = glm::max(corner.z, boxExtent.z);
                }
            }
        }

        vec3 xAxis = vec3(m[0][0], m[1][0], m[2][0]);
        vec3 yAxis = vec3(m[0][1], m[1][1], m[2][1]);
        vec3 zAxis = vec3(m[0][2], m[1][2], m[2][2]);

        ret.radius = glm::sqrt(
            glm::max(glm::dot(xAxis, xAxis),
                     glm::max(glm::dot(yAxis, yAxis),
                              glm::dot(zAxis, zAxis)))) * radius;
    }

    return ret;
}

Line3DPtr BoxSphereBounds::getRadiusVisualization()
{
    return m_radiusVisualization;
}

Circle3DPtr BoxSphereBounds::getCircleVisualization()
{
    return m_circleVisualization;
}
