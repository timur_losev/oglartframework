#pragma once

#include "PointLightPass.h"

forward_this(Light);

class SpotLightPass : public PointLightPass
{
protected:
    virtual void commitImpl();

    struct SpotLight
    {
        uint32_t outerCone;
        uint32_t innerCone;
        uint32_t directional;

        SpotLight():
            outerCone(INVALID_ID),
            innerCone(INVALID_ID),
            directional(INVALID_ID)
        {}
    } m_spotLightLoc;

public:
    SpotLightPass();
    ~SpotLightPass();

    virtual void use(LightConstPtr light);
};