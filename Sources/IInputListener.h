#pragma once

namespace OIS {
    class KeyEvent;
    class MouseEvent;
    class Mouse;
    class Keyboard;
}

class IInputListener
{
public:
    virtual bool keyPressed(const OIS::KeyEvent& evt) {return false;}
    virtual bool keyReleased(const OIS::KeyEvent& evt) {return false;}

    virtual bool mousePressed(const OIS::MouseEvent& evt, int id) {return false;}
    virtual bool mouseReleased(const OIS::MouseEvent& evt, int id) {return false;}
    virtual bool mouseMoved(const OIS::MouseEvent& evt) {return false;}

    virtual void nonBufferedUpdateInput(OIS::Keyboard*, OIS::Mouse*, uint32_t dt) {}
};