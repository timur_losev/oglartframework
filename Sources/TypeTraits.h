#pragma once

template<typename T, bool>
class RemoveRefIf;

template<typename T>
struct RemoveRefIf<T, false>
{
    typedef T Result;
};

template<typename T>
struct RemoveRefIf<T, true>
{
    typedef typename std::remove_reference<T>::type Result;
};

template<typename T>
struct RemoveRefIfFundamental
{
    typedef typename RemoveRefIf<T, std::is_fundamental<typename std::remove_reference<T>::type>::value>::Result Result;
};
