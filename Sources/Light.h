#pragma once

#include "LightTypes.h"
#include "Adaptation.h"

#include "Node.h"

forward_this(Light);
forward_this(VolumeMesh);

class Light : public Node
{
public:
    LightTypes::Enum    type;
    vec3                direction;
    vec3                directionViewSpace;

    /** Constant light attenuation factor. 
    *
    *  The intensity of the light source at a given distance 'd' from
    *  the light's position is
    *  @code
    *  Atten = 1/( att0 + att1 * d + att2 * d*d)
    *  @endcode
    *  This member corresponds to the att0 variable in the equation.
    *  Naturally undefined for directional lights.
    */
    float               attenuationConstant;

    /** Linear light attenuation factor. 
    *
    *  The intensity of the light source at a given distance 'd' from
    *  the light's position is
    *  @code
    *  Atten = 1/( att0 + att1 * d + att2 * d*d)
    *  @endcode
    *  This member corresponds to the att1 variable in the equation.
    *  Naturally undefined for directional lights.
    */
    float               attenuationLinear;

    /** Quadratic light attenuation factor. 
    *  
    *  The intensity of the light source at a given distance 'd' from
    *  the light's position is
    *  @code
    *  Atten = 1/( att0 + att1 * d + att2 * d*d)
    *  @endcode
    *  This member corresponds to the att2 variable in the equation.
    *  Naturally undefined for directional lights.
    */
    float               attenuationQuadratic;

    LightFalloffTypes::Enum falloffType;

    Color_t             colorDiffuse;
    Color_t             colorAmbient;
    Color_t             colorSpecular;

    float               angleInnerCone;
    float               angleOuterCone;

    float               distance;

    VolumeMeshPtr       lightGeometry;

    Light(const std::string& name);

    ~Light();
};