/*
 * File:   Scene.h
 * Author: void
 *
 * Created on April 3, 2013, 11:28 PM
 */

#ifndef SCENE_H
#    define	SCENE_H

#include "IInputListener.h"

#include <boost/variant.hpp>

namespace Collada
{
    forward_this(ColladaParser);
    forward_this_s(Node);
}

forward_this(Scene);
forward_this(Node);
forward_this(MeshManager);
forward_this(LightManager);
forward_this(Camera);
forward_this(GraphicMesh);
forward_this(Frustum);
forward_this(Line3D);
forward_this(Circle3D);
forward_this(Light);

class Renderer;

class Scene : public IInputListener, public std::enable_shared_from_this<Scene>
{
private:
    Collada::ColladaParserPtr   colladaParser;

    NodePtr                     root;

    NodePtr                     buildHierarchy(Collada::NodeConstPtr);

    std::string                 findNameForNode(Collada::NodeConstPtr) const;
    void                        resolveNodeInstances(Collada::NodeConstPtr, std::vector<Collada::NodeConstPtr>& resolved);
    Collada::NodeConstPtr       findNode(Collada::NodeConstPtr, const std::string& name) const;

    bool                        commitStaticRecursive(NodePtr);

    void                        applyFrustumRecursive(NodePtr, FrustumPtr);
    void                        applyFrustum(NodePtr, FrustumPtr);
    void                        applyFrustum(LightPtr, FrustumPtr);

    void                        correctAxis();

    MeshManagerPtr              meshManager;
    LightManagerPtr             lightManager;

    CameraPtr                   camera;

    typedef std::pair<GraphicMeshPtr, std::tuple<mat4, mat4>>     MeshTransformPair_t;
    typedef std::pair<Line3DPtr, mat4>          Line3DTransformPair_t;
    typedef std::pair<Circle3DPtr, mat4>        Circle3DTransformPair_t;
    typedef std::pair<LightPtr, std::tuple<mat4, mat4>>           LightTransformPair_t; // Light, mv matrix, mvp matrix

    typedef boost::variant<
        MeshTransformPair_t
        ,Line3DTransformPair_t
        ,Circle3DTransformPair_t
        ,LightTransformPair_t
    > Renderable_t;

    typedef std::list<Renderable_t> Renderables_t;
    Renderables_t m_renderables;
    Renderables_t m_lightVolumes;

    Bool_t        m_locked;
    Bool_t        m_renderDebug;
public:
    Scene();

    ~Scene();

    bool load(const std::string& filename);

    MeshManagerPtr              getMeshManager() const { return meshManager; }

    void                        update(uint32_t dt);

    void                        lock();
    void                        unlock();

    void                        render(Renderer& renderer);
    void                        renderLightVolume(Renderer& renderer, uint32_t i);

    virtual void                nonBufferedUpdateInput( OIS::Keyboard*, OIS::Mouse* , uint32_t dt);

    CameraPtr                   getCamera() const { return camera; }

    size_t                      getLightsInFrustumCount() const;
} ;

#endif	/* SCENE_H */

