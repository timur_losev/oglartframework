/*
 * File:   MeshManager.h
 * Author: void
 *
 * Created on April 6, 2013, 6:29 PM
 */

#ifndef MESHMANAGER_H
#    define	MESHMANAGER_H

forward_this(GraphicMesh);
forward_this(Node);

namespace Collada
{
    forward_this(ColladaParser);
    forward_this_s(Mesh);
    forward_this_s(SubMesh);
    forward_this_s(Controller);
    forward_this_s(Node);
};

forward_this(MaterialManager);
forward_this(Material);

class MeshManager
{
    class ColladaMeshIndex
    {
    public:
        std::string     meshID;
        size_t          subMesh;
        std::string     material;

        ColladaMeshIndex(const std::string& id, size_t subMesh, const std::string& mat):
        meshID(id), subMesh(subMesh), material(mat)
        {}

        bool operator < (const ColladaMeshIndex& p) const
        {
            if (meshID == p.meshID)
            {
                if (subMesh == p.subMesh)
                    return material < p.material;
                else
                    return subMesh < p.subMesh;
            }
            else
            {
                return meshID < p.meshID;
            }
        }
    };

public:
    typedef std::vector<GraphicMeshPtr> Meshes_t;

private:
    typedef std::unordered_map<std::string, size_t> MaterialIndexByName_t;
    typedef std::map<ColladaMeshIndex, size_t> MeshIndexByID_t;

    MeshIndexByID_t       meshIndexByID;
    Meshes_t              meshes;

    MaterialManagerPtr    materialManager;

public:

    MeshManager();

    GraphicMeshPtr createMesh(Collada::ColladaParserConstPtr parser, Collada::MeshConstPtr srcMesh,
                       const Collada::SubMesh& subMesh, Collada::ControllerConstPtr srcController,
                       size_t startVertex, size_t startFace);

    void    buildMeshesForNode(Collada::ColladaParserConstPtr parser, Collada::NodeConstPtr, NodePtr target);

    void    dumpMeshes();

    const Meshes_t&     getMeshes() const { return meshes; }

    MaterialManager&    getMaterialManager() const;
};

#endif	/* MESHMANAGER_H */

