#pragma once

#include "RenderPass.h"

forward_this(ScreenQuadRenderPass);

class ScreenQuadRenderPass: public RenderPass
{
private:
    vec4 m_testLightPos;
protected:
    virtual void commitImpl();
public:
    ScreenQuadRenderPass();
    ~ScreenQuadRenderPass();

    virtual void use();

    void setTestLightPos(const vec4& p);
};