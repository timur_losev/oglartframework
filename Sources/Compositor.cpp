#include "Precompiled.h"
#include "Compositor.h"

#include "FrameBuffer.h"
#include "ScreenQuad.h"
#include "GeometryBuffer.h"

#include "ScreenQuadRenderPass.h"
#include "LightPass.h"

#include "Texture.h"

#include "Light.h"

Compositor::Compositor( const ivec2& dimenstion )
{
    m_geometryBuffer.reset(new GeometryBuffer());

    assert(m_geometryBuffer->create(dimenstion) && "An error occurred while creating the Geometry Frame buffer");
}

Compositor::~Compositor()
{

}

GeometryBufferPtr Compositor::getGeometryBuffer() const
{
    return m_geometryBuffer;
}
