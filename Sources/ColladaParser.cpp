#include "Precompiled.h"
#include "ColladaParser.h"

#include "Global.h"
#include "LightTypes.h"

#include "ZBase32.h"

#include "Logger.h"

#define LOG_ASSERT(cond, mess)\
    OGL_TRACE_ASSERT(m_logger, cond, mess);\
    assert(cond)

#define LOG_WRONG_FILE_FORMAT_IF(cond)\
        LOG_ASSERT(cond == false, "Wrond file format " << fileName)

static const uint32_t BDaeBufferSize = 1024 * 1024;

namespace Collada
{

    ColladaParser::ColladaParser(const std::string& filename) :
    fileName(filename)
    {
        unitSize = 1.f;
        upDirection = UP_Z;

        format = FV_1_5_n;

        m_logger = GetLoggerPtr();
        
        ReadContents();
    }

    void ColladaParser::ReadAssetInfo(tinyxml2::XMLElement* elem)
    {
        for ( tinyxml2::XMLElement* assetElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());
                assetElem; assetElem = assetElem->NextSiblingElement())
        {
            std::string elemName = assetElem->Value();

            if (elemName == "unit")
            {
                const char* meter = assetElem->Attribute("meter");
                if (meter)
                {
                    unitSize = boost::lexical_cast<float>(meter);
                }
                else
                {
                    unitSize = 1.f;
                }

                OGL_TRACE_DEBUG(m_logger, "Choose unit size " << unitSize);
            }
            else if (elemName == "up_axis")
            {
                std::string up = assetElem->GetText();

                if (up == "X_UP")
                    upDirection = UP_X;
                else if (up == "Y_UP")
                    upDirection = UP_Y;
                else if (up == "Z_UP")
                    upDirection = UP_Z;

                OGL_TRACE_DEBUG(m_logger, "Choose up direction " << up);
            }
        }
    }

    void ColladaParser::ReadDataArray(tinyxml2::XMLElement* elem)
    {
        std::string elemName = elem->Value();
        bool isStringArray = elemName == "IDREF_array" || elemName == "Name_array";

        const char* chID = elem->Attribute("id");

        LOG_WRONG_FILE_FORMAT_IF(!chID);

        std::string ID = chID;

        const char* chCount = elem->Attribute("count");

        LOG_WRONG_FILE_FORMAT_IF(!chCount);

        uint32_t count = boost::lexical_cast<uint32_t>(chCount);

        if (count == 0)
        {
            OGL_TRACE_DEBUG(m_logger, "Data array count == 0.");
            // some exporters write empty data arrays with count="0"
            return;
        }

        tinyxml2::XMLText* text = elem->FirstChild()->ToText();
        const char* content = elem->GetText();

        dataLibrary[ID] = DataPtr(new Data());
        Data& data = *dataLibrary[ID];
        data.isStringArray = isStringArray;

        if (content)
        {
            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Read data array. ElemName [%s]. Count [%d]. Data [%s]. ID [%s]") % elemName % count % content % ID));

            if (isStringArray)
            {
                data.strings.reserve(count);

                boost::split(data.strings, content, boost::is_any_of(" "), boost::token_compress_on);
            }
            else
            {
#ifdef AS_BDAE
                std::vector<std::string> v;
                boost::split(v, content, boost::is_any_of(" "), boost::token_compress_on);

                data.values = new float[v.size()];

                for (uint32_t i = 0; i < v.size(); ++i)
                {
                    float val = boost::lexical_cast<float>(v[i]);
                    data.values[i] = val;
                }

                unsigned char* outenc = new unsigned char[BDaeBufferSize];
                const unsigned char* inenc = (const unsigned char*)(&data.values[0]);

                int encodedlen = zbase32_encode(outenc, inenc, v.size() * sizeof(float) * 8);
                outenc[encodedlen] = 0;

                std::ostringstream ss;
                ss << encodedlen << " ";
                ss << outenc;

                text->SetValue(ss.str().c_str());

                delete[] outenc;
#else
                if (binaryReader)
                {
                    std::string encodedLenStr;
                    while (*content != ' ')
                    {
                        encodedLenStr += *content;
                        content++;
                    }

                    content++;

                    uint32_t encodedLen = atoi(encodedLenStr.c_str());

                    unsigned char* decoded = new unsigned char[BDaeBufferSize];
                    int decodedLen = zbase32_decode(decoded, (const unsigned char*)content, encodedLen * 8);

                    data.values = new float[count];
                    memcpy(data.values, decoded, count * sizeof(float));

                    delete[] decoded;
                }
                else
                {
                    assert("Only bdae at the moment" && false);
                }
#endif

            }
        }
    }

    void ColladaParser::ReadAccessor(const std::string& pID, tinyxml2::XMLElement* e)
    {
        const char* chSource = e->Attribute("source");

        LOG_WRONG_FILE_FORMAT_IF(!chSource);

        LOG_ASSERT(chSource[0] == '#', "Accessor source must be prefixed by #.");

        uint32_t count = boost::lexical_cast<int32_t>(e->Attribute("count"));

        const char* chStride = e->Attribute("stride");

        uint32_t stride = 1;
        if (chStride)
        {
            stride = boost::lexical_cast<uint32_t>(chStride);
        }

        const char* chOffset = e->Attribute("offset");
        uint32_t offset = 0;
        if (chOffset)
        {
            offset = boost::lexical_cast<uint32_t>(chOffset);
        }

        accessorLibrary[pID] = AccessorPtr(new Accessor());
        OGL_TRACE_DEBUG(m_logger, "Registered accessor #" + pID);
        Accessor& acc = *accessorLibrary[pID];
        acc.count = count;
        acc.offset = offset;
        acc.stride = stride;
        acc.source = chSource + 1;
        acc.size = 0;

        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "param")
            {
                const char* chName = elem->Attribute("name");
                std::string name;
                if (chName)
                {
                    name = chName;

                    // analyse for common type components and store it's sub-offset in the corresponding field

                    /* Cartesian coordinates */
                    if (name == "X") acc.subOffset[0] = acc.params.size();
                    else if (name == "Y") acc.subOffset[1] = acc.params.size();
                    else if (name == "Z") acc.subOffset[2] = acc.params.size();

                        // RGBA colors
                    else if (name == "R") acc.subOffset[0] = acc.params.size();
                    else if (name == "G") acc.subOffset[1] = acc.params.size();
                    else if (name == "B") acc.subOffset[2] = acc.params.size();
                    else if (name == "A") acc.subOffset[3] = acc.params.size();

                        /* UVWQ (STPQ) texture coordinates */
                    else if (name == "S") acc.subOffset[0] = acc.params.size();
                    else if (name == "T") acc.subOffset[1] = acc.params.size();
                    else if (name == "P") acc.subOffset[2] = acc.params.size();

                        /* Generic extra data, interpreted as UV data, too*/
                    else if (name == "U") acc.subOffset[0] = acc.params.size();
                    else if (name == "V") acc.subOffset[1] = acc.params.size();
                }

                const char* chType = elem->Attribute("type");
                if (chType)
                {
                    std::string type = chType;

                    if (type == "float4x4")
                        acc.size += 16;
                    else
                        acc.size += 1;
                }

                OGL_TRACE_DEBUG(m_logger, "Pushed name " << name);
                acc.params.push_back(name);
            }

        }

        LOG_WRONG_FILE_FORMAT_IF((!chStride && !chOffset));

    }

    void ColladaParser::ReadSource(tinyxml2::XMLElement* e)
    {
        assert(e);

        std::string sourceID;
        const char* sid = e->Attribute("id");

        assert(sid);

        sourceID = sid;

        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "float_array" || value == "IDREF_array" || value == "Name_array")
            {
                ReadDataArray(elem);
            }
            else if (value == "technique_common")
            {
                tinyxml2::XMLElement* accElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());

                LOG_ASSERT(accElem, "Accessor node must be here!");

                value = accElem->Value();

                if (value == "accessor")
                {
                    ReadAccessor(sourceID, accElem);
                }
                else
                {
                    LOG_ASSERT(false, "Accessor node must be here!");
                }
            }
        }
    }

    void ColladaParser::ReadAnimationSampler(Collada::AnimationChannel& pChannel, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "input")
            {
                const char* chSemantic = elem->Attribute("semantic");
                LOG_WRONG_FILE_FORMAT_IF(!chSemantic);

                const char* chSource = elem->Attribute("source");
                LOG_WRONG_FILE_FORMAT_IF(!chSource);
                LOG_WRONG_FILE_FORMAT_IF(chSource[0] != '#');
                chSource++;

                if (!strcmp(chSemantic, "INPUT"))
                {
                    pChannel.sourceTimes = chSource;
                }
                else if (!strcmp(chSemantic, "OUTPUT"))
                {
                    pChannel.sourceValues = chSource;
                }
            }
        }
    }

    void ColladaParser::ReadAnimation(AnimationPtr& pParent, tinyxml2::XMLElement* elem)
    {
        // an <animation> element may be a container for grouping sub-elements or an animation channel
        // this is the channel collection by ID, in case it has channels

        typedef std::unordered_map<std::string, AnimationChannel> ChannelMap_t;

        ChannelMap_t channels;
        // this is the anim container in case we're a container
        AnimationPtr anim;

        // optional name given as an attribute
        std::string animName;
        const char* chName = elem->Attribute("name");

        if (!chName)
        {
            chName = elem->Attribute("id");
        }

        if (!chName)
        {
            animName = "animation";
        }
        else
        {
            animName = chName;
        }


        for (tinyxml2::XMLElement* animElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());
                animElem; animElem = animElem->NextSiblingElement())
        {
            std::string nodeSemantic = animElem->Value();

            if (nodeSemantic == "animation")
            {
                if (!anim)
                {
                    anim.reset(new Animation());
                    anim->name = animName;
                    pParent->subAnims.push_back(anim);
                }

                ReadAnimation(anim, animElem);
            }
            else if (nodeSemantic == "source")
            {
                ReadSource(animElem);
            }
            else if (nodeSemantic == "sampler")
            {
                std::string ID = animElem->Attribute("id");
                ChannelMap_t::iterator newChannel = channels.insert(std::make_pair(ID, AnimationChannel() )).first;

                ReadAnimationSampler(newChannel->second, animElem);
            }
            else if (nodeSemantic == "channel")
            {
                // the binding element whose whole purpose is to provide the target to animate
                // Thanks, Collada! A directly posted information would have been too simple, I guess.
                // Better add another indirection to that! Can't have enough of those.

                const char* chTarget = animElem->Attribute("target");
                const char* chSource = animElem->Attribute("source");

                LOG_WRONG_FILE_FORMAT_IF(!chSource);

                if (chSource[0] == '#')
                    chSource ++;

                ChannelMap_t::iterator it = channels.find(chSource);
                if (it != channels.end())
                {
                    LOG_WRONG_FILE_FORMAT_IF(!chTarget);
                    it->second.target = chTarget;
                }
            }
            else
            {
                OGL_TRACE_DEBUG(m_logger, "Weird semantic detected. Scene file may be corrupted.");
            }

            OGL_TRACE_DEBUG(m_logger, "Read animation semantic " << nodeSemantic);
        }

    }

    Collada::EInputType ColladaParser::GetTypeForSemantic(const std::string& pSemantic) const
    {
        if ( pSemantic == "POSITION")
            return Collada::IT_Position;
        else if ( pSemantic == "TEXCOORD")
            return Collada::IT_Texcoord;
        else if ( pSemantic == "NORMAL")
            return Collada::IT_Normal;
        else if ( pSemantic == "COLOR")
            return Collada::IT_Color;
        else if ( pSemantic == "VERTEX")
            return Collada::IT_Vertex;
        else if ( pSemantic == "BINORMAL" || pSemantic ==  "TEXBINORMAL")
            return Collada::IT_Bitangent;
        else if ( pSemantic == "TANGENT" || pSemantic == "TEXTANGENT")
            return Collada::IT_Tangent;

        OGL_TRACE_WARN(m_logger, boost::str( boost::format( "Unknown vertex input type \"%s\". Ignoring.") % pSemantic));

        return IT_Invalid;
    }

    void ColladaParser::ReadAnimationLibrary(tinyxml2::XMLElement* elem)
    {
        for (tinyxml2::XMLElement* animElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());
                animElem; animElem = animElem->NextSiblingElement())
        {
            std::string value = animElem->Value();
            if (value == "animation")
            {
                anims.reset(new Animation());

                ReadAnimation(anims, animElem);
            }
        }
    }

    void ColladaParser::ReadInputChannel(std::vector<Collada::InputChannel>& poChannels, tinyxml2::XMLElement* e)
    {
        InputChannel input;

        const char* chSemantic = e->Attribute("semantic");
        if (chSemantic)
            input.type = GetTypeForSemantic(chSemantic);

        const char* chSource = e->Attribute("source");
        if (chSource)
            input.accessor = chSource + 1;

        const char* chOffset = e->Attribute("offset");
        if (chOffset)
            input.offset = boost::lexical_cast<size_t>(chOffset);

        if (input.type == IT_Texcoord || input.type == IT_Color)
        {
            const char* chSet = e->Attribute("set");
            if (chSet)
            {
                input.index = boost::lexical_cast<size_t>(chSet);
            }
        }

        OGL_TRACE_DEBUG(m_logger, "Read channel " << IT_ToString()(input.type));

        if (input.type != IT_Invalid)
        {
            poChannels.push_back(input);
        }
    }

    void ColladaParser::ReadVertexData(Collada::MeshPtr& pMesh, tinyxml2::XMLElement* e)
    {
        const char* chID = e->Attribute("id");
        LOG_WRONG_FILE_FORMAT_IF(!chID);

        pMesh->vertexID = chID;

        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "input")
            {
                ReadInputChannel(pMesh->perVertexData, elem);
            }
        }
    }

    void ColladaParser::ReadIndexData(Collada::MeshPtr& pMesh, tinyxml2::XMLElement* e)
    {
        size_t* vcount = nullptr;
        std::vector<InputChannel> perIndexData;

        // read primitive count from the attribute
        const char* chCount = e->Attribute("count");

        LOG_WRONG_FILE_FORMAT_IF(!chCount);

        size_t numPrimitives = boost::lexical_cast<size_t>(chCount);

        // material subgroup
        const char* chMaterial = e->Attribute("material");

        SubMesh subgroup;
        if (chMaterial)
            subgroup.material =  chMaterial;

        subgroup.numFaces = numPrimitives;

        pMesh->subMeshes.push_back(subgroup);

        // distinguish between polys and triangles
        std::string elementName = e->Value();

        PrimitiveType primType = Prim_Invalid;
        if ( elementName == "lines")
            primType = Prim_Lines;
        else if ( elementName == "linestrips")
            primType = Prim_LineStrip;
        else if ( elementName ==  "polygons")
            primType = Prim_Polygon;
        else if ( elementName ==  "polylist")
            primType = Prim_Polylist;
        else if ( elementName ==  "triangles")
            primType = Prim_Triangles;
        else if ( elementName ==  "trifans")
            primType = Prim_TriFans;
        else if ( elementName ==  "tristrips")
            primType = Prim_TriStrips;

        LOG_ASSERT(primType != Prim_Invalid, "Error");


        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "input")
            {
                ReadInputChannel(perIndexData, elem);
            }
            else if (value == "vcount")
            {
                if (numPrimitives > 0)
                {
                    const char* chContent = elem->GetText();
                    tinyxml2::XMLText* text = elem->FirstChild()->ToText();

                    LOG_WRONG_FILE_FORMAT_IF(!chContent);
//#ifdef AS_BDAE
                    delete[] vcount;
                    vcount = new size_t[numPrimitives];
                    std::vector<std::string> result;
                    result.reserve(numPrimitives);

                    boost::split(result, chContent , boost::is_any_of(" "), boost::token_compress_on);
                    uint32_t i = 0;
                    for (; i < result.size(); ++i)
                    {
                        std::string c = result[i];

                        if (!c.empty() && c != " ")
                        {
                            size_t v = boost::lexical_cast<size_t>(c);
                            vcount[i] = v;
                        }
                    }

//                     unsigned char* outenc = new unsigned char[BDaeBufferSize];
//                     const unsigned char* inenc = (const unsigned char*)(&vcount[0]);
//                     int encodedlen = zbase32_encode(outenc, inenc, i * sizeof(size_t) * 8);
//                     outenc[encodedlen] = 0;
//                     text->SetValue((const char*)outenc);
// #else
//                     if (binaryReader)
//                     {
//                         uint32_t encodedLen = strlen(content); //! WARN. Performance: content should be length prefixed
//                         unsigned char* decoded = new unsigned char[BDaeBufferSize];
//                         int decodedLen = zbase32_decode(decoded, (const unsigned char*)content, encodedLen * 8);
// 
//                         vcount = new size_t[numPrimitives];
//                     }
// #endif
                }
            }
            else if (value == "p")
            {
                ReadPrimitives(pMesh, perIndexData, numPrimitives, vcount, primType, elem);
            }
        }

        delete[] vcount;
    }

    void ColladaParser::ReadPrimitives(Collada::MeshPtr& pMesh,
                                       std::vector<Collada::InputChannel>& pPerIndexChannels, size_t pNumPrimitives,
                                       const size_t* pVCount, Collada::PrimitiveType pPrimType, tinyxml2::XMLElement* e)
    {
        // determine number of indices coming per vertex
        // find the offset index for all per-vertex channels
        size_t numOffsets = 1;
        size_t perVertexOffset = SIZE_MAX; // invalid value

        BOOST_FOREACH( const InputChannel& channel, pPerIndexChannels)
        {
            numOffsets = std::max( numOffsets, channel.offset + 1);
            if ( channel.type == IT_Vertex)
                perVertexOffset = channel.offset;
        }

        // determine the expected number of indices
        size_t expectedPointCount = 0;
        switch ( pPrimType)
        {
        case Prim_Polylist:
        {
            for (size_t i = 0; i < pNumPrimitives; ++i)
            {
                expectedPointCount += pVCount[i];
            }
            break;
        }
        case Prim_Lines:
            expectedPointCount = 2 * pNumPrimitives;
            break;
        case Prim_Triangles:
            expectedPointCount = 3 * pNumPrimitives;
            break;
        default:
            // other primitive types don't state the index count upfront... we need to guess
            break;
        }

        // and read all indices into a temporary array
        size_t* indices = nullptr;
        if ( expectedPointCount > 0)
        {
            indices = new size_t[expectedPointCount * numOffsets];
        }

        size_t indexCount = 0;

        if (pNumPrimitives > 0) // It is possible to not contain any indicies
        {
            const char* content = e->GetText();
            tinyxml2::XMLText* text = e->FirstChild()->ToText();
#ifdef AS_BDAE

            std::vector<std::string> vstr;

            /*boost::split(vstr, content , boost::is_any_of(" "), boost::token_compress_on);

            for (auto i = vstr.begin(), e = vstr.end(); i != e; ++i)
            {
                int value = boost::lexical_cast<int>(*i);
                indices.push_back(value);
            }*/

            std::string accum;
            while(content && *content)
            {
                if (*content != ' ')
                {
                    accum += *content;
                }

                if (*content == ' ')
                {
                    int value = boost::lexical_cast<int>(accum);
                    indices[indexCount++] = value;
                    accum.clear();
                }

                ++content;
            }

            if (!accum.empty())
            {
                int value = boost::lexical_cast<int>(accum);
                indices[indexCount++] = value;
            }

            unsigned char* outenc = new unsigned char[BDaeBufferSize];
            const unsigned char* inenc = (const unsigned char*)(&indices[0]);
            int encodedlen = zbase32_encode(outenc, inenc, indexCount * sizeof(size_t) * 8);
            outenc[encodedlen] = 0;

            std::ostringstream ss;
            ss << encodedlen << " ";
            ss << outenc;

            text->SetValue(ss.str().c_str());

            delete[] outenc;
#else
            if (binaryReader)
            {
                std::string encodedLenStr;
                while (*content != ' ')
                {
                    encodedLenStr += *content;
                    content++;
                }

                content++;

                uint32_t encodedLen = atoi(encodedLenStr.c_str());

                int decodedLen = zbase32_decode(m_bcolUnitBuffer, (const unsigned char*)content, encodedLen * 8);

                indexCount = expectedPointCount * numOffsets;

                indices = new size_t[indexCount];

                memcpy(indices, m_bcolUnitBuffer, indexCount * sizeof(size_t));
            }
#endif
        }

        // complain if the index count doesn't fit
        if ( expectedPointCount > 0 && indexCount != expectedPointCount * numOffsets)
        {
            LOG_ASSERT(false, "Index count doesn't fit");
        }
        else if ( expectedPointCount == 0 && (indexCount % numOffsets) != 0)
        {
            LOG_ASSERT(false, "Index count doesn't fit");
        }

        // find the data for all sources
        for ( std::vector<InputChannel>::iterator it = pMesh->perVertexData.begin();
                it != pMesh->perVertexData.end(); ++it)
        {
            InputChannel& input = *it;
            if ( input.resolved)
                continue;

            // find accessor
            input.resolved = ResolveLibraryReference( accessorLibrary, input.accessor);
            // resolve accessor's data pointer as well, if neccessary
            AccessorConstPtr acc = input.resolved;
            if ( !acc->data)
                acc->data = ResolveLibraryReference( dataLibrary, acc->source);
        }
        // and the same for the per-index channels
        for ( std::vector<InputChannel>::iterator it = pPerIndexChannels.begin();
                it != pPerIndexChannels.end(); ++it)
        {
            InputChannel& input = *it;
            if ( input.resolved)
                continue;

            // ignore vertex pointer, it doesn't refer to an accessor
            if ( input.type == IT_Vertex)
            {
                // warn if the vertex channel does not refer to the <vertices> element in the same mesh
                LOG_ASSERT(input.accessor == pMesh->vertexID, "Unsupported vertex referencing scheme.");
                continue;
            }

            // find accessor
            input.resolved = ResolveLibraryReference( accessorLibrary, input.accessor);
            // resolve accessor's data pointer as well, if neccessary
            AccessorConstPtr acc = input.resolved;
            if ( !acc->data)
                acc->data = ResolveLibraryReference( dataLibrary, acc->source);
        }


        // now assemble vertex data according to those indices
        size_t* idx = indices;

        // For continued primitives, the given count does not come all in one <p>, but only one primitive per <p>
        size_t numPrimitives = pNumPrimitives;
        if ( pPrimType == Prim_TriFans || pPrimType == Prim_Polygon)
            numPrimitives = 1;

        pMesh->faceSize.reserve( numPrimitives);
        pMesh->facePosIndices.reserve( indexCount / numOffsets);

        for ( size_t a = 0; a < numPrimitives; a++)
        {
            // determine number of points for this primitive
            size_t numPoints = 0;
            switch ( pPrimType)
            {
            case Prim_Lines:
                numPoints = 2;
                break;

            case Prim_Triangles:
                numPoints = 3;
                break;

            case Prim_Polylist:
                numPoints = pVCount[a];
                break;

            case Prim_TriFans:
            case Prim_Polygon:
                numPoints = indexCount / numOffsets;
                break;

            default:

                // LineStrip and TriStrip not supported due to expected index unmangling
                LOG_ASSERT(false, "Unsupported primitive type.");
                break;
            }

            // store the face size to later reconstruct the face from
            pMesh->faceSize.push_back( numPoints);

            // gather that number of vertices
            for ( size_t b = 0; b < numPoints; b++)
            {
                // read all indices for this vertex. Yes, in a hacky local array
                LOG_ASSERT( numOffsets < 20 && perVertexOffset < 20, "ERROR!");

                size_t vindex[20];
                for ( size_t offsets = 0; offsets < numOffsets; ++offsets)
                {
                    vindex[offsets] = *idx++;
                }

                // extract per-vertex channels using the global per-vertex offset
                for ( std::vector<InputChannel>::iterator it = pMesh->perVertexData.begin(); it != pMesh->perVertexData.end(); ++it)
                {
                    ExtractDataObjectFromChannel( *it, vindex[perVertexOffset], pMesh);
                }
                // and extract per-index channels using there specified offset
                for ( std::vector<InputChannel>::iterator it = pPerIndexChannels.begin(); it != pPerIndexChannels.end(); ++it)
                {
                    ExtractDataObjectFromChannel( *it, vindex[it->offset], pMesh);
                }

                // store the vertex-data index for later assignment of bone vertex weights
                pMesh->facePosIndices.push_back( vindex[perVertexOffset]);
            }
        }

        delete[] indices;
    }

    void ColladaParser::ExtractDataObjectFromChannel(const Collada::InputChannel& pInput, size_t pLocalIndex, Collada::MeshPtr pMesh)
    {
        // ignore vertex referrer - we handle them that separate
        if ( pInput.type == IT_Vertex)
            return;

        const Accessor& acc = *pInput.resolved;
        if ( pLocalIndex >= acc.count)
        {
            LOG_ASSERT(pLocalIndex < acc.count, boost::str( boost::format( "Invalid data index (%d/%d) in primitive specification") % pLocalIndex % acc.count));
            return;
        }

        // get a pointer to the start of the data object referred to by the accessor and the local index
        const float* dataObject = &(acc.data->values[0]) + acc.offset + pLocalIndex * acc.stride;

        // assemble according to the accessors component sub-offset list. We don't care, yet,
        // what kind of object exactly we're extracting here
        float obj[4];
        for ( size_t c = 0; c < 4; ++c)
        {
            obj[c] = dataObject[acc.subOffset[c]];
        }

        // now we reinterpret it according to the type we're reading here
        switch ( pInput.type)
        {
        case IT_Position: // ignore all position streams except 0 - there can be only one position
        {
            if ( pInput.index == 0)
            {
                pMesh->positions.push_back( glm::vec3( obj[0], obj[1], obj[2]));
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: just one vertex position stream supported");
            }
        }
            break;

        case IT_Normal:
        {
            // pad to current vertex count if necessary
            if ( pMesh->normals.size() < pMesh->positions.size() - 1)
            {
                pMesh->normals.insert( pMesh->normals.end(), pMesh->positions.size() - pMesh->normals.size() - 1, glm::vec3( 0, 1, 0));
            }

            // ignore all normal streams except 0 - there can be only one normal
            if ( pInput.index == 0)
            {
                pMesh->normals.push_back( glm::vec3( obj[0], obj[1], obj[2]));
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: just one vertex normal stream supported");
            }
        }
            break;

        case IT_Tangent:
        {
            // pad to current vertex count if necessary
            if ( pMesh->tangents.size() < pMesh->positions.size() - 1)
            {
                pMesh->tangents.insert( pMesh->tangents.end(), pMesh->positions.size() - pMesh->tangents.size() - 1, glm::vec3( 1, 0, 0));
            }

            // ignore all tangent streams except 0 - there can be only one tangent
            if ( pInput.index == 0)
            {
                pMesh->tangents.push_back( glm::vec3( obj[0], obj[1], obj[2]));
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: just one vertex tangent stream supported");
            }
        }
            break;

        case IT_Bitangent:
        {
            // pad to current vertex count if necessary
            if ( pMesh->bitangents.size() < pMesh->positions.size() - 1)
            {
                pMesh->bitangents.insert( pMesh->bitangents.end(), pMesh->positions.size() - pMesh->bitangents.size() - 1, glm::vec3( 0, 0, 1));
            }

            // ignore all bitangent streams except 0 - there can be only one bitangent
            if ( pInput.index == 0)
            {
                pMesh->bitangents.push_back( glm::vec3( obj[0], obj[1], obj[2]));
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: just one vertex bitangent stream supported");
            }
            break;
        }

        case IT_Texcoord:
        {
            // up to 4 texture coord sets are fine, ignore the others
            if ( pInput.index < _MAX_NUMBER_OF_TEXTURECOORDS)
            {
                // pad to current vertex count if necessary
                if ( pMesh->texCoords[pInput.index].size() < pMesh->positions.size() - 1)
                {
                    pMesh->texCoords[pInput.index].insert( pMesh->texCoords[pInput.index].end(),
                                                          pMesh->positions.size() - pMesh->texCoords[pInput.index].size() - 1, glm::vec3( 0, 0, 0));
                }

                pMesh->texCoords[pInput.index].push_back( glm::vec3( obj[0], obj[1], obj[2]));
                if (0 != acc.subOffset[2] || 0 != acc.subOffset[3]) /* hack ... consider cleaner solution */
                {
                    pMesh->numUVComponents[pInput.index] = 3;
                }
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: too many texture coordinate sets. Skipping.");
            }
        }

            break;

        case IT_Color:
        {
            // up to 4 color sets are fine, ignore the others
            if ( pInput.index < _MAX_NUMBER_OF_COLOR_SETS)
            {
                // pad to current vertex count if necessary
                if ( pMesh->colors[pInput.index].size() < pMesh->positions.size() - 1)
                {
                    pMesh->colors[pInput.index].insert( pMesh->colors[pInput.index].end(),
                                                       pMesh->positions.size() - pMesh->colors[pInput.index].size() - 1, Color_t( 0, 0, 0, 1));
                }

                Color_t result(0, 0, 0, 1);
                for (size_t i = 0; i < pInput.resolved->size; ++i)
                {
                    result[i] = obj[pInput.resolved->subOffset[i]];
                }
                pMesh->colors[pInput.index].push_back(result);
            }
            else
            {
                OGL_TRACE_ERROR(m_logger, "Collada: too many vertex color sets. Skipping.");
            }
        }

            break;

        default:
            // IT_Invalid and IT_Vertex
            LOG_ASSERT(false , "shouldn't ever get here");
        }
    }

    void ColladaParser::ReadMesh(Collada::MeshPtr& pMesh, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "source")
            {
                OGL_TRACE_INFO(m_logger, "\t Read source data");
                ReadSource(elem);
            }
            else if (value == "vertices")
            {
                OGL_TRACE_INFO(m_logger, "\t Read vertex data");
                ReadVertexData(pMesh, elem);
            }
            else if (value == "triangles" || value == "lines" || value == "linestrips" || value == "polygons" || value == "polylist"
                    || value == "trifans" || value == "tristrips")
            {
                OGL_TRACE_INFO(m_logger, "\t Read index data");
                ReadIndexData(pMesh, elem);
            }
        }
    }

    void ColladaParser::ReadGeometry(Collada::MeshPtr& pMesh, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "mesh")
            {
                ReadMesh(pMesh, elem);
            }
        }
    }

    void ColladaParser::ReadGeometryLibrary(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "geometry")
            {
                const char* chID = elem->Attribute("id");

                LOG_WRONG_FILE_FORMAT_IF(!chID);

                std::string id = chID;

                OGL_TRACE_INFO(m_logger, "Read geometry [" << id << "]");

                Collada::MeshPtr mesh(new Collada::Mesh);
                meshLibrary[id] = mesh;

                ReadGeometry(mesh, elem);
            }
        }
    }

    void ColladaParser::ReadScene(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "instance_visual_scene")
            {
                LOG_ASSERT(!rootNode, "Invalid scene containing multiple root nodes");

                const char* chUrl = elem->Attribute("url");
                if (chUrl[0] != '#')
                {
                    LOG_ASSERT(false, "Unknown reference format");
                }

                NodeLibrary_t::const_iterator it = nodeLibrary.find(chUrl + 1);
                if (it == nodeLibrary.end())
                {
                    LOG_ASSERT(false, "Unable to resolve visual_scene reference " + std::string(chUrl));
                }

                rootNode = it->second;
            }
        }
    }

    void ColladaParser::ReadSceneNode(NodePtr pNode, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "node")
            {
                NodePtr child(new Node);
                const char* chID = elem->Attribute( "id");
                if (chID)
                {
                    child->ID = chID;
                }

                const char* chSID = elem->Attribute( "sid");
                if (chSID)
                {
                    child->SID = chSID;
                }

                const char* chName = elem->Attribute( "name");
                if (chName)
                {
                    child->name = chName;
                }

                // TODO: (thom) support SIDs
                // ai_assert( TestAttribute( "sid") == -1);

                if (pNode)
                {
                    pNode->children.push_back(child);
                    child->parent = pNode;
                }
                else
                {
                    // no parent node given, probably called from <library_nodes> element.
                    // create new node in node library
                    nodeLibrary[child->ID] = child;
                }

                OGL_TRACE_DEBUG(m_logger, "Read object " << child->name);

                // read on recursively from there
                ReadSceneNode(child, elem);
                continue;
            }
            else if (!pNode)
            {
                continue;
            }
            else if (value == "lookat")
            {
                ReadNodeTransformation(pNode, TF_LOOKAT, elem);
            }
            else if (value == "matrix")
            {
                ReadNodeTransformation(pNode, TF_MATRIX, elem);
            }
            else if (value == "ratate")
            {
                ReadNodeTransformation(pNode, TF_ROTATE, elem);
            }
            else if (value == "scale")
            {
                ReadNodeTransformation(pNode, TF_SCALE, elem);
            }
            else if (value == "skew")
            {
                ReadNodeTransformation(pNode, TF_SKEW, elem);
            }
            else if (value == "translate")
            {
                ReadNodeTransformation(pNode, TF_TRANSLATE, elem);
            }
            else if (value == "render" && pNode->parent.lock() && pNode->primaryCamera.empty())
            {
                // ... scene evaluation or, in other words, postprocessing pipeline,
                // or, again in other words, a turing-complete description how to
                // render a Collada scene. The only thing that is interesting for
                // us is the primary camera.

                const char* chCameraNode = elem->Attribute("camera_node");

                if (chCameraNode)
                {
                    if (chCameraNode[0] != '#')
                    {
                        OGL_TRACE_ERROR(m_logger, "Unresolved reference format of camera");
                    }
                    else
                    {
                        pNode->primaryCamera = chCameraNode + 1;
                        OGL_TRACE_DEBUG(m_logger, "Detected primary camera " << pNode->primaryCamera);
                    }
                }
            }
            else if (value == "instanced_node")
            {
                // find the node in the library
                const char* chUrl = elem->Attribute( "url");
                if (!chUrl)
                {
                    if (chUrl[0] != '#')
                    {
                        OGL_TRACE_DEBUG(m_logger, "Unresolved reference format of node");
                    }
                    else
                    {
                        pNode->nodeInstances.push_back(NodeInstance());
                        pNode->nodeInstances.back().node = chUrl + 1;
                    }
                }
            }
            else if (value == "instance_geometry" || value == "instance_controller")
            {
                // Reference to a mesh or controller, with possible material associations
                ReadNodeGeometry(pNode, elem);
            }
            else if (value == "instance_light")
            {
                const char* chUrl = elem->Attribute("url");
                if (!chUrl)
                {
                    OGL_TRACE_ERROR(m_logger, "Expected url attribute in <instance_light> element");
                }
                else
                {
                    if (chUrl[0] != '#')
                    {
                        OGL_TRACE_ASSERT(m_logger, false, "Unknown reference format in <instance_light> element");
                    }
                    else
                    {
                        pNode->lights.push_back(LightInstance());
                        pNode->lights.back().light = chUrl + 1;
                    }
                }
            }
            else if (value == "instance_camera")
            {
                // Reference to a camera, name given in 'url' attribute
                const char* chUrl = elem->Attribute("url");
                if (!chUrl)
                {
                    OGL_TRACE_WARN(m_logger, "Expected url attribute in <instance_camera> element");
                }
                else
                {
                    if ( chUrl[0] != '#')
                    {
                        LOG_ASSERT(false, "Unknown reference format in <instance_camera> element");
                    }
                    else
                    {
                        pNode->cameras.push_back(CameraInstance());
                        pNode->cameras.back().camera = chUrl + 1;
                    }
                }
            }
        }
    }

    void ColladaParser::ReadNodeTransformation(Collada::NodePtr& pNode, Collada::ETransformType pType, tinyxml2::XMLElement* e)
    {
        std::string tagName = e->Value();

        Transform tf;
        tf.type = pType;

        const char* chSid = e->Attribute("sid");
        if (chSid)
        {
            tf.ID = chSid;
        }

        std::string content = e->GetText();

        // read as many parameters and store in the transformation
        std::vector<std::string> split;

        boost::split(split, content, boost::is_any_of(" "), boost::token_compress_on);

        LOG_ASSERT(split.size() <= 16, content);

        for (uint32_t i = 0; i < split.size(); ++i)
        {
            tf.f[i] = boost::lexical_cast<float>(split[i]);
        }

        pNode->transforms.push_back(tf);
    }

    void ColladaParser::ReadNodeGeometry(NodePtr pNode, tinyxml2::XMLElement* e)
    {
        // referred mesh is given as an attribute of the <instance_geometry> element
        const char* chUrl = e->Attribute( "url");
        if ( chUrl[0] != '#')
        {
            LOG_ASSERT(false, "Unknown reference format");
            return;
        }

        Collada::MeshInstance instance;
        instance.meshOrController = chUrl + 1; // skipping the leading #

        // read material associations. Ignore additional elements in between
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "bind_material")
            {
                tinyxml2::XMLElement* materialElem = elem->FirstChildElement()->FirstChildElement();

                LOG_WRONG_FILE_FORMAT_IF(materialElem == nullptr);

                value = materialElem->Value();

                if (value == "instance_material")
                {
                    // read ID of the geometry subgroup and the target material
                    std::string group = materialElem->Attribute("symbol");
                    const char* chUrlMat = materialElem->Attribute("target");
                    Collada::SemanticMappingTable s;
                    if ( chUrlMat[0] == '#')
                    {
                        chUrlMat++;
                    }

                    s.matName = chUrlMat;

                    // resolve further material details + THIS UGLY AND NASTY semantic mapping stuff
                    ReadMaterialVertexInputBinding(s, materialElem);

                    // store the association
                    instance.materials[group] = s;
                }
            }
        }

        // store it
        pNode->meshes.push_back( instance);
    }

    void ColladaParser::ReadMaterialVertexInputBinding(Collada::SemanticMappingTable& tbl, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "bind_vertex_input")
            {
                Collada::InputSemanticMapEntry vn;

                std::string semantic = elem->Attribute("semantic");

                // input semantic
                vn.type = GetTypeForSemantic( elem->Attribute("input_semantic") );

                // index of input set
                const char* chInputSet = elem->Attribute("input_set");
                if (chInputSet)
                {
                    vn.set = boost::lexical_cast<unsigned int>(chInputSet);
                }

                tbl.table[semantic] = vn;
            }
            else if ( value == "bind")
            {
                OGL_TRACE_WARN(m_logger, "Found unsupported <bind> element");
            }
        }
    }

    void ColladaParser::ReadSceneLibrary(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "visual_scene")
            {
                // read ID. Is optional according to the spec, but how on earth should a scene_instance refer to it then?
                const char* chID = elem->Attribute("id");
                LOG_ASSERT(chID, "read ID. Is optional according to the spec, but how on earth should a scene_instance refer to it then?");

                // read name if given.
                const char* chName = elem->Attribute( "name");
                std::string name;
                if (chName)
                {
                    name = chName;
                }
                else
                {
                    name = "unnamed";
                }

                // create a node and store it in the library under its ID
                NodePtr node(new Node());
                node->ID = chID;
                node->name = name;
                nodeLibrary[node->ID] = node;

                ReadSceneNode(node, elem);
            }
        }
    }

    void ColladaParser::ReadMaterial(Collada::Material& pMaterial, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "instance_effect")
            {
                std::string url = elem->Attribute("url");

                LOG_WRONG_FILE_FORMAT_IF(url[0] != '#');

                pMaterial.effect = url.c_str() + 1;
            }
        }
    }

    void ColladaParser::ReadMaterialLibrary(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "material")
            {
                std::string id = elem->Attribute("id");
                MaterialPtr newMaterial(new Material());
                materialLibrary[id] = newMaterial;

                ReadMaterial(*newMaterial, elem);
            }
        }
    }

    void ColladaParser::ReadEffectParam(Collada::EffectParam& pParam, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "surface")
            {
                tinyxml2::XMLElement* expectedElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());

                if (expectedElem)
                {
                    std::string init_from = expectedElem->Value();

                    if (init_from == "init_from")
                    {
                        std::string reference = expectedElem->GetText();
                        pParam.type = Param_Surface;
                        pParam.reference = reference;
                    }
                }
            }
            else if (value == "sampler2D")
            {
                tinyxml2::XMLElement* expectedElem = static_cast<tinyxml2::XMLElement*> (elem->FirstChild());

                if (expectedElem)
                {
                    std::string source = expectedElem->Value();

                    if (source == "source")
                    {
                        std::string reference = expectedElem->GetText();
                        pParam.type = Param_Sampler;
                        pParam.reference = reference;
                    }
                }
            }
        }
    }

    void ColladaParser::ReadSamplerProperties(Collada::Sampler& pSampler, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "wrapU")
            {
                pSampler.wrapU = boost::lexical_cast<bool>(elem->GetText());
            }
        }
    }

    void ColladaParser::ReadEffectColor(Color_t& pColor, Collada::Sampler& pSampler, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "color")
            {
                std::string content = elem->GetText();

                std::vector<std::string> vec;
                boost::split(vec, content, boost::is_any_of(" "), boost::token_compress_on);

                LOG_WRONG_FILE_FORMAT_IF(vec.size() != 4);

                pColor.r = boost::lexical_cast<float>(vec[0]);
                pColor.g = boost::lexical_cast<float>(vec[1]);
                pColor.b = boost::lexical_cast<float>(vec[2]);
                pColor.a = boost::lexical_cast<float>(vec[3]);
            }
            else if (value == "texture")
            {
                pSampler.name = elem->Attribute("texture");
                pSampler.UVChannel = elem->Attribute("texcoord");
            }
            else if (value == "technique")
            {
                const char* profile = elem->Attribute("profile");

                if (!strcmp(profile, "MAYA") || !strcmp(profile, "MAX3D"))
                {
                    ReadSamplerProperties(pSampler, elem);
                }
            }
        }
    }

    void ColladaParser::ReadEffectFloat(float& pFloat, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "float")
            {
                pFloat = boost::lexical_cast<float>(elem->GetText());
            }
        }
    }


    void ColladaParser::ReadEffectTechniqueCommon( Collada::Effect& effect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "phong")
            {
                effect.shadeType = Shade_Phong;
                ReadEffectPhong(effect, elem);
            }
            else if (value == "constant")
            {
                effect.shadeType = Shade_Constant;
            }
            else if (value == "lambert")
            {
                effect.shadeType = Shade_Lambert;
                ReadEffectLambert(effect, elem);
            }
            else if (value == "blinn")
            {
                effect.shadeType = Shade_Blinn;
            }
            else if (value == "extra")
            {
                ReadEffectExtra(effect, elem);
            }
        }
    }

    void ColladaParser::ReadEffectLambert( Collada::Effect& effect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "emission")
            {
                ReadEffectColor(effect.emissive, effect.texEmissive, elem);
            }
            else if (value == "ambient")
            {
                ReadEffectColor(effect.ambient, effect.texAmbient, elem);
            }
            else if (value == "diffuse")
            {
                ReadEffectColor(effect.diffuse, effect.texDiffuse, elem);
            }
            else if (value == "reflective")
            {
                ReadEffectColor(effect.reflective, effect.texReflective, elem);
            }
            else if (value == "reflectivity")
            {
                ReadEffectFloat(effect.reflectivity, elem);
            }
            else if (value == "index_of_refraction")
            {
                ReadEffectFloat(effect.refractIndex, elem);
            }
        }
    }

    void ColladaParser::ReadEffectPhong( Collada::Effect& effect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "emission")
            {
                ReadEffectColor(effect.emissive, effect.texEmissive, elem);
            }
            else if (value == "ambient")
            {
                ReadEffectColor(effect.ambient, effect.texAmbient, elem);
            }
            else if (value == "diffuse")
            {
                ReadEffectColor(effect.diffuse, effect.texDiffuse, elem);
            }
            else if (value == "specular")
            {
                ReadEffectColor(effect.specular, effect.texSpecular, elem);
            }
            else if (value == "reflective")
            {
                ReadEffectColor(effect.reflective, effect.texReflective, elem);
            }
            else if (value == "transparent")
            {
                ReadEffectColor(effect.transparent, effect.texTransparent, elem);
            }
            else if (value == "shininess")
            {
                ReadEffectFloat(effect.shininess, elem);
            }
            else if (value == "reflectivity")
            {
                ReadEffectFloat(effect.reflectivity, elem);
            }
            else if (value == "transparency")
            {
                ReadEffectFloat(effect.transparency, elem);
            }
            else if (value == "index_of_refraction")
            {
                ReadEffectFloat(effect.refractIndex, elem);
            }
        }
    }

    void ColladaParser::ReadEffectTechniqueExtra(Collada::Effect& effect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            const std::string value = elem->Value();

            if (value == "bump")
            {
                Color_t dummy;
                ReadEffectColor(dummy, effect.texBump, elem);
            }
        }
    }

    void ColladaParser::ReadEffectExtra( Collada::Effect& effect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            const std::string value = elem->Value();

            if (value == "double_sided")
            {
                effect.doubleSided = ReadBoolFromTextContent(elem->GetText());
            }
            else if (value == "technique")
            {
                ReadEffectTechniqueExtra(effect, elem);
            }
        }
    }

    void ColladaParser::ReadEffectProfileCommon(Collada::Effect& pEffect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "newparam")
            {
                std::string sid = elem->Attribute("sid");
                pEffect.params[sid] = EffectParam();
                ReadEffectParam(pEffect.params[sid], elem);
            }
            else if (value == "technique")
            {
                ReadEffectTechniqueCommon(pEffect, elem);
            }
            else if (value == "extra")
            {
                ReadEffectExtra(pEffect, elem);
            }
            /*else if (value == "bump")
            {
                Color_t dummy;
                ReadEffectColor(dummy, pEffect.texBump, elem);
            }
            else if (value == "wireframe")
            {
                pEffect.wireframe = ReadBoolFromTextContent(elem->GetText());
            }
            else if (value == "faceted")
            {
                pEffect.faceted = ReadBoolFromTextContent(elem->GetText());
            }*/
        }
    }

    bool ColladaParser::ReadBoolFromTextContent(const char* text)
    {
        LOG_ASSERT(text, "NULL pointer argument");
        return boost::lexical_cast<bool>(text);
    }

    void ColladaParser::ReadEffect(Collada::Effect& pEffect, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "profile_COMMON")
            {
                ReadEffectProfileCommon(pEffect, elem);
            }
        }
    }

    void ColladaParser::ReadEffectLibrary(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "effect")
            {
                std::string id = elem->Attribute("id");

                EffectPtr newEffect(new Effect());
                effectLibrary[id] = newEffect;

                ReadEffect(*newEffect, elem);
            }
        }
    }

    void ColladaParser::ReadImageLibrary(tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "image")
            {
                std::string id = elem->Attribute("id");

                ImagePtr newImage(new Image());
                imageLibrary[id] = newImage;
                ReadImage(*newImage, elem);
            }
        }
    }

    void ColladaParser::ReadImage(Collada::Image& pImage, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild()); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "init_from")
            {
                pImage.fileName = elem->GetText();

                pImage.fileName.replace_extension(Global::FileSystemMappings::TexturesExt);
                
                OGL_TRACE_DEBUG(m_logger, "Read new image " << pImage.fileName);
            }
        }
    }

    void ColladaParser::ReadStructure(tinyxml2::XMLElement* e)
    {
        tinyxml2::XMLElement* elem = static_cast<tinyxml2::XMLElement*> (e->FirstChild());

        while (elem)
        {
            std::string elemName = elem->Value();

            OGL_TRACE_DEBUG(m_logger, "Reading " << elemName);

            if (elemName == "asset")
            {
                ReadAssetInfo(elem);
            }
            else if (elemName == "library_animations")
            {
#ifndef AS_BDAE
                ReadAnimationLibrary(elem);
#endif
            }
            else if (elemName == "library_controllers")
            {

            }
            else if (elemName == "library_images")
            {
#ifndef AS_BDAE
                ReadImageLibrary(elem);
#endif
            }
            else if (elemName == "library_materials")
            {
#ifndef AS_BDAE
                ReadMaterialLibrary(elem);
#endif
            }
            else if (elemName == "library_effects")
            {
#ifndef AS_BDAE
                ReadEffectLibrary(elem);
#endif
            }
            else if (elemName == "library_geometries")
            {
                ReadGeometryLibrary(elem);
            }
            else if (elemName == "library_visual_scenes")
            {
#ifndef AS_BDAE
                ReadSceneLibrary(elem);
#endif
            }
            else if (elemName == "library_lights")
            {
#ifndef AS_BDAE
                ReadLightLibrary(elem);
#endif
            }
            else if (elemName == "library_cameras")
            {

            }
            else if (elemName == "library_nodes")
            {
#ifndef AS_BDAE
                ReadSceneNode(NodePtr(), elem);
#endif
            }
            else if (elemName == "scene")
            {
#ifndef AS_BDAE
                ReadScene(elem);
#endif
            }

            elem = elem->NextSiblingElement();
        }
    }

    glm::mat4 ColladaParser::CalculateResultTransform(const std::vector<Collada::Transform>& pTransforms) const
    {
        glm::mat4 res;

        for ( std::vector<Transform>::const_iterator it = pTransforms.begin(); it != pTransforms.end(); ++it)
        {
            const Transform& tf = *it;

            switch ( tf.type)
            {
            case TF_LOOKAT:
            {
                vec3 pos( tf.f[0], tf.f[1], tf.f[2]);
                vec3 dstPos( tf.f[3], tf.f[4], tf.f[5]);
                vec3 up = glm::normalize(vec3( tf.f[6], tf.f[7], tf.f[8]));
                vec3 dir = glm::normalize(vec3( dstPos - pos));
                vec3 right =  glm::cross(dir, up);

                res *= mat4(
                            right.x, up.x, -dir.x, pos.x,
                            right.y, up.y, -dir.y, pos.y,
                            right.z, up.z, -dir.z, pos.z,
                            0, 0, 0, 1);
                break;
            }

            case TF_ROTATE:
            {
                mat4 rot;
                float angle = tf.f[3] * float(M_PI) / 180.0f;
                vec3 axis( tf.f[0], tf.f[1], tf.f[2]);
                glm::rotate(rot, angle, axis);
                res *= rot;
                break;
            }

            case TF_TRANSLATE:
            {
                mat4 trans;
                glm::translate(trans, vec3( tf.f[0], tf.f[1], tf.f[2]));
                res *= trans;
                break;
            }

            case TF_SCALE:
            {
                mat4 scale( tf.f[0], 0.0f,      0.0f,    0.0f,
                           0.0f,    tf.f[1],   0.0f,    0.0f,
                           0.0f,    0.0f,      tf.f[2], 0.0f,
                           0.0f,    0.0f,      0.0f,    1.0f);

                res *= scale;
                break;
            }

            case TF_SKEW:
            {
                // TODO: (thom)
                assert(false);
                break;
            }

            case TF_MATRIX:
            {
                mat4 mat( tf.f[0], tf.f[1], tf.f[2], tf.f[3], tf.f[4], tf.f[5], tf.f[6], tf.f[7],
                         tf.f[8], tf.f[9], tf.f[10], tf.f[11], tf.f[12], tf.f[13], tf.f[14], tf.f[15]);
                res *= mat;
                break;
            }

            default:
                assert( false);
                break;
            }
        }

        res = glm::transpose(res);

        return res;
    }

    bool ColladaParser::ReadContents()
    {
        if (!boost::filesystem::exists(fileName))
        {
            OGL_TRACE_ERROR(m_logger, "Scene file does not exists " + fileName);

            return false;
        }

        LOG_ASSERT(!fileName.empty(), "Empty string provided.");

        std::string ext = boost::filesystem::extension(fileName);

        binaryReader = ext == ".bdae";

        if(binaryReader)
        {
            m_bcolUnitBuffer = new unsigned char[BDaeBufferSize];
        }
        else
        {
            m_bcolUnitBuffer = nullptr;
        }

        int r = reader.LoadFile(fileName.c_str());

        if (r)
        {
            OGL_TRACE_ERROR(m_logger, "Cannot read scene file " + fileName);
            return false;
        }

        tinyxml2::XMLElement* elem = reader.FirstChildElement();

        if (elem)
        {
            const char* verstr = elem->Attribute("version");

            LOG_ASSERT(verstr, "Wrong file format " << fileName);

            if (!strncmp(verstr , "1.5", 3))
            {
                format = FV_1_5_n;
            }
            else if (!strncmp(verstr, "1.4", 3))
            {
                format = FV_1_4_n;
            }
            else if (!strncmp(verstr, "1.3", 3))
            {
                format = FV_1_3_n;
            }

            OGL_TRACE_DEBUG(m_logger, boost::str(boost::format("Collada version is %s") % verstr));

            try
            {
                ReadStructure(elem);
            }
            catch (boost::bad_lexical_cast& e)
            {
                OGL_TRACE_ERROR(m_logger, "Bad lexical cast " << e.what());
                assert(false);
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    void ColladaParser::ReadLightLibrary( tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = e->FirstChildElement(); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "light")
            {
                std::string id = elem->Attribute("id");

                Collada::LightPtr light(new Collada::Light());
                lightLibrary[id] = light;

                ReadLight(*light, elem);
            }
        }
    }


    void ColladaParser::ReadLightBaseData( Collada::Light& pLight, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = e->FirstChildElement(); elem; elem = elem->NextSiblingElement())
        {
            std::string techValue = elem->Value();

            if (techValue == "color")
            {
                std::vector<std::string> vals;
                std::string content = elem->GetText();
                boost::split(vals, content, boost::is_any_of(" "));

                pLight.color.r = boost::lexical_cast<float>(vals[0]);
                pLight.color.g = boost::lexical_cast<float>(vals[1]);
                pLight.color.b = boost::lexical_cast<float>(vals[2]);
            }
            else if (techValue == "constant_attenuation")
            {
                pLight.attConstant = boost::lexical_cast<float>(elem->GetText());
            }
            else if (techValue == "linear_attenuation")
            {
                pLight.attLinear = boost::lexical_cast<float>(elem->GetText());
            }
            else if (techValue == "quadratic_attenuation")
            {
                pLight.attQuadratic = boost::lexical_cast<float>(elem->GetText());
            }
        }
    }

    void ColladaParser::ReadPointLight(Collada::Light& pLight, tinyxml2::XMLElement* e)
    {
        ReadLightBaseData(pLight, e);
    }

    void ColladaParser::ReadSpotLight( Collada::Light& pLight, tinyxml2::XMLElement* e)
    {
        ReadLightBaseData(pLight, e);

        for (tinyxml2::XMLElement* elem = e->FirstChildElement(); elem; elem = elem->NextSiblingElement())
        {
            std::string techValue = elem->Value();

            if (techValue == "falloff_angle")
            {
                pLight.falloffAngle = boost::lexical_cast<float>(elem->GetText());
            }
            else if(techValue == "falloff_exponent")
            {
                pLight.falloffExponent = boost::lexical_cast<float>(elem->GetText());
            }
        }
    }

    void ColladaParser::ReadLight( Collada::Light& pLight, tinyxml2::XMLElement* e)
    {
        for (tinyxml2::XMLElement* elem = e->FirstChildElement(); elem; elem = elem->NextSiblingElement())
        {
            std::string value = elem->Value();

            if (value == "technique_common")
            {
                for (tinyxml2::XMLElement* techElem = elem->FirstChildElement(); techElem; techElem = techElem->NextSiblingElement())
                {
                    std::string techValue = techElem->Value();

                    if (techValue == "spot")
                    {
                        pLight.type = LightTypes::SPOT;

                        ReadSpotLight(pLight, techElem);
                    }
                    else if (techValue == "ambient")
                    {
                        pLight.type = LightTypes::AMBIENT;
                    }
                    else if (techValue == "directional")
                    {
                        pLight.type = LightTypes::DIRECTIONAL;
                    }
                    else if (techValue == "point")
                    {
                        pLight.type = LightTypes::POINT;

                        ReadPointLight(pLight, techElem);
                    }
                    // FCOLLADA extensions 
                    // -------------------------------------------------------
                    else if (techValue == "outer_cone")
                    {
                        pLight.outerAngle = boost::lexical_cast<float>(techElem->GetText());
                    }
                    // ... and this one is even deprecated
                    else if (techValue == "penumbra_angle")
                    {
                        pLight.penumbraAngle = boost::lexical_cast<float>(techElem->GetText());
                    }
                    else if (techValue == "intensity")
                    {
                        pLight.intensity = boost::lexical_cast<float>(techElem->GetText());
                    }
                    else if (techValue == "falloff")
                    {
                        pLight.outerAngle = boost::lexical_cast<float>(techElem->GetText());
                    }
                    else if (techValue == "hotspot_beam")
                    {
                        pLight.falloffAngle = boost::lexical_cast<float>(techElem->GetText());
                    }
                }
            }
            else if (value == "extra")
            {
                tinyxml2::XMLElement* extraElem = elem->FirstChildElement();
                LOG_WRONG_FILE_FORMAT_IF(!extraElem);

                std::string extraValue = extraElem->Value();

                if (extraValue == "technique")
                {
                    const char* profile = extraElem->Attribute("profile");

                    LOG_WRONG_FILE_FORMAT_IF(!profile);

                    if (strcmp(profile, "blender") == 0)
                    {
                        for (tinyxml2::XMLElement* techElem = extraElem->FirstChildElement(); techElem; techElem = techElem->NextSiblingElement())
                        {
                            const std::string techValue = techElem->Value();

                            if (techValue == "dist")
                            {
                                pLight.dist = boost::lexical_cast<float>(techElem->GetText());
                            }
                            else if (techValue == "energy")
                            {
                                pLight.intensity = boost::lexical_cast<float>(techElem->GetText());
                            }
                            else if (techValue == "falloff_type")
                            {
                                pLight.falloffType = boost::lexical_cast<unsigned int>(techElem->GetText());

                                OGL_TRACE_INFO(m_logger, "Read light falloff type " << LightFalloffTypes::toString((LightFalloffTypes::Enum)pLight.falloffType));
                            }
                        }

                    }
                }
            }

            OGL_TRACE_INFO(m_logger, "Read light " << LightTypes::toString((LightTypes::Enum)pLight.type));
        }
    }

    ColladaParser::~ColladaParser()
    {
        delete[] m_bcolUnitBuffer;
    }


} //Collada
