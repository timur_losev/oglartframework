#include "Precompiled.h"
#include "ShaderObject.h"
#include "RenderUtils.h"
#include "Logger.h"

ShaderObject::ShaderObject( ShaderObjectType::Enum withType ):
    handle(0)
    ,objectType(withType)
    ,compiled(FALSE)
{

}

ShaderObject::~ShaderObject()
{
    glo::glDeleteShader(handle);
}

bool ShaderObject::compile( const std::string& sourceCode )
{
    return compile(sourceCode.c_str(), sourceCode.length());
}

bool ShaderObject::compile(const char* data, size_t len)
{
#ifdef DEBUG_MODE
    source.assign(data, len);
#endif

    handle = glo::glCreateShader(objectType);

    glo::glShaderSource(handle, 1, &data, (GLint*)&len);

    glo::glCompileShader(handle);

    if (rut::shaderStatus(handle, GL_COMPILE_STATUS) != GL_TRUE)
    {
        return false;
    }
    else
    {
        compiled = TRUE;

        OGL_TRACE_DEBUG(GetLoggerPtr(), "Shader object " << handle << " compiled and ready for use.");

        return true;
    }
}

GLuint ShaderObject::getHandle() const
{
    return handle;
}

Bool_t ShaderObject::hasCompiled() const
{
    return compiled;
}

