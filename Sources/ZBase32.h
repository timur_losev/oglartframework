#pragma once

extern int zbase32_decode(unsigned char *decoded,
                   const unsigned char *encoded,
                   unsigned int bits);

extern int zbase32_encode(unsigned char *encoded,
                   const unsigned char *input,
                   unsigned int bits);