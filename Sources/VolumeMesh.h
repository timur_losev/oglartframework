#pragma once

#include "GraphicMesh.h"

forward_this(VolumeMesh);

class VolumeMesh : public GraphicMesh
{
public:
    VolumeMesh();
    ~VolumeMesh();

    typedef std::vector<uint32_t> Indices_t;
    Indices_t indices;

    virtual bool commit(GLuint usage = GL_STATIC_DRAW);
};