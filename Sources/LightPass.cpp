#include "Precompiled.h"
#include "LightPass.h"
#include "ShaderProgramManager.h"
#include "ShaderProgram.h"
#include "Vertex.h"

#include "TextureManager.h"
#include "Texture.h"
#include "RenderUtils.h"
#include "Light.h"

LightPass::LightPass():
    m_ambientIntensity(0.1f),
    m_diffuseIntensity(0.8f)
{

}

LightPass::~LightPass()
{

}

void LightPass::use(LightConstPtr light)
{
    use();

    glo::glUniform3f(m_baseLightLoc.Color, light->colorDiffuse.r,
        light->colorDiffuse.g, light->colorDiffuse.b);

    if (m_baseLightLoc.AmbientIntensity != INVALID_ID)
        glo::glUniform1f(m_baseLightLoc.AmbientIntensity, m_ambientIntensity);

    if (m_baseLightLoc.DiffuseIntensity != INVALID_ID)
        glo::glUniform1f(m_baseLightLoc.DiffuseIntensity, m_diffuseIntensity);

}

void LightPass::use()
{
    ShaderProgramManager::use(m_shaderProgram);
}

void LightPass::commitImpl()
{
    int posLoc = rut::glVertexAttribPointer("VertexPosition", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, position));
    int tcLoc = rut::glVertexAttribPointer("VertexTexcoord0", m_shaderProgram, 2, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, texcoord0));
    int colLoc = rut::glVertexAttribPointer("VertexColor0", m_shaderProgram, 4, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, color));
    int normLoc = rut::glVertexAttribPointer("VertexNormal", m_shaderProgram, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const void*) offsetof(Vertex, normal));

    rut::glEnableVertexAttribArray(posLoc);
    rut::glEnableVertexAttribArray(tcLoc);
    rut::glEnableVertexAttribArray(colLoc);
    rut::glEnableVertexAttribArray(normLoc);

    m_baseLightLoc.Color                = m_shaderProgram->get("LightColor");
    m_baseLightLoc.AmbientIntensity     = m_shaderProgram->getUnsafe("AmbientIntensity");
    m_baseLightLoc.DiffuseIntensity     = m_shaderProgram->getUnsafe("DiffuseIntensity");

    glo::glUniform1i(m_shaderProgram->get("Mrt0"), 0);
    glo::glUniform1i(m_shaderProgram->get("Mrt1"), 1);

    m_uniformAccessors[UniformAccessors::LightPositionViewSpace] = m_shaderProgram->get("Position");
    m_uniformAccessors[UniformAccessors::EyePositionViewSpace] = m_shaderProgram->getUnsafe("EyeWorldPos");
}
