//
//  Logger.cpp
//  OGLArt
//
//  Created by tlosev on 10/14/14.
//
//

#include "Precompiled.h"
#include "Logger.h"

SINGLETON_SETUP(Logger);

Logger::Logger()
{
    SINGLETON_ENABLE_THIS;
}

Logger::~Logger()
{
    
}

void Logger::setupFileLog(const Path_t &logFilePath)
{
    BoostLogger::add_file_log(logFilePath,
                              LogKeywords::auto_flush = true,
                              LogKeywords::format = "[%TimeStamp%]: %Message%"
                              //LogKeywords::open_mode = (std::ios::out | std::ios::app)
                              );
    
    BoostLogger::core::get()->set_filter(BoostLogger::trivial::severity >= BoostLogger::trivial::info);
    BoostLogger::add_common_attributes();
    
    BoostLogger::add_console_log(std::cout, boost::log::keywords::format = ">> %Message%");
    
    m_fileLoggerActive = true;
}

void Logger::trace(Ostringstream_t &str)
{
    if (m_fileLoggerActive)
    {
        //BOOST_LOG_SEV(m_fileLog, BoostLogger::trivial::severity_level::info) << str.str();
    }
}

void Logger::log(const std::string& str)
{
    if (m_fileLoggerActive)
    {
        BOOST_LOG_SEV(m_fileLog, BoostLogger::trivial::severity_level::info) << str;
    }
}
