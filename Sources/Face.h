/*
 * File:   Face.h
 * Author: void
 *
 * Created on April 6, 2013, 4:32 PM
 */

#ifndef FACE_H
#    define	FACE_H

class Face
{
public:
    std::vector<uint32_t> indices;

    Face()
    {}
};

#endif	/* FACE_H */

