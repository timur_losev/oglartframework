#pragma once

#include "LightPass.h"

class PointLightPass : public LightPass
{
protected:
    virtual void commitImpl();

    struct PointLight
    {
        uint32_t Constant;
        uint32_t Linear;
        uint32_t Exp;

        PointLight():
            Constant(INVALID_ID),
            Linear(INVALID_ID),
            Exp(INVALID_ID)
        {}
    } m_pointLightLoc;

public:
    PointLightPass();
    virtual ~PointLightPass();

    virtual void use(LightConstPtr light);
};