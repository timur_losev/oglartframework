#pragma once

forward_this(RenderPass);
forward_this(RenderTechnique);

class RenderTechnique
{
public:
    typedef std::vector<RenderPassPtr> RenderPasses_t;

    enum
    {
        FFP_PASS = 0
    };

private:
    RenderPasses_t m_passes;

    std::string  m_name; //optional

    RenderPassPtr  m_currentPass;
public:

    RenderTechnique();
    ~RenderTechnique();

    RenderPassPtr getPass(size_t index) const;
    size_t        getPassesCount() const;

    void          addPass(RenderPassPtr, int at = -1);

    RenderPassPtr switchPass(size_t switchToPassId);
    RenderPassPtr getCurrentPass() const;

    const std::string& getName() const;
    void setName(const std::string&);
};