#include "Precompiled.h"
#include "SceneFile.h"
#include "Logger.h"

#include "Scene.h"
#include "LoaderCommon.h"

#include "MaterialManager.h"
#include "TextureManager.h"
#include "MeshManager.h"

SceneFile::SceneFile(ScenePtr requester) : m_file(nullptr), m_requester(requester)
{

}

SceneFile::~SceneFile()
{
    delete m_file;
    m_file = nullptr;
}

bool SceneFile::parse(const Path_t& fname)
{
    if (fname.empty())
    {
        OGL_LOG(("BlendFile: File [%s] loading failed. File name is empty.") % fname);
        return false;
    }

    m_file = new fbtBlend();
    int status = m_file->parse(fname, fbtFile::PM_COMPRESSED);
    if (status != fbtFile::FS_OK)
    {
        delete m_file;
        m_file = 0;
        OGL_LOG(("BlendFile: File [%s] loading failed. code: [%d])") % fname % status);;
        return false;
    }

    return true;
}

bool SceneFile::parse(const void* mem, uint32_t size)
{
    m_file = new fbtBlend();

    // first check to use uncompressed version
    int status = m_file->parse(mem, size, fbtFile::PM_UNCOMPRESSED, true);
#ifndef OGL_DISABLE_ZIP
    // if this fails with invalid-header string try to uncompress the blend
    if (status == fbtFile::FS_INV_HEADER_STR) {
        status = m_file->parse(mem, size, fbtFile::PM_COMPRESSED);
    }
#endif

    if (status != fbtFile::FS_OK)
    {
        delete m_file;
        m_file = 0;
        OGL_LOG(("BlendFile: MemoryBlend loading failed. code [%d]") % status);
        return false;
    }

    return true;
}

Blender::FileGlobal* SceneFile::getFileGlobal() const
{
    return m_file->m_fileGlobal;
}

Blender::Scene* SceneFile::getFirstScene() const
{
    assert(m_file);

    fbtList::Iterator it = getScenes();

    return it.hasMoreElements() ? (Blender::Scene*)it.getNext() : nullptr;
}

int SceneFile::getVersion() const
{
    assert(m_file);

    return m_file->getVersion();
}

void SceneFile::loadActiveScene()
{
    Blender::FileGlobal* fg = m_file->m_fileGlobal;
    if (fg)
    {
        if (!fg->curscene)
        {
            fg->curscene = getFirstScene();
        }

        Blender::Scene* sc = fg->curscene;

        _readCurrentSceneInfo(sc);
    }
}

void SceneFile::_readCurrentSceneInfo(Blender::Scene* sc)
{
    if (!sc) return;

    m_fps = sc->r.frs_sec / sc->r.frs_sec_base;

}

void SceneFile::_buildAllTextures()
{
    fbtList::Iterator iter = getImages();

    while (iter.hasMoreElements())
    {
        Blender::Image* img = (Blender::Image*)(iter.getNext());

        if (img->id.us <= 0)
        {
            //! Skip images with no users
            continue;
        }

        String_t name = OGL_IDNAME(img);

        TextureManager& textureManager = m_requester->getMeshManager()->getMaterialManager().getTextureManager();
    }
}

#define IMPLEMENT_GET_ITER_LIST(FNAME, FBTNAME) \
        fbtList::Iterator SceneFile::FNAME() const\
        { \
            assert(m_file); \
            fbtList::Iterator iter(&m_file->FBTNAME); \
            return iter; \
        }


IMPLEMENT_GET_ITER_LIST(getScenes, m_scene)
IMPLEMENT_GET_ITER_LIST(getTexts, m_text)
IMPLEMENT_GET_ITER_LIST(getSounds, m_sound)
IMPLEMENT_GET_ITER_LIST(getActions, m_action)
IMPLEMENT_GET_ITER_LIST(getObjects, m_object)
IMPLEMENT_GET_ITER_LIST(getParticles, m_particle)
IMPLEMENT_GET_ITER_LIST(getVFonts, m_vfont)
IMPLEMENT_GET_ITER_LIST(getMeshes, m_mesh)
IMPLEMENT_GET_ITER_LIST(getArmatures, m_armature)
IMPLEMENT_GET_ITER_LIST(getGroups, m_group)
IMPLEMENT_GET_ITER_LIST(getScripts, m_script)
IMPLEMENT_GET_ITER_LIST(getCameras, m_camera)
IMPLEMENT_GET_ITER_LIST(getWorlds, m_world)
IMPLEMENT_GET_ITER_LIST(getMaterials, m_mat)
IMPLEMENT_GET_ITER_LIST(getImages, m_image)
IMPLEMENT_GET_ITER_LIST(getLamps, m_lamp)
IMPLEMENT_GET_ITER_LIST(getLatts, m_latt)
IMPLEMENT_GET_ITER_LIST(getIpos, m_ipo)
IMPLEMENT_GET_ITER_LIST(getKeys, m_key)
IMPLEMENT_GET_ITER_LIST(getCurves, m_curve)
IMPLEMENT_GET_ITER_LIST(getNodeTrees, m_nodetree)
IMPLEMENT_GET_ITER_LIST(getLibrarys, m_library)
IMPLEMENT_GET_ITER_LIST(getMBalls, m_mball)



