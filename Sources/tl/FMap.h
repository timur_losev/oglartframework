#pragma once

#include <boost/container/map.hpp>

template < class Key, class T, class Compare = std::less<Key>
    , class Allocator = std::allocator< std::pair< const Key, T> >, class MapOptions = boost::tree_assoc_defaults >
using FMap = boost::container::map<Key, T, Compare, Allocator, MapOptions>;
