#pragma once

#include <boost/container/list.hpp>

template<typename T, typename A = std::allocator<T> >
using FList = boost::container::list<T, A>;