#pragma once

#include <boost/container/vector.hpp>
#include "FMemory.h"

template <typename T>
using FVector = boost::container::vector < T, std::allocator<T> >;

template<typename T>
inline bool FVectorRawEqual(const FVector<T>& v1, const FVector<T>& v2)
{
    if (v1.size() == v2.size())
    {
        if (v1.empty()) return true;

        return sysMemcmp(v1.data(), v2.data(), sizeof(T) * v1.size());
    }

    return false;
}


