#pragma once

#include <boost/unordered_map.hpp>

template<
    typename Key,
    typename T,
    typename Hasher = std::hash<Key>,
    typename Eq = std::equal_to<Key>,
    typename A = std::allocator<std::pair<Key, T> > >
using FHashTable = boost::unordered::unordered_map<Key, T, Hasher, Eq, A>;
