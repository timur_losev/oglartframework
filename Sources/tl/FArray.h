#pragma once

#include <boost/array.hpp>

template<class T, size_t N>
using FArray = boost::array < T, N > ;