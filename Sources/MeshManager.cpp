#include "Precompiled.h"
#include "MeshManager.h"

#include "ColladaParser.h"

#include "MaterialManager.h"

#include "GraphicMesh.h"
#include "Node.h"

MeshManager::MeshManager():
materialManager(new MaterialManager())
{
}

void MeshManager::buildMeshesForNode(Collada::ColladaParserConstPtr parser, Collada::NodeConstPtr n, NodePtr target)
{
    // accumulated mesh references by this node
    Node::Meshes_t newMeshRefs;
    newMeshRefs.reserve(n->meshes.size());

    // add a mesh for each subgroup in each collada mesh

    BOOST_FOREACH(const Collada::MeshInstance& mid, n->meshes)
    {
        Collada::MeshConstPtr srcMesh;
        Collada::ControllerConstPtr srcController;

        // find the referred mesh

        const Collada::ColladaParser::MeshLibrary_t& meshLibrary = parser->meshLibrary;
        const Collada::ColladaParser::ControllerLibrary_t& controllerLibrary = parser->controllerLibrary;

        Collada::ColladaParser::MeshLibrary_t::const_iterator srcMeshIt =
            meshLibrary.find(mid.meshOrController);

        if (srcMeshIt == meshLibrary.end())
        {
            // if not found in the mesh-library, it might also be a controller referring to a mesh
            Collada::ColladaParser::ControllerLibrary_t::const_iterator srcContrIt =
                controllerLibrary.find( mid.meshOrController);

            if ( srcContrIt != controllerLibrary.end())
            {
                srcController = srcContrIt->second;
                srcMeshIt = meshLibrary.find(srcController->meshId);

                if ( srcMeshIt != meshLibrary.end())
                {
                    srcMesh = srcMeshIt->second;
                }
            }

            if ( !srcMesh)
            {
                OGL_TRACE_WARN(GetLoggerPtr(), "Unable to find geometry for ID \"" << mid.meshOrController  << "\". Skipping.")
                    continue;
            }
        }
        else
        {
            // ID found in the mesh library -> direct reference to an unskinned mesh
            srcMesh = srcMeshIt->second;
        }

        // build a mesh for each of its subgroups

        size_t vertexStart = 0, faceStart = 0;
        for ( size_t sm = 0; sm < srcMesh->subMeshes.size(); ++sm)
        {
            const Collada::SubMesh& submesh = srcMesh->subMeshes[sm];
            if ( submesh.numFaces == 0)
            {
                continue;
            }

            // find material assigned to this submesh
            std::string meshMaterial;
            const auto meshMatIt = mid.materials.find( submesh.material);

            const Collada::SemanticMappingTable* table = nullptr;
            if (meshMatIt != mid.materials.end())
            {
                table = &meshMatIt->second;
                meshMaterial = table->matName;
            }
            else
            {
                OGL_TRACE_WARN(GetLoggerPtr(),  boost::str( boost::format( "No material specified for subgroup \"%s\" in geometry \"%s\".") % submesh.material % mid.meshOrController));
                if (!mid.materials.empty())
                {
                    meshMaterial = mid.materials.begin()->second.matName;
                }
            }

            if (table && !table->table.empty() )
            {
                //TODO
                /*std::pair<Collada::Effect*, aiMaterial*>&  mat = newMats[matIdx];

                // Iterate through all texture channels assigned to the effect and
                // check whether we have mapping information for it.
                ApplyVertexToEffectSemanticMapping(mat.first->mTexDiffuse,    *table);
                ApplyVertexToEffectSemanticMapping(mat.first->mTexAmbient,    *table);
                ApplyVertexToEffectSemanticMapping(mat.first->mTexSpecular,   *table);
                ApplyVertexToEffectSemanticMapping(mat.first->mTexEmissive,   *table);
                ApplyVertexToEffectSemanticMapping(mat.first->mTexTransparent, *table);
                ApplyVertexToEffectSemanticMapping(mat.first->mTexBump,       *table);*/
            }

            // built lookup index of the Mesh-Submesh-Material combination
            ColladaMeshIndex index( mid.meshOrController, sm, meshMaterial);

            // if we already have the mesh at the library, just add its index to the node's array
            MeshIndexByID_t::const_iterator dstMeshIt = meshIndexByID.find(index);

            if ( dstMeshIt != meshIndexByID.end())
            {
                newMeshRefs.push_back(meshes[dstMeshIt->second]);
            }
            else
            {
                // else we have to add the mesh to the collection and store its newly assigned index at the node
                GraphicMeshPtr dstMesh ( createMesh( parser, srcMesh, submesh, srcController, vertexStart, faceStart) );

                MaterialPtr newMaterial = materialManager->getMaterial(meshMaterial);

                assert(newMaterial);

                dstMesh->setMaterial(newMaterial);

                // store the mesh, and store its new index in the node
                newMeshRefs.push_back(dstMesh);
                meshIndexByID[index] = meshes.size();

                meshes.push_back(dstMesh);
                vertexStart += dstMesh->vertices.size();
                faceStart += submesh.numFaces;

                // assign the material index
#if 0
                dstMesh->materialIndex = matIdx;
#endif
                dstMesh->name = mid.meshOrController;
            }
        }
    }

    if (!newMeshRefs.empty())
    {
        /*Node::Meshes_t& nms = target->getMeshes();
        nms = std::move(newMeshRefs);*/

        target->meshes = std::move(newMeshRefs);
    }

}

GraphicMeshPtr MeshManager::createMesh(Collada::ColladaParserConstPtr parser, Collada::MeshConstPtr srcMesh,
    const Collada::SubMesh& subMesh, Collada::ControllerConstPtr srcController,
    size_t startVertex, size_t startFace)
{
    GraphicMeshPtr dstMesh(new GraphicMesh());

    if (srcMesh->texCoords[1].size() > 0)
    {
        OGL_TRACE_WARN(GetLoggerPtr(), "Detected more than one texcoord set. Skipped other");
    }

    // count the vertices addressed by its faces
    const size_t numVertices = std::accumulate( srcMesh->faceSize.begin() + startFace,
        srcMesh->faceSize.begin() + startFace + subMesh.numFaces, 0);

    const std::vector<vec3>& texcoords = srcMesh->texCoords[0];
    const std::vector<vec3>& positions = srcMesh->positions;

    dstMesh->vertices.reserve(numVertices);

    /*std::copy( positions.begin() + startVertex,
    positions.begin() + startVertex + numVertices, std::back_inserter(dstMesh->vertices));*/


    bool hastexcoords = texcoords.size() == numVertices;
    bool hasnormals = srcMesh->normals.size() == numVertices;
#if USE_NORMAL_MAPPING
    bool hasTangents = srcMesh->tangents.size() == numVertices;
    bool hasBiNormals = srcMesh->bitangents.size() == numVertices;
#endif
    for (size_t i = startVertex , e = startVertex + numVertices; i < e; ++i)
    {
        Vertex vrt;
        vrt.position = positions[i];

        if (hastexcoords)
        {
            vrt.texcoord0 = vec2(texcoords[i].x, texcoords[i].y);
        }

        if (hasnormals)
        {
            vrt.normal = srcMesh->normals[i];
        }
#if USE_NORMAL_MAPPING
        if (hasBiNormals)
        {
            vrt.binormal = srcMesh->bitangents[i];
        }

        if (hasTangents)
        {
            vrt.tangent = srcMesh->tangents[i];
        }
#endif
        dstMesh->vertices.push_back(vrt);
    }

#if 0
    // tangents, if given.
    if ( pSrcMesh->mTangents.size() >= pStartVertex + numVertices)
    {
        dstMesh->mTangents = new aiVector3D[numVertices];
        std::copy( pSrcMesh->mTangents.begin() + pStartVertex, pSrcMesh->mTangents.begin() +
            pStartVertex + numVertices, dstMesh->mTangents);
    }

    // bitangents, if given.
    if ( pSrcMesh->mBitangents.size() >= pStartVertex + numVertices)
    {
        dstMesh->mBitangents = new aiVector3D[numVertices];
        std::copy( pSrcMesh->mBitangents.begin() + pStartVertex, pSrcMesh->mBitangents.begin() +
            pStartVertex + numVertices, dstMesh->mBitangents);
    }

    // same for texturecoords, as many as we have
    // empty slots are not allowed, need to pack and adjust UV indexes accordingly
    for ( size_t a = 0, real = 0; a < /*AI_MAX_NUMBER_OF_TEXTURECOORDS*/ 1; a++)
    {
        if ( pSrcMesh->texCoords[a].size() >= pStartVertex + numVertices)
        {
            dstMesh->mTextureCoords[real] = new aiVector3D[numVertices];
            for ( size_t b = 0; b < numVertices; ++b)
                dstMesh->mTextureCoords[real][b] = pSrcMesh->mTexCoords[a][pStartVertex + b];

            dstMesh->mNumUVComponents[real] = pSrcMesh->mNumUVComponents[a];
            ++real;
        }
    }
#else

#endif

#if 0
    // same for vertex colors, as many as we have. again the same packing to avoid empty slots
    for ( size_t a = 0, real = 0; a < AI_MAX_NUMBER_OF_COLOR_SETS; a++)
    {
        if ( pSrcMesh->mColors[a].size() >= pStartVertex + numVertices)
        {
            dstMesh->mColors[real] = new aiColor4D[numVertices];
            std::copy( pSrcMesh->mColors[a].begin() + pStartVertex, pSrcMesh->mColors[a].begin() + pStartVertex + numVertices, dstMesh->mColors[real]);
            ++real;
        }
    }
#endif


    // create faces. Due to the fact that each face uses unique vertices, we can simply count up on each vertex
    size_t vertex = 0;
    const size_t numFaces = subMesh.numFaces;
    dstMesh->faces.resize(numFaces);

    for ( size_t a = 0; a < numFaces; ++a)
    {
        size_t s = srcMesh->faceSize[startFace + a];
        Face& face = dstMesh->faces[a];
        face.indices.reserve(s);
        for ( size_t b = 0; b < s; ++b)
        {
            face.indices.push_back(vertex++);
        }
    }

#if 0
    // create bones if given
    if ( pSrcController)
    {
        // refuse if the vertex count does not match
        //		if( pSrcController->mWeightCounts.size() != dstMesh->mNumVertices)
        //			throw DeadlyImportError( "Joint Controller vertex count does not match mesh vertex count");

        // resolve references - joint names
        const Collada::Accessor& jointNamesAcc = pParser.ResolveLibraryReference( pParser.mAccessorLibrary, pSrcController->mJointNameSource);
        const Collada::Data& jointNames = pParser.ResolveLibraryReference( pParser.mDataLibrary, jointNamesAcc.mSource);
        // joint offset matrices
        const Collada::Accessor& jointMatrixAcc = pParser.ResolveLibraryReference( pParser.mAccessorLibrary, pSrcController->mJointOffsetMatrixSource);
        const Collada::Data& jointMatrices = pParser.ResolveLibraryReference( pParser.mDataLibrary, jointMatrixAcc.mSource);
        // joint vertex_weight name list - should refer to the same list as the joint names above. If not, report and reconsider
        const Collada::Accessor& weightNamesAcc = pParser.ResolveLibraryReference( pParser.mAccessorLibrary, pSrcController->mWeightInputJoints.mAccessor);
        if ( &weightNamesAcc != &jointNamesAcc)
            throw DeadlyImportError( "Temporary implementational lazyness. If you read this, please report to the author.");
        // vertex weights
        const Collada::Accessor& weightsAcc = pParser.ResolveLibraryReference( pParser.mAccessorLibrary, pSrcController->mWeightInputWeights.mAccessor);
        const Collada::Data& weights = pParser.ResolveLibraryReference( pParser.mDataLibrary, weightsAcc.mSource);

        if ( !jointNames.mIsStringArray || jointMatrices.mIsStringArray || weights.mIsStringArray)
            throw DeadlyImportError( "Data type mismatch while resolving mesh joints");
        // sanity check: we rely on the vertex weights always coming as pairs of BoneIndex-WeightIndex
        if ( pSrcController->mWeightInputJoints.mOffset != 0 || pSrcController->mWeightInputWeights.mOffset != 1)
            throw DeadlyImportError( "Unsupported vertex_weight adressing scheme. ");

        // create containers to collect the weights for each bone
        size_t numBones = jointNames.mStrings.size();
        std::vector<std::vector<aiVertexWeight> > dstBones( numBones);

        // build a temporary array of pointers to the start of each vertex's weights
        typedef std::vector< std::pair<size_t, size_t> > IndexPairVector;
        std::vector<IndexPairVector::const_iterator> weightStartPerVertex;
        weightStartPerVertex.resize(pSrcController->mWeightCounts.size(), pSrcController->mWeights.end());

        IndexPairVector::const_iterator pit = pSrcController->mWeights.begin();
        for ( size_t a = 0; a < pSrcController->mWeightCounts.size(); ++a)
        {
            weightStartPerVertex[a] = pit;
            pit += pSrcController->mWeightCounts[a];
        }

        // now for each vertex put the corresponding vertex weights into each bone's weight collection
        for ( size_t a = pStartVertex; a < pStartVertex + numVertices; ++a)
        {
            // which position index was responsible for this vertex? that's also the index by which
            // the controller assigns the vertex weights
            size_t orgIndex = pSrcMesh->mFacePosIndices[a];
            // find the vertex weights for this vertex
            IndexPairVector::const_iterator iit = weightStartPerVertex[orgIndex];
            size_t pairCount = pSrcController->mWeightCounts[orgIndex];

            for ( size_t b = 0; b < pairCount; ++b, ++iit)
            {
                size_t jointIndex = iit->first;
                size_t vertexIndex = iit->second;

                float weight = ReadFloat( weightsAcc, weights, vertexIndex, 0);

                // one day I gonna kill that XSI Collada exporter
                if ( weight > 0.0f)
                {
                    aiVertexWeight w;
                    w.mVertexId = a - pStartVertex;
                    w.mWeight = weight;
                    dstBones[jointIndex].push_back( w);
                }
            }
        }

        // count the number of bones which influence vertices of the current submesh
        size_t numRemainingBones = 0;
        for ( std::vector<std::vector<aiVertexWeight> >::const_iterator it = dstBones.begin(); it != dstBones.end(); ++it)
            if ( it->size() > 0)
                numRemainingBones++;

        // create bone array and copy bone weights one by one
        dstMesh->mNumBones = numRemainingBones;
        dstMesh->mBones = new aiBone*[numRemainingBones];
        size_t boneCount = 0;
        for ( size_t a = 0; a < numBones; ++a)
        {
            // omit bones without weights
            if ( dstBones[a].size() == 0)
                continue;

            // create bone with its weights
            aiBone* bone = new aiBone;
            bone->mName = ReadString( jointNamesAcc, jointNames, a);
            bone->mOffsetMatrix.a1 = ReadFloat( jointMatrixAcc, jointMatrices, a, 0);
            bone->mOffsetMatrix.a2 = ReadFloat( jointMatrixAcc, jointMatrices, a, 1);
            bone->mOffsetMatrix.a3 = ReadFloat( jointMatrixAcc, jointMatrices, a, 2);
            bone->mOffsetMatrix.a4 = ReadFloat( jointMatrixAcc, jointMatrices, a, 3);
            bone->mOffsetMatrix.b1 = ReadFloat( jointMatrixAcc, jointMatrices, a, 4);
            bone->mOffsetMatrix.b2 = ReadFloat( jointMatrixAcc, jointMatrices, a, 5);
            bone->mOffsetMatrix.b3 = ReadFloat( jointMatrixAcc, jointMatrices, a, 6);
            bone->mOffsetMatrix.b4 = ReadFloat( jointMatrixAcc, jointMatrices, a, 7);
            bone->mOffsetMatrix.c1 = ReadFloat( jointMatrixAcc, jointMatrices, a, 8);
            bone->mOffsetMatrix.c2 = ReadFloat( jointMatrixAcc, jointMatrices, a, 9);
            bone->mOffsetMatrix.c3 = ReadFloat( jointMatrixAcc, jointMatrices, a, 10);
            bone->mOffsetMatrix.c4 = ReadFloat( jointMatrixAcc, jointMatrices, a, 11);
            bone->mNumWeights = dstBones[a].size();
            bone->mWeights = new aiVertexWeight[bone->mNumWeights];
            std::copy( dstBones[a].begin(), dstBones[a].end(), bone->mWeights);

            // apply bind shape matrix to offset matrix
            aiMatrix4x4 bindShapeMatrix;
            bindShapeMatrix.a1 = pSrcController->mBindShapeMatrix[0];
            bindShapeMatrix.a2 = pSrcController->mBindShapeMatrix[1];
            bindShapeMatrix.a3 = pSrcController->mBindShapeMatrix[2];
            bindShapeMatrix.a4 = pSrcController->mBindShapeMatrix[3];
            bindShapeMatrix.b1 = pSrcController->mBindShapeMatrix[4];
            bindShapeMatrix.b2 = pSrcController->mBindShapeMatrix[5];
            bindShapeMatrix.b3 = pSrcController->mBindShapeMatrix[6];
            bindShapeMatrix.b4 = pSrcController->mBindShapeMatrix[7];
            bindShapeMatrix.c1 = pSrcController->mBindShapeMatrix[8];
            bindShapeMatrix.c2 = pSrcController->mBindShapeMatrix[9];
            bindShapeMatrix.c3 = pSrcController->mBindShapeMatrix[10];
            bindShapeMatrix.c4 = pSrcController->mBindShapeMatrix[11];
            bindShapeMatrix.d1 = pSrcController->mBindShapeMatrix[12];
            bindShapeMatrix.d2 = pSrcController->mBindShapeMatrix[13];
            bindShapeMatrix.d3 = pSrcController->mBindShapeMatrix[14];
            bindShapeMatrix.d4 = pSrcController->mBindShapeMatrix[15];
            bone->mOffsetMatrix *= bindShapeMatrix;

            // HACK: (thom) Some exporters address the bone nodes by SID, others address them by ID or even name.
            // Therefore I added a little name replacement here: I search for the bone's node by either name, ID or SID,
            // and replace the bone's name by the node's name so that the user can use the standard
            // find-by-name method to associate nodes with bones.
            const Collada::Node* bnode = FindNode( pParser.mRootNode, bone->mName.data);
            if ( !bnode)
                bnode = FindNodeBySID( pParser.mRootNode, bone->mName.data);

            // assign the name that we would have assigned for the source node
            if ( bnode)
                bone->mName.Set( FindNameForNode( bnode));
            else
                DefaultLogger::get()->warn( boost::str( boost::format( "ColladaLoader::CreateMesh(): could not find corresponding node for joint \"%s\".") % bone->mName.data));

            // and insert bone
            dstMesh->mBones[boneCount++] = bone;
        }
    }
#endif



    return dstMesh;
}

void MeshManager::dumpMeshes()
{
    OGL_TRACE_DEBUG(GetLoggerPtr(), "Mesh dump begin\n{");
    for(auto i = meshes.begin(), e = meshes.end(); i != e; ++i)
    {
        GraphicMeshPtr mesh = *i;

        OGL_TRACE_DEBUG(GetLoggerPtr(), "\tMesh name " << mesh->name);
        OGL_TRACE_DEBUG(GetLoggerPtr(), "\n\t\tVertices\n\t\t{");

        const auto& vert = mesh->vertices;

        for(size_t f = 0, fe = mesh->faces.size(); f < fe; ++f)
        {
            const auto& ind = mesh->faces[f].indices;

            OGL_TRACE_DEBUG(GetLoggerPtr(), "\t\t\tFace size " << ind.size());
            OGL_TRACE_DEBUG(GetLoggerPtr(), boost::str(boost::format("\n\t\t\t%f %f %f") % vert[ind[0]].position.x % vert[ind[0]].position.y % vert[ind[0]].position.z));
            OGL_TRACE_DEBUG(GetLoggerPtr(), boost::str(boost::format("\n\t\t\t%f %f %f") % vert[ind[1]].position.x % vert[ind[1]].position.y % vert[ind[1]].position.z));
            OGL_TRACE_DEBUG(GetLoggerPtr(), boost::str(boost::format("\n\t\t\t%f %f %f") % vert[ind[2]].position.x % vert[ind[2]].position.y % vert[ind[2]].position.z));
        }

        OGL_TRACE_DEBUG(GetLoggerPtr(), "\n\t\t}");

    }

    OGL_TRACE_DEBUG(GetLoggerPtr(), "Mesh dump end\n}");
}

MaterialManager& MeshManager::getMaterialManager() const
{
    return *materialManager;
}
