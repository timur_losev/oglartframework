#pragma once

#include "Vertex.h"
#include "Adaptation.h"

#include "HardwareElement.h"

forward_this(Material);

class Line3D: public HardwareElement
{
public:
    typedef std::vector<Vertex> TwoVertex_t;

private:
    TwoVertex_t m_vert;
    MaterialPtr m_material;
public:
    Line3D();
    Line3D(const vec3& from, const vec3& to, const Color_t& color);

    TwoVertex_t& getTwoVertex() { return m_vert; }

    void        setMaterial(MaterialPtr mat);
    MaterialPtr getMaterial() const;

    virtual bool commit(GLenum usage = GL_STATIC_DRAW);
};