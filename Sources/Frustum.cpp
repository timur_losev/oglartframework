﻿#include "Precompiled.h"
#include "Frustum.h"
#include "BoxSphereBounds.h"

Frustum::Frustum()
{
    memset(frustum, 0, 24 * sizeof(float));
}

void Frustum::extractFrustum(const mat4& c)
{
    mat4 clip = glm::transpose(c);

    vec4 planes[6]=
    {   clip[3] - clip[0], //Right
        clip[3] + clip[0], //Left

        clip[3] + clip[1], //Bottom
        clip[3] - clip[1], //Top
            
        clip[3] + clip[2], //Near
        clip[3] - clip[2] //Far
    };

    for(size_t i = 0; i < 6; ++i)
    {
        float t = glm::length(vec3(planes[i].xyz()));
        frustum[i] = planes[i] / t;
    }
}

bool Frustum::inside( const vec3& point ) const
{
    for (int p = 0; p < 6; ++p)
    {
        float v =
            frustum[p][0] * point.x + 
            frustum[p][1] * point.y + 
            frustum[p][2] * point.z + 
            frustum[p][3];

        if (v <= 0)
        {
            return false;
        }
    }

    return true;
}

bool Frustum::inside( const BoxSphereBounds& bbs ) const
{
    const vec3& bscen = bbs.origin;

    for (int p = 0; p < 6; ++p)
    {
        float v = glm::dot(frustum[p], vec4(bscen.xyz(), 1.f));

        if (v <= -bbs.radius)
        {
            return false;
        }
    }

    return true;
}
