#include "Precompiled.h"
#include "Box.h"
#include "Vertex.h"

Box::Box( const Vertex* points, size_t count)
{
    for (size_t i = 0; i < count; ++i)
    {
        *this += points[i].position;
    }
}

Box& Box::operator+=( const Box& oth )
{
    if (isValid && oth.isValid)
    {
#if ASM_GOOD
        __asm
        {
            mov     eax,[oth]
            mov     ecx,[this]

            movss   xmm0,[ecx]Box.min.x
            movss   xmm1,[ecx]Box.min.y
            movss   xmm2,[ecx]Box.min.z
            minss   xmm0,[eax]Box.min.x
            minss   xmm1,[eax]Box.min.y
            minss   xmm2,[eax]Box.min.z
            movss   [ecx]Box.min.x,xmm0
            movss   [ecx]Box.min.y,xmm1
            movss   [ecx]Box.min.z,xmm2

            movss   xmm0,[ecx]Box.max.x
            movss   xmm1,[ecx]Box.max.y
            movss   xmm2,[ecx]Box.max.z
            maxss   xmm0,[eax]Box.max.x
            maxss   xmm1,[eax]Box.max.y
            maxss   xmm2,[eax]Box.max.z
            movss   [ecx]Box.max.x,xmm0
            movss   [ecx]Box.max.y,xmm1
            movss   [ecx]Box.max.z,xmm2
        }

#else
        min.x = glm::min( min.x, oth.min.x );
        min.y = glm::min( min.y, oth.min.y );
        min.z = glm::min( min.z, oth.min.z );
                                 
        max.x = glm::max( max.x, oth.max.x );
        max.y = glm::max( max.y, oth.max.y );
        max.z = glm::max( max.z, oth.max.z );

#endif
    }
    else
    {
        *this = oth;
    }

    return *this;
}

Box Box::operator+( const Box& oth ) const
{
    return Box(*this) += oth;
}

vec3 Box::getExtent() const
{
    return 0.5f * (max - min);
}

vec3 Box::getCenter() const
{
    return min + getExtent();
}
