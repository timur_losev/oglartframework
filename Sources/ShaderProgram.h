#pragma once

#include "ShaderObjectType.h"

forward_this(ShaderObject);
forward_this(ShaderProgram);

class ShaderProgram
{
public:
    typedef std::unordered_map<std::string, uint32_t> Elements_t;
private:
    ShaderObjectPtr m_vertexShader;
    ShaderObjectPtr m_fragmentShader;

    GLuint          m_handle;

    Bool_t          m_linked;

    Elements_t      m_elements;

    void printActiveAttributes();
    void printActiveUniforms();
public:

    ShaderProgram();
    ~ShaderProgram();

    bool                    compile(const char* data, size_t size, ShaderObjectType::Enum);
    bool                    compile(const std::string& data, ShaderObjectType::Enum);
    bool                    compile(const Path_t& file, ShaderObjectType::Enum);

    Bool_t                  link();

    const Elements_t&       getElements() const;

    // Generates a runtime error
    uint32_t                get(const std::string& elemName) const; //attributes and uniforms inside one map

    // Returns -1
    int                     getUnsafe(const std::string& elemName) const; //attributes and uniforms inside one map
    
    bool                    has(const std::string& elemName) const;

    Bool_t                  hasLinked();

    void                    use() const;

public:

    bool                    operator==(unsigned int) const;
    bool                    operator!=(unsigned int) const;
};