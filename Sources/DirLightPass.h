#pragma once

#include "RenderPass.h"

forward_this(Light);
forward_this(DirLightPass);

class DirLightPass : public RenderPass
{
private:
    struct BaseLight
    {
        uint32_t Color;
        uint32_t AmbientIntensity;
        uint32_t DiffuseIntensity;

        BaseLight():
            Color(INVALID_ID),
            AmbientIntensity(INVALID_ID),
            DiffuseIntensity(INVALID_ID)
        {}
    } m_baseLightLoc;

    struct DirectionalLight
    {
        uint32_t Direction;

        DirectionalLight():
            Direction(INVALID_ID)
        {}
    } m_directionalLightLoc;
protected:
    virtual void commitImpl();
public:

    DirLightPass();
    ~DirLightPass();

    virtual void use();
};