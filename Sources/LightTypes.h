#pragma once

class LightTypes
{
public:

    enum Enum
    {
        SPOT,
        AMBIENT,
        DIRECTIONAL,
        POINT,
        UNDEFINED
    };

    static std::string toString(Enum e)
    {
        switch(e)
        {
        case SPOT: return "SPOT";
        case AMBIENT: return "AMBIENT";
        case DIRECTIONAL: return "DIRECTIONAL";
        case POINT: return "POINT";
        case UNDEFINED: return "UNDEFINED";
        default: assert(false); return "ERROR";
        }
    }
};

class LightFalloffTypes
{
public:

    enum Enum
    {
        CONSTANT = 0,
        INVERSE_LINEAR = 1,
        INVERSE_SQUARE = 2,
        CUSTOM_CURVE = 3,
        LIN_QUAD_WEIGHTED = 4
    };

    static std::string toString(Enum e)
    {
        switch(e)
        {
            case CONSTANT : return "CONSTANT";
            case INVERSE_LINEAR: return "INVERSE_LINEAR";
            case INVERSE_SQUARE: return "INVERSE_SQUARE";
            case CUSTOM_CURVE: return "CUSTOM_CURVE";
            case LIN_QUAD_WEIGHTED: return "LIN_QUAD_WEIGHTED";
            default: assert(false); return "ERROR";
        }
    }
};