/*
 * File:   ColladaParser.h
 * Author: void
 *
 * Created on March 30, 2013, 10:47 PM
 */

#ifndef COLLADAPARSER_H
#    define	COLLADAPARSER_H


#ifdef WIN32
#    include "tinyxml2/tinyxml2.h"
#else
#    include "tinyxml2.h"
#endif

#    include "ColladaHelper.h"

#include "Logger.h"

class Logger;

namespace Collada
{

    forward_this_s(Mesh);

    // ------------------------------------------------------------------------------------------

    /** Parser helper class for the Collada loader.
     *
     *  Does all the XML reading and builds internal data structures from it,
     *  but leaves the resolving of all the references to the loader.
     */
    class ColladaParser
    {
        unsigned char*  m_bcolUnitBuffer = nullptr;

    public:
        /** Filename, for a verbose error message */
        String_t fileName;

        bool        binaryReader;

        Logger*     m_logger;
        /** XML reader, member for everyday use */
        //irr::io::IrrXMLReader* mReader;
        tinyxml2::XMLDocument reader;

        /** All data arrays found in the file by ID. Might be referred to by actually
            everyone. Collada, you are a steaming pile of indirection. */
        typedef std::unordered_map<std::string, Collada::DataPtr> DataLibrary_t;

        DataLibrary_t dataLibrary;

        /** Same for accessors which define how the data in a data array is accessed. */
        typedef std::unordered_map<std::string, Collada::AccessorPtr> AccessorLibrary_t;
        AccessorLibrary_t accessorLibrary;

        /** Mesh library: mesh by ID */
        typedef std::unordered_map<std::string, Collada::MeshPtr> MeshLibrary_t;
        MeshLibrary_t meshLibrary;

        /** node library: root node of the hierarchy part by ID */
        typedef std::unordered_map<std::string, Collada::NodePtr> NodeLibrary_t;
        NodeLibrary_t nodeLibrary;

        /** Image library: stores texture properties by ID */
        typedef std::unordered_map<std::string, Collada::ImagePtr> ImageLibrary_t;
        ImageLibrary_t imageLibrary;

        /** Effect library: surface attributes by ID */
        typedef std::unordered_map<std::string, Collada::EffectPtr> EffectLibrary_t;
        EffectLibrary_t effectLibrary;

        /** Material library: surface material by ID */
        typedef std::unordered_map<std::string, Collada::MaterialPtr> MaterialLibrary_t;
        MaterialLibrary_t materialLibrary;

        /** Light library: surface light by ID */
        typedef std::unordered_map<std::string, Collada::LightPtr> LightLibrary_t;
        LightLibrary_t lightLibrary;

        /** Camera library: surface material by ID */
        typedef std::unordered_map<std::string, Collada::CameraPtr> CameraLibrary_t;
        CameraLibrary_t cameraLibrary;

        /** Controller library: joint controllers by ID */
        typedef std::unordered_map<std::string, Collada::ControllerPtr> ControllerLibrary_t;
        ControllerLibrary_t controllerLibrary;

        /** Pointer to the root node. Don't delete, it just points to one of
            the nodes in the node library. */
        Collada::NodePtr rootNode;

        /** Root animation container */
        AnimationPtr anims;

        /** Size unit: how large compared to a meter */
        float unitSize;

        /** Which is the up vector */
        enum
        {
            UP_X, UP_Y, UP_Z
        } upDirection;

        /** Collada file format version */
        Collada::EFormatVersion format;
    public:

        NodePtr getRootNode() const { return rootNode; }


        ColladaParser(const std::string& filename);

        /** Destructor */
        ~ColladaParser();;

        /** Reads the contents of the file */
        bool ReadContents();

        /** Reads the structure of the file */
        void ReadStructure(tinyxml2::XMLElement* elem);

        /** Reads asset informations such as coordinate system informations and legal blah */
        void ReadAssetInfo(tinyxml2::XMLElement* elem);

        /** Reads the animation library */
        void ReadAnimationLibrary(tinyxml2::XMLElement* elem);

        /** Reads an animation into the given parent structure */
        void ReadAnimation( AnimationPtr& pParent, tinyxml2::XMLElement* elem);

        /** Reads an animation sampler into the given anim channel */
        void ReadAnimationSampler( Collada::AnimationChannel& pChannel, tinyxml2::XMLElement*);

        /** Reads the skeleton controller library */
        void ReadControllerLibrary();

        /** Reads a controller into the given mesh structure */
        void ReadController( Collada::Controller& pController);

        /** Reads the joint definitions for the given controller */
        void ReadControllerJoints( Collada::Controller& pController);

        /** Reads the joint weights for the given controller */
        void ReadControllerWeights( Collada::Controller& pController);

        /** Reads the image library contents */
        void ReadImageLibrary(tinyxml2::XMLElement*);

        /** Reads an image entry into the given image */
        void ReadImage( Collada::Image& pImage, tinyxml2::XMLElement*);

        /** Reads the material library */
        void ReadMaterialLibrary(tinyxml2::XMLElement*);

        /** Reads a material entry into the given material */
        void ReadMaterial( Collada::Material& pMaterial, tinyxml2::XMLElement*);

        /** Reads the camera library */
        void ReadCameraLibrary();

        /** Reads a camera entry into the given camera */
        void ReadCamera( Collada::Camera& pCamera);

        /** Reads the light library */
        void ReadLightLibrary(tinyxml2::XMLElement*);

        /** Reads a light entry into the given light */
        void ReadLight( Collada::Light& pLight, tinyxml2::XMLElement*);

        void ReadPointLight(Collada::Light& pLight, tinyxml2::XMLElement*);
        void ReadSpotLight(Collada::Light& pLight, tinyxml2::XMLElement*);
        void ReadLightBaseData(Collada::Light& pLight, tinyxml2::XMLElement*);

        /** Reads the effect library */
        void ReadEffectLibrary(tinyxml2::XMLElement*);

        /** Reads an effect entry into the given effect*/
        void ReadEffect( Collada::Effect& pEffect, tinyxml2::XMLElement*);

        /** Reads an COMMON effect profile */
        void ReadEffectProfileCommon( Collada::Effect& pEffect, tinyxml2::XMLElement*);

        void ReadEffectTechniqueCommon(Collada::Effect& effect, tinyxml2::XMLElement*);
        void ReadEffectTechniqueExtra(Collada::Effect& effect, tinyxml2::XMLElement*);
        void ReadEffectExtra(Collada::Effect& effect, tinyxml2::XMLElement*);
        void ReadEffectPhong(Collada::Effect& effect, tinyxml2::XMLElement*);
        void ReadEffectLambert(Collada::Effect& effect, tinyxml2::XMLElement*);

        /** Read sampler properties */
        void ReadSamplerProperties( Collada::Sampler& pSampler, tinyxml2::XMLElement*);

        /** Reads an effect entry containing a color or a texture defining that color */
        void ReadEffectColor( Color_t& pColor, Collada::Sampler& pSampler, tinyxml2::XMLElement* e);

        /** Reads an effect entry containing a float */
        void ReadEffectFloat( float& pFloat, tinyxml2::XMLElement* e);

        /** Reads an effect parameter specification of any kind */
        void ReadEffectParam( Collada::EffectParam& pParam, tinyxml2::XMLElement*);

        /** Reads the geometry library contents */
        void ReadGeometryLibrary(tinyxml2::XMLElement*);

        /** Reads a geometry from the geometry library. */
        void ReadGeometry( Collada::MeshPtr& pMesh, tinyxml2::XMLElement*);

        /** Reads a mesh from the geometry library */
        void ReadMesh( Collada::MeshPtr& pMesh, tinyxml2::XMLElement*);

        /** Reads a source element - a combination of raw data and an accessor defining
         * things that should not be redefinable. Yes, that's another rant.
         */
        void ReadSource(tinyxml2::XMLElement* elem);

        /** Reads a data array holding a number of elements, and stores it in the global library.
         * Currently supported are array of floats and arrays of strings.
         */
        void ReadDataArray(tinyxml2::XMLElement*);

        /** Reads an accessor and stores it in the global library under the given ID -
         * accessors use the ID of the parent <source> element
         */
        void ReadAccessor( const std::string& pID, tinyxml2::XMLElement*);

        /** Reads input declarations of per-vertex mesh data into the given mesh */
        void ReadVertexData( Collada::MeshPtr& pMesh, tinyxml2::XMLElement*);

        /** Reads input declarations of per-index mesh data into the given mesh */
        void ReadIndexData( Collada::MeshPtr& pMesh, tinyxml2::XMLElement*);

        /** Reads a single input channel element and stores it in the given array, if valid */
        void ReadInputChannel( std::vector<Collada::InputChannel>& poChannels, tinyxml2::XMLElement*);

        /** Reads a <p> primitive index list and assembles the mesh data into the given mesh */
        void ReadPrimitives( Collada::MeshPtr& pMesh, std::vector<Collada::InputChannel>& pPerIndexChannels,
                            size_t pNumPrimitives, const size_t* pVCount, Collada::PrimitiveType pPrimType, tinyxml2::XMLElement*);

        /** Extracts a single object from an input channel and stores it in the appropriate mesh data array */
        void ExtractDataObjectFromChannel( const Collada::InputChannel& pInput, size_t pLocalIndex, Collada::MeshPtr pMesh);

        /** Reads the library of node hierarchies and scene parts */
        void ReadSceneLibrary(tinyxml2::XMLElement*);

        /** Reads a scene node's contents including children and stores it in the given node */
        void ReadSceneNode( NodePtr pNode, tinyxml2::XMLElement* elem);

        /** Reads a node transformation entry of the given type and adds it to the given node's transformation list. */
        void ReadNodeTransformation( Collada::NodePtr& pNode, Collada::ETransformType pType, tinyxml2::XMLElement*);

        /** Reads a mesh reference in a node and adds it to the node's mesh list */
        void ReadNodeGeometry(NodePtr pNode, tinyxml2::XMLElement*);

        /** Reads the collada scene */
        void ReadScene(tinyxml2::XMLElement*);

        // Processes bind_vertex_input and bind elements
        void ReadMaterialVertexInputBinding( Collada::SemanticMappingTable& tbl, tinyxml2::XMLElement*);

                /** Calculates the resulting transformation from all the given transform steps */
        glm::mat4 CalculateResultTransform( const std::vector<Collada::Transform>& pTransforms) const;


    protected:
        /** Aborts the file reading with an exception */
        void ThrowException( const std::string& pError) const;

        /** Skips all data until the end node of the current element */
        void SkipElement();

        /** Skips all data until the end node of the given element */
        void SkipElement( const char* pElement);

        /** Compares the current xml element name to the given string and returns true if equal */
        bool IsElement( const char* pName) const;

        /** Tests for the opening tag of the given element, throws an exception if not found */
        void TestOpening( const char* pName);

        /** Tests for the closing tag of the given element, throws an exception if not found */
        void TestClosing( const char* pName);

        /** Checks the present element for the presence of the attribute, returns its index
            or throws an exception if not found */
        int GetAttribute( const char* pAttr) const;

        /** Returns the index of the named attribute or -1 if not found. Does not throw,
            therefore useful for optional attributes */
        int TestAttribute( const char* pAttr) const;

        /** Reads the text contents of an element, throws an exception if not given.
            Skips leading whitespace. */
        const char* GetTextContent();

        /** Reads the text contents of an element, returns NULL if not given.
            Skips leading whitespace. */
        const char* TestTextContent();

        /** Reads a single bool from current text content */
        bool ReadBoolFromTextContent(const char* text);

        /** Reads a single float from current text content */
        float ReadFloatFromTextContent();

        /** Determines the input data type for the given semantic string */
        Collada::EInputType GetTypeForSemantic( const std::string& pSemantic) const;

        /** Finds the item in the given library by its reference, throws if not found */
        template <typename Type>
        std::shared_ptr<Type> ResolveLibraryReference(const std::unordered_map<std::string, std::shared_ptr<Type>> pLibrary, const std::string& pURL) const;

    } ;

    // ------------------------------------------------------------------------------------------------
    // Check for element match

    inline bool ColladaParser::IsElement( const char* pName) const
    {
        /*assert( mReader->getNodeType() == irr::io::EXN_ELEMENT);
        return ::strcmp( mReader->getNodeName(), pName) == 0;*/

        return false;
    }

    // ------------------------------------------------------------------------------------------------
    // Finds the item in the given library by its reference, throws if not found

    template <typename Type>
    std::shared_ptr<Type> ColladaParser::ResolveLibraryReference( const std::unordered_map<std::string, std::shared_ptr<Type>> pLibrary, const std::string& pURL) const
    {
        typedef typename std::shared_ptr<Type> TypePtr;
        auto it = pLibrary.find( pURL);
        if ( it == pLibrary.end())
        {
            OGL_TRACE_ASSERT(m_logger, false, "Unable to resolve library reference " << pURL);
            return std::shared_ptr<Type>();
        }

        return it->second;
    }

}//Collada

#endif	/* COLLADAPARSER_H */

