#include "Precompiled.h"
#include "GeomUtils.h"

#include "Mesh.h"
#include "VolumeMesh.h"

void GeomUtils::CreateLightCone( VolumeMeshPtr mesh, float radius, float height, int nVerticesInBase )
{
    Mesh::Vertices_t& vertices = mesh->vertices;
    vertices.resize(nVerticesInBase + 2);

    const float haflh = height * 0.5f;

    //Positions : cone head and base
    vertices[0].position = vec3(0.f, 0.f, 0.f);

    //Base :
    const float fDeltaBaseAngle = (2.f * glm::pi<float>()) / static_cast<float>(nVerticesInBase);
    for (int i = 1; i <= nVerticesInBase; i++)
    {
        float angle = i * fDeltaBaseAngle;

        float x = radius * glm::cos(angle);
        float z = radius * glm::sin(angle);

        vertices[i].position = vec3(x, z, -height);
    }

    mesh->indices.resize( (3 * nVerticesInBase) + (3 * (nVerticesInBase - 2) ));
    uint32_t* pIndex = &mesh->indices[0];

    for(int i = 0; i < nVerticesInBase; ++i)
    {
        *pIndex++ = 0;
        *pIndex++ = (i % nVerticesInBase) + 1;
        *pIndex++ = ((i + 1) % nVerticesInBase) + 1;
    }

    for(int i = 0; i < nVerticesInBase - 2; ++i)
    {
        *pIndex++ = 1;
        *pIndex++ = i + 3;
        *pIndex++ = i + 2;
    }
}

void GeomUtils::CreateLightSphere( VolumeMeshPtr mesh, float radius, uint32_t nSegments, uint32_t nRings )
{
    const float fDeltaRingAngle = glm::pi<float>() / static_cast<float>(nRings);
    const float fDeltaSegAngle = 2 * glm::pi<float>() / static_cast<float>(nSegments);

    const size_t indexCount = 6 * nRings * (nSegments + 1);
    mesh->indices.resize(indexCount);
    uint32_t* pIndex = &mesh->indices[0];
    uint16_t vIndex = 0;

    for(uint32_t ring = 0; ring <= nRings; ++ring)
    {
        float r0 = radius * std::sinf(ring * fDeltaRingAngle);
        float y0 = radius * std::cosf(ring * fDeltaRingAngle);

        for(uint32_t seg = 0; seg <= nSegments; ++seg)
        {
            float x0 = r0 * std::sinf(seg * fDeltaSegAngle);
            float z0 = r0 * std::cosf(seg * fDeltaSegAngle);

            Vertex v;
            v.position = vec3(x0, y0, z0);
            mesh->vertices.push_back(v);

            if (ring != nRings)
            {
                *pIndex++ = vIndex + nSegments + 1;
                *pIndex++ = vIndex;
                *pIndex++ = vIndex + nSegments;
                *pIndex++ = vIndex + nSegments + 1;
                *pIndex++ = vIndex + 1;
                *pIndex++ = vIndex;

                ++vIndex;
            }
        }
    }
}
