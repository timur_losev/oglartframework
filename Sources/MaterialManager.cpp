/*
 * File:   MaterialManager.cpp
 * Author: void
 *
 * Created on April 21, 2013, 12:45 PM
 */

#include "Precompiled.h"
#include "MaterialManager.h"

#include "ColladaParser.h"

#include "TextureManager.h"
#include "DeferredMaterialManager.h"

#include "Texture.h"

#include "Material.h"
#include "MaterialAttributes.h"
#include "RenderTechnique.h"
#include "RenderPass.h"
#include "GBufferPass.h"

#include "ShaderProgram.h"

#include "Global.h"

#include "Logger.h"

MaterialManager::MaterialManager():
m_textureManager(new TextureManager())
{
    m_deferredMaterialManager.reset(new DeferredMaterialManager());
}

MaterialManager::~MaterialManager() { }

void MaterialManager::buildMaterials(Collada::ColladaParserPtr parser)
{
    for(Collada::ColladaParser::MaterialLibrary_t::const_iterator matIt = parser->materialLibrary.begin(), matEnd = parser->materialLibrary.end();
        matIt != matEnd; ++matIt)
    {
        Collada::MaterialConstPtr sourceMaterial = matIt->second;
        Collada::ColladaParser::EffectLibrary_t::iterator effit = parser->effectLibrary.find(sourceMaterial->effect);

        if (effit == parser->effectLibrary.end())
        {
            continue;
        }

        Collada::EffectConstPtr sourceEffect = effit->second;

#if 1
        MaterialPtr newMaterial = m_deferredMaterialManager->createGBufferMaterial(matIt->first);
#else
        MaterialPtr newMaterial = m_deferredMaterialManager->createForwardMaterial(matIt->first);
#endif
        GBufferPassPtr newPass = std::static_pointer_cast<GBufferPass>(newMaterial->getTechnique(0)->getPass(0));

        newPass->setAttribute(MaterialAttributes::NameString, "GBuffer");
        newPass->setAttribute(MaterialAttributes::DiffuseColor, sourceEffect->diffuse);
        newPass->setAttribute(MaterialAttributes::AmbientColor, sourceEffect->ambient);
        newPass->setAttribute(MaterialAttributes::SpecularColor, sourceEffect->specular);
        newPass->setAttribute(MaterialAttributes::EmissiveColor, sourceEffect->emissive);
        newPass->setAttribute(MaterialAttributes::Shininess, sourceEffect->shininess);
        newPass->setAttribute(MaterialAttributes::ShadeType, (uint32_t)sourceEffect->shadeType);

        std::vector<std::string> sources;

        sources.push_back(sourceEffect->texDiffuse.name);
#if USE_NORMAL_MAPPING
        sources.push_back(sourceEffect->texBump.name);
#endif
        /*{
            // Don't miss the order
            sourceEffect->texDiffuse.name
            //,sourceEffect->texBump.name
        };*/

        for (uint32_t i = 0; i < sources.size(); ++i)
        {
            const std::string samplerName = sources[i];

            if (samplerName.empty() && i == 0)
            {
                // Set texture stub
                TexturePtr texture = m_textureManager->getTexture("BumpyMetal.dds");
                newPass->setTexture(GL_TEXTURE0, texture);
            }
            else if (!samplerName.empty())
            {
                Collada::Effect::ParamLibrary_t::const_iterator paramsIt = sourceEffect->params.find(samplerName);
                if (paramsIt != sourceEffect->params.end())
                {
                    std::string surfaceName = paramsIt->second.reference;
                    paramsIt = sourceEffect->params.find(surfaceName);

                    assert(paramsIt != sourceEffect->params.end());

                    Collada::ColladaParser::ImageLibrary_t::const_iterator imagesIt = parser->imageLibrary.find(paramsIt->second.reference);

                    //assert(imagesIt != parser->imageLibrary.end());
                    TexturePtr texture = nullptr;

                    if (imagesIt != parser->imageLibrary.end())
                    {
                        texture = m_textureManager->getTexture(imagesIt->second->fileName);
                    }
                    else
                    {
                        texture = m_textureManager->getTexture("BumpyMetal.dds");
                    }

                    LOG_TODO("Make true texture layers");

                    newPass->setTexture(GL_TEXTURE0 + i, texture);
                }
                else
                {
                    assert(false);
                }
            }
        }
        
        
        //    const std::string samplerName = sourceEffect->texDiffuse.name;

        //    Collada::Effect::ParamLibrary_t::const_iterator paramsIt = sourceEffect->params.find(samplerName);
        //    if (paramsIt != sourceEffect->params.end())
        //    {
        //        std::string surfaceName = paramsIt->second.reference;

        //        paramsIt = sourceEffect->params.find(surfaceName);

        //        assert(paramsIt != sourceEffect->params.end());

        //        Collada::ColladaParser::ImageLibrary_t::const_iterator imagesIt = parser->imageLibrary.find(paramsIt->second.reference);

        //        assert(imagesIt != parser->imageLibrary.end());

        //        TexturePtr texture = m_textureManager->getTexture(imagesIt->second->fileName);

        //        LOG_TODO("Make true texture layers");

        //        newPass->setTexture(GL_TEXTURE0, texture);
        //    }
        //    else
        //    {
        //        assert(false);
        //    }
        //}
        //else
        //{
        //    // Set texture stub
        //    TexturePtr texture = m_textureManager->getTexture("BumpyMetal.ktx");
        //    newPass->setTexture(GL_TEXTURE0, texture);
        //}

        OGL_TRACE_DEBUG(GetLoggerPtr(), "Created new material " << matIt->first);

#ifdef DEBUG_MODE
        newPass->dumpAttributes();
#endif

        materials[matIt->first] = newMaterial;
    }
}

TextureManager& MaterialManager::getTextureManager() const
{
    return *m_textureManager;
}

DeferredMaterialManager& MaterialManager::getDeferredMaterialManager() const
{
    return *m_deferredMaterialManager;
}

MaterialPtr MaterialManager::getMaterial( const std::string& name ) const
{
    auto it = materials.find(name);

    return it != materials.end() ? it->second : nullptr;
}
