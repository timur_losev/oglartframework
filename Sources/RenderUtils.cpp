#include "Precompiled.h"
#include "RenderUtils.h"
#include "ShaderProgram.h"
#include "Logger.h"

namespace rut
{
    GLint shaderStatus( GLuint shader, GLenum param)
    {
        GLint status, length;
        GLchar buffer[1024];

        glo::glGetShaderiv(shader, param, &status);

        if (status != GL_TRUE)
        {
            glo::glGetShaderInfoLog(shader, 1024, &length, buffer);

            OGL_TRACE_ERROR(GetLoggerPtr(), "Shader error : " << buffer);
        }

        return status;
    }

    GLint shaderProgramStatus( GLuint program, GLenum param)
    {
        GLint status, length;
        GLchar buffer[1024];

        glo::glGetProgramiv(program, param, &status);

        if (status != GL_TRUE)
        {
            glo::glGetProgramInfoLog(program, 1024, &length, buffer);

            OGL_TRACE_ERROR(GetLoggerPtr(), "Shader error : " << buffer);
        }

        return status;
    }

    int glVertexAttribPointer( const std::string& attrib, ShaderProgramPtr program, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer )
    {
        int attr = program->getUnsafe(attrib);

        if (attr > -1)
        {
            glo::glVertexAttribPointer(static_cast<GLuint>(attr), size, type, normalized, stride, pointer);
        }

        return attr;
    }

    void glEnableVertexAttribArray( int attr )
    {
        if (attr > -1)
        {
            glo::glEnableVertexAttribArray(attr);
        }
    }

}