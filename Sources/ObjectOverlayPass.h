#pragma once

#include "RenderPass.h"

class ObjectOverlayPass : public RenderPass
{
protected:
    virtual void commitImpl();
public:

    ObjectOverlayPass();
    ~ObjectOverlayPass();

    virtual void use();
};