#pragma once

class Box;
class Sphere;
class Vertex;

forward_this(Line3D);
forward_this(Circle3D);

class BoxSphereBounds
{
private:

    mat4 m_prev;

    Line3DPtr m_radiusVisualization;
    Circle3DPtr m_circleVisualization;

public:
    vec3        origin;
    vec3        boxExtent;
    float       radius;

    BoxSphereBounds();
    BoxSphereBounds(const vec3& inOrigin, const vec3& inBoxExtent, float inRadius);
    BoxSphereBounds(const Box& inBox, const Sphere& inSphere);
    BoxSphereBounds(const Vertex* points, size_t count);

    void                    buildFromPoints(const Vertex* points, size_t count);

    BoxSphereBounds         transform(const mat4& m);

    Line3DPtr               getRadiusVisualization();
    Circle3DPtr             getCircleVisualization();
};