#include "Precompiled.h"
#include "SpotLightPass.h"
#include "ShaderProgramManager.h"
#include "RenderUtils.h"
#include "ShaderProgram.h"
#include "Vertex.h"
#include "Light.h"

SpotLightPass::SpotLightPass()
{
    m_ambientIntensity = 0.1f;
    m_diffuseIntensity = 1.f;
}

SpotLightPass::~SpotLightPass()
{

}

void SpotLightPass::use( LightConstPtr light )
{
    PointLightPass::use(light);

    assert(light->type == LightTypes::SPOT);

    if (m_spotLightLoc.outerCone != INVALID_ID)
        glo::glUniform1f(m_spotLightLoc.outerCone, std::cosf(light->angleOuterCone));

    if (m_spotLightLoc.innerCone != INVALID_ID)
        glo::glUniform1f(m_spotLightLoc.innerCone, std::cosf(light->angleInnerCone));

    if (m_spotLightLoc.directional != INVALID_ID)
        glo::glUniform3fv(m_spotLightLoc.directional, 1, &light->directionViewSpace[0]);

}

void SpotLightPass::commitImpl()
{
    PointLightPass::commitImpl();

    m_spotLightLoc.outerCone = m_shaderProgram->getUnsafe("OuterCone");
    m_spotLightLoc.innerCone = m_shaderProgram->getUnsafe("InnerCone");
    m_spotLightLoc.directional = m_shaderProgram->getUnsafe("Direction");
}
