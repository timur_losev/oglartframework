/*
* File:   TextureManager.cpp
* Author: void
*
* Created on April 15, 2013, 10:33 PM
*/

#include "Precompiled.h"
#include "TextureManager.h"

#include "Texture.h"

#include "Global.h"

#include "Logger.h"

#include "gli/gli.hpp"

#ifdef _WIN32
#include <cvt/wstring>
#endif

#include <locale>
#include <codecvt>

TextureConstPtr TextureManager::textureInUse;

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{

}

TexturePtr TextureManager::getTexture(const Path_t &filename)
{
    Path_t path = Global::FileSystemMappings::TexturesPath / filename;
    
    Textures_t::iterator it = m_textures.find(path.c_str());
    if (it != m_textures.end())
    {
        return it->second;
    }
    else
    {
        OGL_TRACE_ASSERT(GetLoggerPtr(), FS::exists(path), "Unable to open " << path);
        
        std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;

        TexturePtr newTexture(new Texture());
        gli::texture2D texture(gli::load_dds(convert.to_bytes((char16_t*)path.c_str()).c_str()));

        glGenTextures(1, &newTexture->texture);
        
        glBindTexture(GL_TEXTURE_2D, newTexture->texture);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, GLint(texture.levels() - 1));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA);

        gli::texture2D::size_type mipMaps =  texture.levels();
        gli::internalFormat internalFormat = gli::internal_format(texture.format());

        ImageDimension_t dim = { texture.dimensions().x, texture.dimensions().y, 32 };
        newTexture->imageDimentsion = dim;

        glo::glTexStorage2D(GL_TEXTURE_2D,
                       GLint(mipMaps),
                       GLenum(internalFormat),
                       GLsizei(dim.width),
                       GLsizei(dim.height));

        newTexture->isCompressed = gli::is_compressed(texture.format());

        if (newTexture->isCompressed)
        {
            for (gli::texture2D::size_type mipMap = 0; mipMap < mipMaps; ++mipMap)
            {
                glo::glCompressedTexSubImage2D(GL_TEXTURE_2D,
                                          GLint(mipMap),
                                          0, 0,
                                          GLsizei(texture[mipMap].dimensions().x),
                                          GLsizei(texture[mipMap].dimensions().y),
                                          GLenum(gli::internal_format(texture.format())),
                                          GLsizei(texture[mipMap].size()),
                                          texture[mipMap].data());
            }
        }
        else
        {
            for (gli::texture2D::size_type mipMap = 0; mipMap < mipMaps; ++mipMap)
            {
                glTexSubImage2D(GL_TEXTURE_2D,
                                GLint(mipMap),
                                0, 0,
                                GLsizei(texture[mipMap].dimensions().x),
                                GLsizei(texture[mipMap].dimensions().y),
                                GLenum(gli::external_format(texture.format())),
                                GLenum(gli::type_format(texture.format())),
                                texture[mipMap].data());                
            }
        }

        return newTexture;
    }

    GLenum texError = glGetError();

    OGL_TRACE_ASSERT(GetLoggerPtr(), texError == 0, "OGL Texture loading error " << texError);

}

bool TextureManager::use( TextureConstPtr texture )
{
    if (texture != textureInUse)
    {
        texture->bind();

        textureInUse = texture;

        return true;
    }
    else
    {
        return false;
    }
}
