#include "Precompiled.h"
#include "Line3D.h"
#include "RenderUtils.h"
#include "Material.h"

Line3D::Line3D( const vec3& from, const vec3& to, const Color_t& col )
{
    m_vert.resize(2);

    m_vert[0].position = from;
    m_vert[0].color = col;

    m_vert[1].position = to;
    m_vert[1].color = col;
}

Line3D::Line3D()
{
    m_vert.resize(2);
}

bool Line3D::commit(GLenum usage)
{
    assert(checkVirginity());

    glo::glGenVertexArrays(1, &m_vboHandles[VAO]);
    glo::glBindVertexArray(m_vboHandles[VAO]);

    glo::glGenBuffers(1, &m_vboHandles[VBO]);
    glo::glBindBuffer(GL_ARRAY_BUFFER, m_vboHandles[VBO]);

    rut::glBufferData(GL_ARRAY_BUFFER, m_vert, usage);

    assert(m_material);

    m_material->commit();

    unbindAll();

    return true;
}

void Line3D::setMaterial( MaterialPtr mat )
{
    m_material = mat;
}

MaterialPtr Line3D::getMaterial() const
{
    return m_material;
}
