#pragma once

class ShaderObjectType
{
public:

    enum Enum
    {
        VERTEX = GL_VERTEX_SHADER,
        FRAGMENT = GL_FRAGMENT_SHADER,
        GEOMETRY = GL_GEOMETRY_SHADER,
        TESS_CONTROL = GL_TESS_CONTROL_SHADER,
        TESS_EVALUATION = GL_TESS_EVALUATION_SHADER
    };

    static std::string toString(Enum e)
    {
        switch(e)
        {
        case VERTEX: return "VERTEX";
        case FRAGMENT: return "FRAGMENT";
        case GEOMETRY: return "GEOMETRY";
        case TESS_CONTROL: return "TESS_CONTROL";
        case TESS_EVALUATION: return "TESS_EVALUATION";
        default: assert(false); return "UNKNOWN";
        }
    }

};