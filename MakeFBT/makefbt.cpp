#include "Precompiled.h"

#include "fbtBuilder.h"

#ifdef FBT_USE_SCALAR
# ifndef FBT_SCALAR
#  error FBT_USE_SCALAR defined without defining a type FBT_SCALAR="name"
# endif
#endif

int main(int argc, char** argv)
{
    if (argc < 4)
    {
        fbtPrintf("Usage:\n");
        fbtPrintf("\tmakefbt FileId outfile infile[0] ... infine[n]\n");
        return 1;
    }

    fbtBuilder tables;

    for (int i = 3; i < argc; ++i)
    {
        if (tables.parseFile(argv[i]) < 0)
            return 1;
    }

    int code;
    if (( code = tables.buildTypes()) != LNK_OK)
        return code;


    tables.writeFile(argv[1], argv[2]);
    return 0;
}
