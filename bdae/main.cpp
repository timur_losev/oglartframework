#include "Precompiled.h"
#include "ColladaParser.h"

int main(int argc, char** argv)
{
    Logger* logger = new Logger();
    logger->setupFileLog("bdae.log");

    if (argc < 2)
    {
        OGL_TRACE(GetLoggerPtr(), "Usage: ./bdae example.dae");
        return -1;
    }

    Collada::ColladaParser parser(argv[1]);
    
    std::string savefn = argv[1];

    size_t extpos = savefn.find_last_of(".");

    savefn = savefn.substr(0, extpos) + ".bdae";

    tinyxml2::XMLError xmlError = parser.reader.SaveFile(savefn.c_str());
    
    if (xmlError)
    {
        OGL_TRACE_ERROR(GetLoggerPtr(), "File save error : " << xmlError);
    }
    else
    {
        OGL_TRACE_INFO(GetLoggerPtr(), "File saved at " << savefn);
    }

    delete logger;
    
    return 0;
}