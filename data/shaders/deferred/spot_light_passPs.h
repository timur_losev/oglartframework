#version 330

uniform sampler2D Mrt0;
uniform sampler2D Mrt1;

uniform vec3 EyeWorldPos;

// Base light
uniform vec3  LightColor;
uniform float AmbientIntensity;
uniform float DiffuseIntensity;

// Spot light
uniform vec3  Position;
uniform vec3  Direction;

uniform float Constant;
uniform float Linear;
uniform float Exp;
uniform float OuterCone;
uniform float InnerCone;

out vec4 FragColor;

//in vec2 FragTexcoord0;

in vec3 Pos;

vec4 CalcSpotLight(vec3 WorldPos, vec3 Normal)
{
    vec4 final_color = vec4(LightColor, 1.0f) * AmbientIntensity;

    vec3 lightDir = WorldPos - Position;
    vec3 L = normalize(lightDir);
    vec3 D = Direction;

    float curAngle = dot(L, D);
    float innerMinusOuter = InnerCone - OuterCone;
    float f = (curAngle - OuterCone) / innerMinusOuter;
    float spot = clamp( f, 0.0, 1.0);
    
    float lambertTerm = max( dot(Normal, -L), 0.0);
    if(lambertTerm > 0.0)
    {
        final_color += vec4(LightColor, 1.0f) *  DiffuseIntensity * lambertTerm * spot;
    
        vec3 VertexToEye = normalize(EyeWorldPos - WorldPos);
        vec3 LightReflect = normalize(reflect(L, Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, 40);
        vec4 SpecularColor = vec4(0);
        if (SpecularFactor > 0)
        {
            SpecularColor = vec4(LightColor, 1.0f) * 1 * SpecularFactor * spot;
        }
    
        final_color += SpecularColor;
    }
    
    float Distance = length(lightDir);
    float Attenuation =  Constant +
                         Linear * Distance +
                         Exp * Distance * Distance;

    return final_color / Attenuation;
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / vec2(1024, 768);
}

void main()
{
    vec2 TexCoord = CalcTexCoord();

    vec4 Color0 = texture(Mrt0, TexCoord);
    vec4 Color1 = texture(Mrt1, TexCoord);

    float z = Color0.w;
    vec3 Normal = Color1.xyz;

    vec3 WorldPos = Pos * z / Pos.z;

    FragColor = vec4(Color0.xyz, 1.0) * CalcSpotLight(WorldPos, Normal);
    //FragColor = vec4(Color0.xyz + WorldPos * 0.00000001 + Normal * 0.00000001, 1) + CalcSpotLight(WorldPos, Normal) * 0.00000000001;
}