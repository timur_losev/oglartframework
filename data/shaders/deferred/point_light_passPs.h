#version 330

uniform sampler2D Mrt0;
uniform sampler2D Mrt1;

uniform vec3 EyeWorldPos;

// Base light
uniform vec3  LightColor;
uniform float AmbientIntensity;
uniform float DiffuseIntensity;

// Point light
uniform vec3  Position;
uniform float Constant;
uniform float Linear;
uniform float Exp;

out vec4 FragColor;

//in vec2 FragTexcoord0;

in vec3 Pos;

vec4 CalcLightInternal(
                       vec3 LightDirection,
                       vec3 WorldPos,
                       vec3 Normal)
{
    vec4 AmbientColor = vec4(LightColor, 1.0f) * AmbientIntensity;
	
    float DiffuseFactor = dot(Normal, -LightDirection);

    vec4 DiffuseColor  = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0)
    {
        DiffuseColor = vec4(LightColor, 1.0f) * DiffuseIntensity * DiffuseFactor;

        vec3 VertexToEye = normalize(EyeWorldPos - WorldPos);
        vec3 LightReflect = normalize(reflect(LightDirection, Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, 40);
        if (SpecularFactor > 0)
        {
            SpecularColor = vec4(LightColor, 1.0f) * 1 * SpecularFactor;
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcPointLight(vec3 WorldPos, vec3 Normal)
{
    vec3 LightDirection = WorldPos - Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);

    vec4 Color = CalcLightInternal(LightDirection, WorldPos, Normal);

    float Attenuation =  Constant +
                         Linear * Distance +
                         Exp * Distance * Distance;

    //Attenuation = min(1.0, Attenuation);

    return Color / Attenuation;
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / vec2(1024, 768);
}

vec3 diffuseModel( vec3 pos, vec3 norm, vec3 diff )
{
	vec3 s = normalize(Position - pos);
	float dp = dot(s, norm);
	float sDotN = max(dp , 0.0 );
	vec3 diffuse = LightColor * diff * sDotN;
	return diffuse;
}

void main()
{
    vec2 TexCoord = CalcTexCoord();
    /*vec3 WorldPos = texture(PositionMap, TexCoord).xyz;
    vec3 Color    = texture(ColorMap, TexCoord).xyz;
    vec3 Normal   = texture(NormalMap, TexCoord).xyz;
    Normal = normalize(Normal);*/
	
	vec4 Color0 = texture(Mrt0, TexCoord);
	vec4 Color1 = texture(Mrt1, TexCoord);

	float z = Color0.w;
	vec3 Normal = Color1.xyz;
	
	vec3 WorldPos = Pos * z / Pos.z;
	    
    FragColor = vec4(Color0.xyz, 1.0) * CalcPointLight(WorldPos, Normal);

	/*vec3 l = normalize(Position - WorldPos);
	vec3 v = normalize(EyeWorldPos - WorldPos);
	vec3 r = reflect(-v, Normal);
	vec4 diff = vec4(Color, 1) * max(dot(Normal, l), 0.0);
	vec4 spec = vec4(Color, 1) * pow(max(dot(l, r), 0.0), 40);
	FragColor = diff + spec;*/
	
	/*vec2 FragTexcoord0 = CalcTexCoord();

    vec3    p  = texture ( PositionMap,    FragTexcoord0 ).xyz;
    vec3    n  = texture ( NormalMap,      FragTexcoord0 ).xyz;
    vec3    c  = texture ( ColorMap,       FragTexcoord0 ).xyz;
    vec3    l  = normalize     ( Position - p );
    vec3    v  = normalize     ( - p );
    vec3    h  = normalize     ( l + v );
    float   diff = max         ( 0.0, dot ( l, n ) );
    float   spec = pow         ( max ( 0.0, dot ( h, n ) ), 40 );

	float Distance = length(Position - p);
	
	float Attenuation =  Constant +
                         Linear * Distance +
                         Exp * Distance * Distance;
	
	vec3 FinalCol = (diff * c * LightColor + vec3(spec)) / Attenuation;
    FragColor = vec4( FinalCol, 1.0);*/
	
	//FragColor = vec4(Color * 0.00000001 + WorldPos * 0.00000001 + Normal , 1);
}