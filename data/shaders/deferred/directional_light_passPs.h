#version 330

uniform sampler2D Mrt0;
uniform sampler2D Mrt1;

// Base light
uniform vec3 Color;
uniform float AmbientIntensity;
uniform float DiffuseIntensity;

// Directional light
uniform vec3 Direction;

out vec4 FragColor;

in vec3 Pos;

vec4 CalcLightInternal(
                       vec3 LightDirection,
                       vec3 WorldPos,
                       vec3 Normal)
{
    vec4 AmbientColor = vec4(Color, 1.0f) * AmbientIntensity;
    float DiffuseFactor = dot(LightDirection, Normal);

    vec4 DiffuseColor  = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0)
    {
        DiffuseColor = vec4(Color, 1.0f) * DiffuseIntensity * DiffuseFactor;

        vec3 VertexToEye = normalize(LightDirection + normalize(-WorldPos));
        vec3 LightReflect = normalize(reflect(-LightDirection, Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = pow(SpecularFactor, 40);
        if (SpecularFactor > 0)
        {
            //SpecularColor = vec4(Color, 1.0f) * 10 * SpecularFactor;
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcDirectionalLight(vec3 WorldPos, vec3 Normal)
{
    return CalcLightInternal(
                             Direction,
                             WorldPos,
                             Normal);
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / vec2(1024, 768);
}

void main()
{
	vec2 TexCoord =	CalcTexCoord();
    vec4 Color0 = texture(Mrt0, TexCoord);
	vec4 Color1 = texture(Mrt1, TexCoord);

	float z = Color0.w;
	vec3 Normal = Color1.xyz;
	
	vec3 WorldPos = normalize(Direction) * z; //Pos * z / Pos.z;
    FragColor = vec4(Color0.xyz, 1.0) * CalcDirectionalLight(WorldPos, Normal);
}