#version 330

uniform mat4 MVP;
uniform mat4 NormalMatrix;
uniform mat4 ModelView;
uniform mat4 ModelMatrix;

in vec3 VertexPosition;
in vec2 VertexTexcoord0;
in vec4 VertexColor0;
in vec3 VertexNormal;

out vec2 FragTexcoord0;
out vec4 FragColor0;
out vec3 EyeCoords;
out vec3 TransformedNormal;


void main()
{
    TransformedNormal = (NormalMatrix * vec4(VertexNormal, 0.0)).xyz;

    gl_Position = MVP * vec4(VertexPosition, 1.0);
    
    EyeCoords = (ModelView * vec4(VertexPosition, 1.0)).xyz;
    
    FragTexcoord0 = VertexTexcoord0;
    FragColor0 = VertexColor0;
}
