#version 330

uniform mat4 MVP;
uniform mat4 ModelView;

in vec3 VertexPosition;

out vec3 Pos;

void main()
{
    gl_Position = MVP * vec4(VertexPosition, 1.0);
	Pos = vec3(ModelView * vec4(VertexPosition, 1.0));
}