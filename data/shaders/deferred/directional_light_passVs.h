#version 330

uniform mat4 MVP;
in vec3 VertexPosition;
uniform mat4 ModelView;
out vec3 Pos;

void main()
{
	gl_Position = vec4(VertexPosition, 1.0);
	
	Pos = VertexPosition;
}