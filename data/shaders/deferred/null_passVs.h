#version 330

uniform mat4 MVP;
in vec3 VertexPosition;


void main()
{
    gl_Position = MVP * vec4(VertexPosition, 1.0);    
}