#version 330

in vec2 FragTexcoord0;
in vec4 FragColor0;
in vec3 EyeCoords;
in vec3 TransformedNormal;
in vec3 TransformedBiNormal;
in vec3 TransformedTangent;


out vec4 Mrt0;
out vec4 Mrt1;
//out vec3 Normal;
//out vec3 TexCoord;

uniform sampler2D Texture0;
uniform sampler2D Texture1;

uniform float Shininess;

void main()
{
    //WorldPos = EyeCoords;
    
    vec3 nn = 2.0 * texture( Texture1, FragTexcoord0 ).xyz - vec3 ( 1.0 );
    
    Mrt0 = vec4(texture(Texture0, FragTexcoord0).xyz, EyeCoords.z);
    //Normal = normalize(TransformedNormal) * 0.5 + vec3(0.5);
    
    vec3 norm = normalize ( nn.x * TransformedTangent + nn.y * TransformedBiNormal + nn.z * TransformedNormal);
    Mrt1 = vec4(norm.xyz, Shininess);
    
	//Mrt1 = vec4(normalize(TransformedNormal).xyz, Shininess);
    //TexCoord = vec3(FragTexcoord0, 0.0);
}