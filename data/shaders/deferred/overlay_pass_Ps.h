#version 330

in vec4 FragColor0;

out vec4 FragColor;

void main()
{
	FragColor = FragColor0;
}