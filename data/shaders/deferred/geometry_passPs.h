#version 330

in vec2 FragTexcoord0;
in vec4 FragColor0;
in vec3 EyeCoords;
in vec3 TransformedNormal;


out vec4 Mrt0;
out vec4 Mrt1;
//out vec3 Normal;
//out vec3 TexCoord;

uniform sampler2D Texture0;
uniform float Shininess;

void main()
{
    //WorldPos = EyeCoords;
    Mrt0 = vec4(texture(Texture0, FragTexcoord0).xyz, EyeCoords.z);
    //Normal = normalize(TransformedNormal) * 0.5 + vec3(0.5);
	Mrt1 = vec4(normalize(TransformedNormal).xyz, Shininess);
    //TexCoord = vec3(FragTexcoord0, 0.0);
}