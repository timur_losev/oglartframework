#version 330

uniform mat4 MVP;
in vec3 VertexPosition;
in vec4 VertexColor0;

out vec4 FragColor0;

void main()
{
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	
	FragColor0 = VertexColor0;
}