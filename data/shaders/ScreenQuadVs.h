#version 330

in vec3 position;

out vec2 fragTexcoord;

void main()
{
    gl_Position = vec4(position, 1.0);
	
    fragTexcoord = (position.xy + vec2(1, 1)) / 2.0;
} 