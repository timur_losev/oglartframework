#version 330

in vec2 FragTexcoord0;
in vec4 FragColor0;

in vec3 EyeCoords;
in vec3 TransformedNormal;
in vec4 Pos;

uniform sampler2D Texture0;
out vec4 FragColor;

void main()
{
    FragColor = texture(Texture0, FragTexcoord0);
}