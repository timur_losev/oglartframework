﻿#version 330

out vec4 FragColor;

in vec2 fragTexcoord;

uniform sampler2D DepthTex;
uniform sampler2D PositionTex;
uniform sampler2D NormalTex;
uniform sampler2D ColorTex;

uniform vec3 LightPos;

/*const vec3 factor = vec3(0.27, 0.67, 0.06);

vec3 filter(vec2 tc)
{
    return vec3(dot(factor, texture(texture0, tc).rgb));
}*/

void main()
{    

    //vec3 LightPos = vec3(0, 0, 25);
    
    vec3    p  = texture ( PositionTex,    fragTexcoord ).xyz;
    vec3    n  = texture ( NormalTex,      fragTexcoord ).xyz;
    vec3    c  = texture ( ColorTex,       fragTexcoord ).xyz;
    vec3    l  = normalize     ( LightPos - p );
    vec3    v  = normalize     ( -p );
    vec3    h  = normalize     ( l + v );
    float   diff = max         ( 0.8, dot ( l, n ) );
    float   spec = pow         ( max ( 0.0, dot ( h, n ) ), 40.0 );
    
    FragColor = vec4( vec3(diff * c + spec), 1.0) + texture(DepthTex, fragTexcoord) * 0.000001;
    //FragColor = texture(PositionTex, fragTexcoord) * 0.00001 + texture(NormalTex, fragTexcoord) * 0.00001 + texture(ColorTex, fragTexcoord) + texture(DepthTex, fragTexcoord);
}