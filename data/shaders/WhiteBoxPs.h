#version 330

in vec2 FragTexcoord0;
in vec4 FragColor0;

in vec3 EyeCoords;
in vec3 TransformedNormal;
in vec4 Pos;

uniform sampler2D Texture0;

out vec3 PositionData;
out vec3 NormalData;
out vec3 ColorData;

uniform uint RenderPass;
uniform vec3 LightPos;

uniform sampler2D PositionTex;
uniform sampler2D NormalTex;
uniform sampler2D ColorTex;
uniform sampler2D DepthTex;

uniform sampler2D FinalTex;

out vec4 FragColor;

void GeometryPass()
{
	PositionData = EyeCoords;
    NormalData = normalize(TransformedNormal.xyz) * 0.5 + vec3(0.5);
    ColorData = vec3(texture(Texture0, FragTexcoord0) + FragColor0);
}

void PointLightPass()
{
    vec3    p  = texture ( PositionTex,    FragTexcoord0 ).xyz;
    vec3    n  = texture ( NormalTex,      FragTexcoord0 ).xyz;
    vec3    c  = texture ( ColorTex,       FragTexcoord0 ).xyz;
    vec3    l  = normalize     ( LightPos - p );
    vec3    v  = normalize     ( -p );
    vec3    h  = normalize     ( l + v );
    float   diff = max         ( 0.8, dot ( l, n ) );
    float   spec = pow         ( max ( 0.0, dot ( h, n ) ), 40.0 );
    
    FragColor = vec4( vec3(diff * c + spec), 1.0) + texture(DepthTex, FragTexcoord0) * 0.000001;
}

void main()
{
	if (RenderPass == 0U)
    {
        GeometryPass();
    }
    else if (RenderPass == 1U)
    {
        PointLightPass();
		//FragColor = texture(FinalTex, FragTexcoord0);
    }
    else if (RenderPass == 2U)
    {
        //NullPass
        //FragColor = vec4(1, 1, 1, 1);
    }
	else if (RenderPass == 3U)
	{
		PointLightPass();
	}
}