#version 330

uniform mat4 MVP;
uniform mat3 NormalMatrix;
uniform mat4 ModelViewMatrix;

in vec3 VertexPosition;
in vec2 VertexTexcoord0;
in vec4 VertexColor0;
in vec3 VertexNormal;

out vec2 FragTexcoord0;
out vec4 FragColor0;
out vec3 EyeCoords;
out vec3 TransformedNormal;
out vec4 Pos;


uniform uint RenderPass;

void GeometryPass()
{
	TransformedNormal = normalize(NormalMatrix * VertexNormal);

	Pos = MVP * vec4(VertexPosition, 1.0);
    gl_Position = Pos;

    EyeCoords = vec3(ModelViewMatrix * vec4(VertexPosition, 1.0));

    FragTexcoord0 = VertexTexcoord0;
    FragColor0 = VertexColor0;
}

void QuadPass()
{
	gl_Position = vec4(VertexPosition, 1.0);
	
    FragTexcoord0 = (VertexPosition.xy + vec2(1, 1)) / 2.0; // mul 0.5
}

void NullPass()
{
    gl_Position = MVP * vec4(VertexPosition, 1.0);
}

void main()
{
    if (RenderPass == 0U)
    {
        GeometryPass();
    }
    else if (RenderPass == 1U) 
    {
        QuadPass();
    }
    else if (RenderPass == 2U)
    {
        NullPass();
    }
	else if (RenderPass == 3U)
	{
		NullPass();
	}
}
