#version 330

in vec2 FragTexcoord0;
in vec4 FragColor0;

in vec3 EyeCoords;
in vec3 TransformedNormal;
in vec4 Pos;

uniform sampler2D Texture0;

out vec3 PositionData;
out vec3 NormalData;
out vec3 ColorData;

uniform uint RenderPass;
uniform vec3 LightPos;

uniform sampler2D PositionTex;
uniform sampler2D NormalTex;
uniform sampler2D ColorTex;
uniform sampler2D DepthTex;

uniform sampler2D FinalTex;

out vec4 FragColor;

void GeometryPass()
{
	FragColor = texture(Texture0, FragTexcoord0);
}


void main()
{
	GeometryPass(); 
}