#! /usr/bin/env python
if __name__ == '__main__':
    import buildstructheader
    raise SystemExit(buildstructheader.main())
    
import os
import shutil
from optparse import OptionParser
    
def main():
    parser = OptionParser()
    parser.add_option("-i", "--makesdna", dest="makesdna", type="string",
                  help="input makesdna directory")
                  
    parser.add_option("-o", "--header", dest="header", type="string", default="Blender.h",
            help="output header file")
            
    parser.add_option("-p", "--prefix", dest="prefix", type="string", default="",
            help="prefix to includes")

    
    (options, args) = parser.parse_args()
    
    dirList = os.listdir(options.makesdna)
    
    dnaHeaders = []
    
    for files in dirList:
        if files.endswith(".h"):
            dnaHeaders.append(files)

    f = open(options.header, 'w')
    f.writelines("#pragma once\n")
    f.writelines("namespace Blender {")
    for dnaHeader in dnaHeaders:
        f.writelines('\n    #include "' + options.prefix + dnaHeader + '"');
    
    f.writelines("\n} //Blender")
    
    f.close()
    